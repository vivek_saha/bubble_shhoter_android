using System;
using System.Collections;
using UnityEngine;

public class SmoothMove : MonoBehaviour
{
	private void Start()
	{
		if (GamePlay.Instance.GameStatus == GameState.Playing || GamePlay.Instance.GameStatus == GameState.Win)
		{
			base.StartCoroutine(this.StartMove());
		}
		else
		{
			mainscript.Instance.TargetCounter1++;
		}
	}

	private IEnumerator StartMove()
	{
		base.GetComponent<SpriteRenderer>().sortingLayerName = "UI layer";
		mainscript.Instance.TargetCounter1++;
		AnimationCurve curveX = new AnimationCurve(new Keyframe[]
		{
			new Keyframe(0f, base.transform.position.x),
			new Keyframe(0.5f, this.targetPos.x)
		});
		AnimationCurve curveY = new AnimationCurve(new Keyframe[]
		{
			new Keyframe(0f, base.transform.position.y),
			new Keyframe(0.5f, this.targetPos.y)
		});
		curveY.AddKey(0.2f, base.transform.position.y - 4f);
		float startTime = Time.time;
		Vector3 startPos = base.transform.position;
		float speed = UnityEngine.Random.Range(0.4f, 0.6f);
		float distCovered = 0f;
		while (distCovered < 1f)
		{
			distCovered = (Time.time - startTime) * speed;
			base.transform.position = new Vector3(curveX.Evaluate(distCovered), curveY.Evaluate(distCovered), 0f);
			yield return new WaitForEndOfFrame();
		}
		UnityEngine.Object.Destroy(base.gameObject.transform.parent.gameObject);
		yield break;
	}

	public Vector3 targetPos;
}
