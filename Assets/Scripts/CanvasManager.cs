using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
//using Firebase;
using UnityEngine;
using UnityEngine.UI;

public class CanvasManager : MonoBehaviour
{



	public static CanvasManager instance { get; set; }

	public bool testmode_unlock_alllvl= false;

	public GameObject Howto_play_panel;

    //   private void Awake()
    //   {
    //	if (testmode_unlock_alllvl)
    //	{
    //		PlayerPrefs.SetInt("OpenLevel", 49);

    //	}
    //}


    private void OnEnable()
    {
		Ads_priority_script.Instance.Show_interrestial();
	}
    private void Start()
	{

		
        if (testmode_unlock_alllvl)
        {
            //PlayerPrefs.SetInt("OpenLevel", 99);

        }

        CanvasManager.instance = this;
		//FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(delegate(Task<DependencyStatus> task)
		//{
		//	DependencyStatus result = task.Result;
		//	if (result == DependencyStatus.Available)
		//	{
		//		PlayerPrefs.SetInt("FireBase_Avail", 1);
		//		MonoBehaviour.print("Firebase Available ------ > avail ");
		//	}
		//	else
		//	{
		//		MonoBehaviour.print("Firebase not Available ------ > not avail");
		//		PlayerPrefs.SetInt("FireBase_Avail", 0);
		//		UnityEngine.Debug.LogError(string.Format("Could not resolve all Firebase dependencies: {0}", result));
		//	}
		//});
		GameObject gameObject = GameObject.Find("MyLogo");
		//GameObject gameObject2 = GameObject.Find("Helicopter");
		if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name == "map")
		{
			if (PlayerPrefs.GetInt("GamePlayFIrstTime", 0) == 0)
			{
				gameObject.GetComponent<SpriteRenderer>().sortingOrder = 10;
				//gameObject.transform.parent = gameObject2.transform;
				PlayerPrefs.SetInt("GamePlayFIrstTime", 1);
				gameObject.transform.localPosition = new Vector3(0f, -2f, 0f);
				//gameObject2.GetComponent<Animator>().Play("HeliCopterGameStartAnim", -1, 0f);
                base.StartCoroutine(this.Mylogonull(gameObject));
				Howto_play_panel.SetActive(true);

			}
			else
			{
				GameObject.Find("MyLogo").GetComponent<MyLogoChangeScript>().enabled = true;
				//gameObject2.transform.parent = GameObject.Find("Main Camera").transform;
				//gameObject2.GetComponent<Animator>().Play("HelicopterMapAnim", -1, 0f);
			}
			this.FreeSpinPanel = GameObject.Find("Canvas").transform.Find("FreeSpinsPanel").gameObject;
			this.GotReward = GameObject.Find("Canvas").transform.Find("GotReward").gameObject;
			if (PlayerPrefs.GetInt("sound", 1) == 1)
			{
				SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.helicopter);
			}
		}
		if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name == "menu")
		{
			if (PlayerPrefs.GetInt("music", 1) == 1)
			{
				MonoBehaviour.print("musicon");
				GameObject.Find("Music").GetComponent<AudioSource>().volume = 0.4f;
			}
			else
			{
				MonoBehaviour.print("musicoff");
				GameObject.Find("Music").GetComponent<AudioSource>().volume = 0f;
			}
			if (PlayerPrefs.GetInt("sound", 1) == 1)
			{
				MonoBehaviour.print("soundon");
				SoundBase.Instance.GetComponent<AudioSource>().volume = 1f;
			}
			else
			{
				MonoBehaviour.print("sound0ff");
				SoundBase.Instance.GetComponent<AudioSource>().volume = 0f;
			}
		}
	}




	public void show_rewarded_video_ads()
    {
		Ads_priority_script.Instance.watch_video(Ads_reward_type.FreeCoin);
    }

	private IEnumerator Mylogonull(GameObject game)
	{
		yield return new WaitForSeconds(0f);
		game.transform.parent = null;
		game.GetComponent<MyLogoChangeScript>().enabled = true;
		yield break;
	}

	private void Update()
	{
		if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name == "menu")
		{
			if (UnityEngine.Input.GetKeyDown(KeyCode.Escape) && !base.transform.GetChild(2).gameObject.activeSelf)
			{
				if (!this.ExitPanel.activeSelf)
				{
					this.ExitPanel.SetActive(true);
				}
				else
				{
					this.ExitPanel.SetActive(false);
				}
			}
		}
		else if (UnityEngine.Input.GetKeyDown(KeyCode.Escape) && !this.FreeSpinPanel.activeSelf)
		{
			if (!this.ExitPanel.activeSelf)
			{
				this.Setobjects(this.Exitpanel);
			}
			else
			{
				this.ExitPanel.SetActive(false);
			}
		}
	}

	public void SettingButton()
	{
		if (this.SettingSTS)
		{
			this.SettingBut.GetComponent<Animator>().Play("SettingPushButton", -1, 0f);
			this.SettingSTS = false;
		}
		else
		{
			this.SettingBut.GetComponent<Animator>().Play("SettingCloseButton", -1, 0f);
			this.SettingSTS = true;
		}
	}

	public void play(GameObject game)
	{
		game.GetComponent<Image>().enabled = false;
		GameObject.Find("Canvas").transform.GetChild(4).gameObject.SetActive(true);
		float x = this.LoadingIncreaser.GetComponent<RectTransform>().offsetMin.x;
		float x2 = this.LoadingIncreaser.GetComponent<RectTransform>().offsetMax.x;
		this.lastRoutine = base.StartCoroutine(this.LoadingIncrease(x, x2));
	}

	private IEnumerator LoadingIncrease(float left, float Right)
	{
		yield return new WaitForSeconds(0.025f);
		GameObject.Find("Canvas").transform.GetChild(2).gameObject.SetActive(true);
		left += 5f;
		Right += 5f;
		this.LoadingIncreaser.GetComponent<RectTransform>().offsetMin = new Vector2(left, 0f);
		this.LoadingIncreaser.GetComponent<RectTransform>().offsetMax = new Vector2(Right, 0f);
		this.lastRoutine = base.StartCoroutine(this.LoadingIncrease(left, Right));
		if (this.LoadingIncreaser.GetComponent<RectTransform>().offsetMin.x >= 0f)
		{
			this.LoadingIncreaser.GetComponent<RectTransform>().offsetMin = new Vector2(0f, 0f);
			this.LoadingIncreaser.GetComponent<RectTransform>().offsetMax = new Vector2(0f, 0f);
			base.StopCoroutine(this.lastRoutine);
		}
		yield break;
	}


	public void _ShowAndroidToastMessage(string message)
	{
		AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject unityActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

		if (unityActivity != null)
		{
			AndroidJavaClass toastClass = new AndroidJavaClass("android.widget.Toast");
			AndroidJavaObject toastObject = toastClass.CallStatic<AndroidJavaObject>("makeText", unityActivity, message, 0);
			toastObject.Call("show");
			//unityActivity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
			//{
			//}));
		}
	}
	public void freespin()
	{
		this.Setobjects(this.FreeSpinPanel);

		//if()
		if (PlayerPrefs.GetInt("SpinUsed", 0) == 1)
		{
			//toast
			_ShowAndroidToastMessage("Rewards Already Claimed Please Try Again Tomorrow...");
		}
		else
		{
			this.FreeSpinPanel.transform.GetChild(1).gameObject.SetActive(true);
			this.FreeSpinPanel.GetComponent<Animator>().enabled = true;
		}
	}

	private IEnumerator closeRewardPanel()
	{
		yield return new WaitForSeconds(0.5f);
		this.FreeSpinPanel.SetActive(false);
		yield break;
	}

	public void setFalseObjectPanels(GameObject game)
	{
		game.SetActive(false);
	}

	//public void ClickOnGetCoin()
	//{
	//	if (GoogleMobileAdsDemoScript.instance.rewardBasedVideo.IsLoaded())
	//	{
	//		GoogleMobileAdsDemoScript.instance.ShowRewardBasedVideo(0);
	//	}
	//	else
	//	{
	//		this.RewardeVideoAvailableText.text = "No video ads available right now!\n";
	//		this.WatchRewardedAdsBtn.GetComponent<RectTransform>().localScale = new Vector3(0f, 0f, 0f);
	//		this.TryAgainAdsBtn.GetComponent<RectTransform>().localScale = new Vector3(0f, 0f, 0f);
	//		this.RewardeVideoAvailableText.gameObject.transform.GetChild(0).GetComponent<Text>().text = "please wait.";
	//		base.StartCoroutine(this.checkInternetConnection());
	//	}
	//	this.tryagain = "getcoin";
	//}

	//public void TryAgain()
	//{
	//	if (GoogleMobileAdsDemoScript.instance.rewardBasedVideo.IsLoaded())
	//	{
	//		GoogleMobileAdsDemoScript.instance.ShowRewardBasedVideo(0);
	//	}
	//	else
	//	{
	//		base.StartCoroutine(this.checkInternetConnection());
	//	}
	//	this.WatchRewardedAdsBtn.GetComponent<RectTransform>().localScale = new Vector3(0f, 0f, 0f);
	//	this.TryAgainAdsBtn.GetComponent<RectTransform>().localScale = new Vector3(0f, 0f, 0f);
	//	this.tryagain = "tryagain";
	//}

	//private IEnumerator checkInternetConnection()
	//{
	//	WWW www = new WWW("https://www.google.com");
	//	this.Setobjects(this.RewardedVideoAdsPanel);
	//	this.RewardedVideoAdsPanel_TOP_text.gameObject.transform.GetComponent<Text>().text = "GET COIN";
	//	yield return www;
	//	if (www.error != null)
	//	{
	//		this.RewardeVideoAvailableText.gameObject.transform.GetChild(0).GetComponent<Text>().text = string.Empty;
	//		this.RewardeVideoAvailableText.text = "Check your internet connection and try again.\n";
	//		this.WatchRewardedAdsBtn.GetComponent<RectTransform>().localScale = new Vector3(0f, 0f, 0f);
	//		this.TryAgainAdsBtn.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);
	//	}
	//	else if (GoogleMobileAdsDemoScript.instance.RewardedAds_flag)
	//	{
	//		this.RewardeVideoAvailableText.gameObject.transform.GetChild(0).GetComponent<Text>().text = string.Empty;
	//		this.RewardeVideoAvailableText.text = "Watch full video to get Extra 3 Coin.";
	//		this.WatchRewardedAdsBtn.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);
	//		this.TryAgainAdsBtn.GetComponent<RectTransform>().localScale = new Vector3(0f, 0f, 0f);
	//	}
	//	else
	//	{
	//		this.RewardeVideoAvailableText.text = "No video ads available right now!\n";
	//		this.WatchRewardedAdsBtn.GetComponent<RectTransform>().localScale = new Vector3(0f, 0f, 0f);
	//		this.TryAgainAdsBtn.GetComponent<RectTransform>().localScale = new Vector3(0f, 0f, 0f);
	//		base.StartCoroutine(this.WaitForVideo());
	//	}
	//	yield break;
	//}

	//private IEnumerator WaitForVideo()
	//{
	//	while (!Ads_priority_script.Instance.gameObject.GetComponent<Admob>().check_loaded_rewarded())
	//	{
	//		if (!GoogleMobileAdsDemoScript.instance.RewardedAds_flag)
	//		{
	//			GoogleMobileAdsDemoScript.instance.RequestRewardBasedVideo();
	//		}
	//		this.RewardeVideoAvailableText.gameObject.transform.GetChild(0).GetComponent<Text>().text = "please wait.";
	//		yield return new WaitForSeconds(0.5f);
	//		this.RewardeVideoAvailableText.gameObject.transform.GetChild(0).GetComponent<Text>().text = "please wait..";
	//		yield return new WaitForSeconds(0.5f);
	//		this.RewardeVideoAvailableText.gameObject.transform.GetChild(0).GetComponent<Text>().text = "please wait...";
	//		yield return new WaitForSeconds(0.5f);
	//	}
	//	this.RewardeVideoAvailableText.gameObject.transform.GetChild(0).GetComponent<Text>().text = string.Empty;
	//	this.RewardeVideoAvailableText.text = "Watch full video to get Extra 3 Coin.";
	//	this.WatchRewardedAdsBtn.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);
	//	this.TryAgainAdsBtn.GetComponent<RectTransform>().localScale = new Vector3(0f, 0f, 0f);
	//	yield break;
	//}

	//public void WatchRewardedAds()
	//{
	//	MonoBehaviour.print("WatchRewardedAds============>");
	//	this.RewardedVideoAdsPanel.SetActive(false);
	//	base.StartCoroutine(this.waitforLoadVideo());
	//}

	//private IEnumerator waitforLoadVideo()
	//{
	//	MonoBehaviour.print("waitforLoadVideo============>");
	//	yield return new WaitForSeconds(0.3f);
	//	GoogleMobileAdsDemoScript.instance.ShowRewardBasedVideo(0);
	//	yield break;
	//}

	public void CloseRewardedVideoPanel()
	{
		this.RewardedVideoAdsPanel.SetActive(false);
	}

	private void OnApplicationPause(bool hasFocus)
	{
		if (!hasFocus)
		{
			//base.StartCoroutine(this.checkInternetConnection_For_Ads());
		}
	}

	//private IEnumerator checkInternetConnection_For_Ads()
	//{
	//	WWW www = new WWW("http://www.google.com");
	//	yield return www;
	//	if (www == null)
	//	{
	//		if (!GoogleMobileAdsDemoScript.instance.rewardBasedVideo.IsLoaded())
	//		{
	//			GoogleMobileAdsDemoScript.instance.RequestRewardBasedVideo();
	//		}
	//		if (!GoogleMobileAdsDemoScript.instance.interstitial.IsLoaded() && GoogleMobileAdsDemoScript.instance.interstitial_flag)
	//		{
	//			GoogleMobileAdsDemoScript.instance.RequestInterstitial();
	//		}
	//	}
	//	yield break;
	//}

	public void CloseRewarededGotreward()
	{
		GameObject.Find("Canvas").transform.Find("FreeSpinsPanel").GetComponent<Animator>().Play("FreeSpinPanelOut");
		base.StartCoroutine(this.CloseRewarededGotrewarded());
	}

	private IEnumerator CloseRewarededGotrewarded()
	{
		yield return new WaitForSeconds(0.5f);
		GameObject.Find("Canvas").transform.Find("FreeSpinsPanel").gameObject.SetActive(false);
		yield break;
	}

	public void Help()
	{
		this.Setobjects(GameObject.Find("Canvas").transform.Find("Tutorial").gameObject);
	}

	public void Rate_emoj(int index)
	{
		base.StopCoroutine("Rate_emoj_1");
		base.StopCoroutine("emoji_text_anim");
		this.emoj_index = index;
		this.emoj = this.Exitpanel.transform.GetChild(0).GetChild(this.emoj_index).gameObject;
		this.emoj_po = this.emoj.GetComponent<RectTransform>().anchoredPosition;
		if (index == 2 || index == 3 || index == 4)
		{
			this.Thnku.SetActive(false);
			this.Thnku.SetActive(true);
		}
		if (!this.Main_emoji.activeSelf)
		{
			this.Main_emoji.SetActive(true);
			this.Main_emoji.GetComponent<Image>().sprite = this.emoji_list[this.emoj_index - 2];
			this.Main_emoji.GetComponent<RectTransform>().anchoredPosition = this.emoj_po;
			if (this.emoj_index == 5 || this.emoj_index == 6)
			{
			}
		}
		else
		{
			this.Main_emoji.GetComponent<Image>().sprite = this.emoji_list[this.emoj_index - 2];
			base.StartCoroutine("Rate_emoj_1");
		}
		for (int i = 2; i < 7; i++)
		{
			this.Exitpanel.transform.GetChild(0).GetChild(i).GetChild(0).GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, -32f);
			this.Exitpanel.transform.GetChild(0).GetChild(i).GetChild(0).GetComponent<Text>().color = new Color32(178, 178, 178, byte.MaxValue);
			this.Exitpanel.transform.GetChild(0).GetChild(i).GetChild(0).GetComponent<Text>().fontSize = 13;
		}
		base.StartCoroutine("emoji_text_anim");
		this.emoj.transform.GetChild(0).GetComponent<Text>().fontSize = 14;
		this.emoj.transform.GetChild(0).GetComponent<Text>().color = new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue);
	}

	private IEnumerator Rate_emoj_1()
	{
		while (this.Main_emoji.GetComponent<RectTransform>().anchoredPosition.x != this.emoj_po.x)
		{
			yield return new WaitForSeconds(0.01f);
			this.Main_emoji.GetComponent<RectTransform>().anchoredPosition = Vector2.MoveTowards(this.Main_emoji.GetComponent<RectTransform>().anchoredPosition, this.emoj_po, 30f);
		}
		this.Main_emoji.GetComponent<RectTransform>().anchoredPosition = this.emoj_po;
		if (this.emoj_index == 5 || this.emoj_index == 6)
		{
		}
		yield break;
	}

	private IEnumerator emoji_text_anim()
	{
		while (this.emoj.transform.GetChild(0).GetComponent<RectTransform>().anchoredPosition.y > -40f)
		{
			yield return new WaitForSeconds(0.01f);
			this.emoj.transform.GetChild(0).GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, this.emoj.transform.GetChild(0).GetComponent<RectTransform>().anchoredPosition.y - 2f);
		}
		yield break;
	}

	public void ReDirectToApkLink()
	{
		base.StartCoroutine(this.ReDirectApkLink());
	}

	private IEnumerator ReDirectApkLink()
	{
		yield return new WaitForSeconds(0.5f);
		Application.OpenURL("https://play.google.com/store/apps/details?id=" + Application.identifier);
		yield break;
	}

	public void MoreGames()
	{
		SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.click);
		base.StartCoroutine(this.openInterstial());
	}

	public IEnumerator openInterstial()
	{
		yield return new WaitForSeconds(0.5f);
        Application.OpenURL("https://play.google.com/store/apps/details?id=" + Application.identifier);

        yield break;
	}

	public void YesButton()
	{
		SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.click);
		Application.Quit();
		UnityEngine.Object.Destroy(this.Music);
	}

	public void Setobjects(GameObject game)
	{
		this.MenuPlay.SetActive(false);
		this.LiveShop.SetActive(false);
		this.FreeSpinsPanel.SetActive(false);
		gemshop_script.Instance.gemshopp.SetActive(false);
		//this.GemsShop.SetActive(false);
		this.GotReward1.SetActive(false);
		this.ExitPanel1.SetActive(false);
		this.RewardedVideoAdsPanel1.SetActive(false);
		this.Tutorial.SetActive(false);
		this.Exit_Panel.SetActive(false);
		game.SetActive(true);
	}

	GameObject temp_for_double_reward;
	public void Open_double_coin_panel(GameObject a)
    {
		Time.timeScale = 0;
		double_coinPanel.SetActive(true);
		temp_for_double_reward = a;
    }
	 public void watch_video_for_double_coin()
    {
		Ads_priority_script.Instance.Double_reward(temp_for_double_reward);

    }


	public void close_double_coin_panel()
    {
		Time.timeScale = 1;
		double_coinPanel.SetActive(false);
    }

	public GameObject double_coinPanel;

	public GameObject LoadingIncreaser;

	public GameObject SettingBut;

	private Coroutine lastRoutine;

	private GameObject FreeSpinPanel;

	private GameObject GotReward;

	public GameObject ExitPanel;

	public Text RewardeVideoAvailableText;

	public GameObject RewardedVideoAdsPanel;

	public Button WatchRewardedAdsBtn;

	public Button TryAgainAdsBtn;

	public Text RewardedVideoAdsPanel_TOP_text;

	private string tryagain;

	private Vector2 emoj_po;

	private GameObject emoj;

	private int emoj_index;

	public GameObject Thnku;

	public GameObject Main_emoji;

	public List<Sprite> emoji_list;

	public GameObject Exitpanel;

	public GameObject sound;

	public GameObject music;

	public Sprite musicoff;

	public Sprite musicon;

	public Sprite soundon;

	public Sprite sound0ff;

	[Header("map scene")]
	public GameObject MenuPlay;

	public GameObject LiveShop;

	public GameObject FreeSpinsPanel;

	public GameObject GemsShop;

	public GameObject GotReward1;

	public GameObject ExitPanel1;

	public GameObject RewardedVideoAdsPanel1;

	public GameObject Tutorial;

	public GameObject Exit_Panel;

	public GameObject Life;

	public GameObject AddLife;

	public GameObject Music;

	private bool SettingSTS = true;
}
