using System;
using UnityEngine;

public class ParticleSorting : MonoBehaviour
{
	private void Start()
	{
		base.GetComponent<ParticleSystem>().GetComponent<Renderer>().sortingLayerName = "UI layer";
		base.GetComponent<ParticleSystem>().GetComponent<Renderer>().sortingOrder = this.sortingOrder;
	}

	private void Update()
	{
	}

	public int sortingOrder = 1;
}
