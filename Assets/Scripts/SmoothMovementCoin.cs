using System;
using System.Collections;
using InitScriptName;
using UnityEngine;

public class SmoothMovementCoin : MonoBehaviour
{
	private void Start()
	{
		this.targetPos = GameObject.Find("CoinPosition").transform.position;
		base.StartCoroutine(this.StartMove());
	}

	private IEnumerator StartMove()
	{
		AnimationCurve curveX = new AnimationCurve(new Keyframe[]
		{
			new Keyframe(0f, base.transform.position.x),
			new Keyframe(0.5f, this.targetPos.x)
		});
		AnimationCurve curveY = new AnimationCurve(new Keyframe[]
		{
			new Keyframe(0f, base.transform.position.y),
			new Keyframe(0.5f, this.targetPos.y)
		});
		curveY.AddKey(0.2f, base.transform.position.y - 2f);
		float startTime = Time.time;
		Vector3 startPos = base.transform.position;
		float speed = UnityEngine.Random.Range(0.4f, 0.6f);
		float distCovered = 0f;
		int x = 0;
		while (distCovered < 1f)
		{
			distCovered = (Time.time - startTime) * speed;
			base.transform.position = new Vector3(curveX.Evaluate(distCovered), curveY.Evaluate(distCovered), 0f);
			yield return new WaitForEndOfFrame();
			if (this.targetPos.y.ToString("00.00") == base.transform.position.y.ToString("00.00"))
			{
				SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.ReceiveCoin);
				if (Double_coin)
				{
					InitScript.Instance.AddGems(2);
				}
				else
				{
					InitScript.Instance.AddGems(1);
				}
				x++;
				UnityEngine.Object.Destroy(base.gameObject);
				break;
			}
		}
		
		yield break;
	}

	[HideInInspector]
	public Vector3 targetPos;
	public bool Double_coin = false;
}
