﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using UnityEngine.Networking;
using System.IO;
using System;
//using IAP;
using TMPro;
using UnityEngine.UI;

public class Setting_API : Singleton<Setting_API>
{

    //public static Setting_API Instance;

    // API url
    string url = "/api/setting";

    // resulting JSON from an API request
    public JSONNode jsonResult;
    public int Local_appVersion_code;

    //public GameObject botbar;
    // create the web request and download handler
    //UnityWebRequest webReq = new UnityWebRequest();

    string rawJson;


    //public 



    //public GameObject home_screen;
    //public Image profile_piiiiccccccc;



    //public bool success;
    public int Total_lvl_server;
    public long fb_coin;
    public long guest_coin;
    public long credits;
    public long currentxp;
    public int app_version;
    public int is_undermaintenance;
    public string app_update_message;
    public string android_app_link;
    public string ios_app_link;
    public string privacy_policy;
    public string app_message;
    public string fb_share_link_title;
    public string share_private_link_message;
    public string terms_and_conditions;
    public int watch_video_coin;
    public bool is_daily_bonus;


    public string bit_coin_value;
    public int cashout_coin_value;
    public bool is_cashout_done;
    public bool cashout_enabled = false;

    public bool req_call = true;

    public string updated_level;

    //userdata
    public string Username;
    public string profile_pic_link;
    public long wallet_coin;
    public int current_xp;
    public int is_unlocked = 0;
    public int is_blocked = 0;
    public int start;
    public int end;
    public string token;
    private void Awake()
    {
        //    if (Instance != null && Instance != this)
        //    {
        //        Destroy(this.gameObject);
        //    }
        //    else
        //    {
        //        Instance = this;
        //}
    }


    private void Start()
    {
    }
    public void Get_settings_data()
    {
        //Shop_API_script.Instance.Get_settings_data();
        //Debug.Log("getdata");
        StartCoroutine("GetData");

    }



    // sends an API request - returns a JSON file
    void Manual_Start()
    {

        Debug.Log(rawJson);
        jsonResult = JSON.Parse(rawJson);
        Set_Json_data();
        Invoke("temp_waiter", 0.5f);


    }
    public void temp_waiter()
    {
        if (is_undermaintenance == 0)
        {
            if (Version_check())
            {
                //temp_before_login_start
                if (is_blocked == 0)
                {
                    Normal_start();
                }
            }
            else
            {
                UPdate_app_notice();
            }
        }
        else
        {
            Undermaintenance_Notice();
        }
    }


    public void Set_Json_data()
    {

        is_undermaintenance = jsonResult["data"]["app_config"]["is_undermaintenance"].AsInt;
        app_message = jsonResult["data"]["app_config"]["app_message"].Value;
        android_app_link = jsonResult["data"]["app_config"]["android_app_link"].Value;
        ios_app_link = jsonResult["data"]["app_config"]["ios_app_link"].Value;
        privacy_policy = jsonResult["data"]["app_config"]["privacy_policy"].Value;
        terms_and_conditions = jsonResult["data"]["app_config"]["privacy_policy"].Value;
        fb_share_link_title = jsonResult["data"]["app_config"]["fb_share_link_title"].Value;
        watch_video_coin = jsonResult["data"]["app_config"]["watch_video_coin"].AsInt;
        bit_coin_value = jsonResult["data"]["app_config"]["bit_coin_value"].Value;
        cashout_coin_value = jsonResult["data"]["app_config"]["cashout_coin_value"].AsInt;
        //userDATA
        is_blocked = jsonResult["data"]["user"]["is_blocked"].AsInt;
        Username = jsonResult["data"]["user"]["name"].Value;
        PlayerPrefs.SetString("Username", Username);
        wallet_coin = jsonResult["data"]["user"]["wallet_coin"].AsLong;
        PlayerPrefs.SetInt("Gems", (int)wallet_coin);
        PlayerPrefs.Save();
        //InitScriptName.InitScript.Instance.SetGems((int)wallet_coin);
        current_xp = jsonResult["data"]["user"]["current_xp"].AsInt;
        is_cashout_done = jsonResult["data"]["user"]["is_cashout_done"].AsBool;

        cashout_enabled = jsonResult["data"]["app_config"]["android_cashout_enabled"].AsBool;
        if (cashout_enabled)
        {
            if (Cashout_Panel_script.Instance != null)
            {
                Cashout_Panel_script.Instance.Cashout_but.gameObject.SetActive(true);
                Cashout_Panel_script.Instance.but_change();
            }
        }
        else
        {
            if (Cashout_Panel_script.Instance != null)
            {
                Cashout_Panel_script.Instance.Cashout_but.gameObject.SetActive(false);
            }
        }
        is_unlocked = jsonResult["data"]["user"]["is_unlocked"].AsInt;
        //is_daily_bonus = jsonResult["data"]["user"]["is_daily_bonus"].AsBool;
        if (is_daily_bonus)
        {
            //disable_daily_bonus
            //Gamemanager.Instance.transform.GetComponent<DailyReward>().disable_dialy_bonus_fromgetapi_data();
        }
        else
        {
            //Gamemanager.Instance.transform.GetComponent<DailyReward>().enable_dialy_bonus_fromgetapi_data();
        }
        //Debug.Log("is_unlocked"+is_unlocked);

        if (is_blocked == 0)
        {
            //Set_User_data.Instance.setdata_after_get();
        }
        else if (is_blocked == 1)
        {
            //Notice_panel_script.Instance.notice_Panel.SetActive(true);
            Notice_panel_script.Instance.Blocked_panel.SetActive(true);
        }
    }
    public void Normal_start()
    {

        //if (PlayerPrefs.GetInt("facebook") == 1)
        //{
        //    fblogin.I.fb_but.SetActive(false);
        //}

        //home_screen.SetActive(true);
        //Splash_Panel_scripts.I.bgimage.SetActive(false);
        //Ads_priority_script.Instance.Show_interrestial();
        //Notice_panel_script.Instance.notice_Panel.SetActive(false);
        Notice_panel_script.Instance.Loading_panel.SetActive(false);
        //botbar.SetActive(true);
        //Debug.Log("called");
        //#if 

        //#endif
        //Gamemanager.Instance.Total_level_final = Total_lvl_server;
        if (is_unlocked == 1)
        {
            //Gamemanager.Instance.Unlocked_all_lvl_test = true;
        }
        //Gamemanager.Instance.manual_Start();
        //Gamemanager.Instance.Generate_shop_panel();
        //Gamemanager.Instance.gameObject.transform.GetComponent<InAppManager>().manual_start();
        //startup_purchase_card();
        //Gamemanager.Instance.ads_controller.GetComponent<AdsController>().Manual_Start();
        //manual_start all ads
        //Manual_start_ALL_ADS();
    }

    public void startup_purchase_card()
    {
        if (PlayerPrefs.GetInt("CurrentXP") > 10000)
        {
            //Gamemanager.Instance.turnon_startup_purchase_panel();
        }
    }

    //public void Manual_start_ALL_ADS()
    //{
    //    Gamemanager.Instance.ads_controller.GetComponent<AdsController>().Manual_Start();
    //    Gamemanager.Instance.ads_controller.GetComponent<Unity_Ads_script>().Manual_Start();
    //    Gamemanager.Instance.ads_controller.GetComponent<FB_ADS_script>().Manual_Start();

    //}

    public void SEt_all_ads_ids()
    {
        //Gamemanager.Instance.ads_controller.GetComponent<>
    }


    public bool Version_check()
    {
        app_version = jsonResult["data"]["app_config"]["app_version"].AsInt;
        //Debug.Log(Application.version);
        if (Local_appVersion_code >= app_version)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //public static bool IsConnected(string hostedURL = "http://www.google.com")
    //{
    //    try
    //    {
    //        string HtmlText = GetHtmlFromUri(hostedURL);
    //        if (HtmlText == "")
    //            return false;
    //        else
    //            return true;
    //    }
    //    catch (IOException ex)
    //    {
    //        return false;
    //    }
    //}

    IEnumerator GetData()
    {
        UnityWebRequest www = UnityWebRequest.Get(Firebase_custome_script.Instance.server_link + url);
        req_call = false;
        www.SetRequestHeader("Authorization", PlayerPrefs.GetString("Token"));
        token = PlayerPrefs.GetString("Token");
        yield return www.Send();
        //Debug.Log(www);
        if (www.isNetworkError)
        {
            Debug.Log(www.error);


            Check_Internet_connection();
            req_call = true;
        }
        else
        {
            close_Retry_panel();
            // Show results as text
            //Debug.Log(www.downloadHandler.text);
            rawJson = www.downloadHandler.text;
            Manual_Start();
            // Or retrieve results as binary data
            // byte[] results = www.downloadHandler.data;
        }
    }


    // Update is called once per frame
    void Update()
    {
        //if (req_call)
        //{
        //    StartCoroutine("GetData");
        //}
    }

    public void Undermaintenance_Notice()
    {
        //Notice_panel_script.Instance.notice_Panel.SetActive(true);
        Notice_panel_script.Instance.Undermaintenance_panel.GetComponentInChildren<TextMeshProUGUI>().text = app_message;
        Notice_panel_script.Instance.Undermaintenance_panel.GetComponentInChildren<Button>().onClick.AddListener(() => Close_app());
        Notice_panel_script.Instance.Undermaintenance_panel.SetActive(true);

    }



    public void UPdate_app_notice()
    {
        //Notice_panel_script.Instance.notice_Panel.SetActive(true);
        Notice_panel_script.Instance.Update_Panel.GetComponentInChildren<TextMeshProUGUI>().text = app_message;
        android_app_link = jsonResult["data"]["app_config"]["android_app_link"].Value;
        Notice_panel_script.Instance.Update_Panel.GetComponentInChildren<Button>().onClick.AddListener(() => Update_app());
        Notice_panel_script.Instance.Update_Panel.SetActive(true);

    }
    public void Close_app()
    {
        Application.Quit();
    }

    public void Update_app()
    {

        print("called");
#if UNITY_ANDROID
        Application.OpenURL(android_app_link);
#elif UNITY_IOS
        Application.OpenURL(ios_app_link);
#else
        Application.OpenURL(android_app_link);
#endif
    }
    public void Check_Internet_connection()
    {
        StartCoroutine(Ck_net(isConnected =>
        {
            if (isConnected)
            {
                Debug.Log("Internet Available!");
                //return true;
                if (!Notice_panel_script.Instance.Loading_panel.activeSelf)
                {
                    //Notice_panel_script.Instance.notice_Panel.SetActive(true);
                    Notice_panel_script.Instance.Something_wentwrong_panel.GetComponentInChildren<Button>().onClick.AddListener(() => Retry_connection());
                    Notice_panel_script.Instance.Something_wentwrong_panel.SetActive(true);
                }
            }
            else
            {
                Debug.Log("Internet Not Available");
                //return  false;
                if (!Notice_panel_script.Instance.Loading_panel.activeSelf)
                {
                    //Notice_panel_script.Instance.notice_Panel.SetActive(true);
                    Notice_panel_script.Instance.No_internet_Panel.GetComponentInChildren<Button>().onClick.AddListener(() => Retry_connection());
                    Notice_panel_script.Instance.No_internet_Panel.SetActive(true);
                }
            }
        }));
    }

    public IEnumerator Ck_net(Action<bool> syncResult)
    {
        const string echoServer = "http://google.com";

        bool result;
        using (var request = UnityWebRequest.Head(echoServer))
        {
            request.timeout = 0;
            yield return request.SendWebRequest();
            result = !request.isNetworkError && !request.isHttpError && request.responseCode == 200;
        }
        syncResult(result);
    }
    //public bool check_net()
    //{
    //    WWW www = new WWW("http://www.google.com");
    //    //yield return www;
    //    if (www.error != null)
    //    {
    //        return false;
    //    }
    //    else
    //    {
    //        return true;
    //    }

    //}
    public void Retry_connection()
    {
        Get_settings_data();
        Notice_panel_script.Instance.Something_wentwrong_panel.SetActive(false);
        Notice_panel_script.Instance.No_internet_Panel.SetActive(false);
        Notice_panel_script.Instance.Loading_panel.SetActive(true);
        StartCoroutine("Retry_connection_coroutine");
    }
    IEnumerator Retry_connection_coroutine()
    {
        yield return new WaitForSeconds(3f);
        //StartCoroutine("check_connection");
        Notice_panel_script.Instance.Loading_panel.SetActive(false);
        //Notice_panel_script.Instance.notice_Panel.SetActive(false);
        Get_settings_data();
    }
    public void close_Retry_panel()
    {
        Notice_panel_script.Instance.Something_wentwrong_panel.SetActive(false);
        Notice_panel_script.Instance.No_internet_Panel.SetActive(false);
        Notice_panel_script.Instance.Loading_panel.SetActive(false);
        //Notice_panel_script.Instance.notice_Panel.SetActive(false);
    }


    //public void Check_updated_lvl()
    //{
    //    if (PlayerPrefs.GetString("updated_level", "0") == updated_level)
    //    {

    //    }
    //    else
    //    {
    //        string full_str = updated_level;
    //        string[] az = full_str.Split(',');
    //        foreach (string a in az)
    //        {
    //            FileChk_lvl(a);
    //        }
    //        PlayerPrefs.SetString("updated_level", updated_level);
    //    }
    //}

    public void FileChk_lvl(string lvl_name)
    {
        if (!Directory.Exists(Application.persistentDataPath + "/Levels"))
        {
            //if it doesn't, create it
            Directory.CreateDirectory(Application.persistentDataPath + "/Levels");

        }

        else
        {
            string filePath = Application.persistentDataPath + "/Levels/" + lvl_name + ".lvl";

            if (System.IO.File.Exists(filePath))
            {
                // The file exists -> run event
                File.Delete(filePath);
            }
            else
            {
                // The file does not exist -> run event
            }
        }
    }

    protected override void OnApplicationQuitCallback()
    {
        //throw new NotImplementedException();
    }

    protected override void OnEnableCallback()
    {
        //throw new NotImplementedException();
    }
}




