﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using SimpleJSON;
using UnityEngine.UI;

public class Ads_API : Singleton<Ads_API>
{
    //public static Ads_API Instance;
    private string rawJson;
    public JSONNode jsonResult;
    string url = "/api/ads";
    public string android_admob_interstitial_id;
    public string ios_admob_interstitial_id;
    public string android_admob_reward_id;
    public string ios_admob_reward_id;

    public string android_appopen_admob_id;
    public string ios_appopen_admob_id;


    public string android_facebook_reward_id;
    public string android_facebook_interstitial_id;
    public string ios_facebook_reward_id;
    public string ios_facebook_interstitial_id;


    public string android_unity_game_id;
    public string ios_unity_game_id;


    public string ads_priority;
    public string interstitial_ads_priority;
    public int ads_click;

    public string ios_ads_priority;
    public string ios_interstitial_ads_priority;
    public int ios_ads_click;

    private void Awake()
    {
        //if (Instance != null && Instance != this)
        //{
        //    Destroy(this.gameObject);
        //}

        //else
        //{
        //    Instance = this;
        //    //DontDestroyOnLoad(this.gameObject);

        //}
        //Instance = this;
    }


    // Start is called before the first frame update
    void Start()
    {
        //get_data();
    }

    public void get_data()
    {
        StartCoroutine("GetData");
    }
    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator GetData()
    {
        UnityWebRequest www = UnityWebRequest.Get(Firebase_custome_script.Instance.server_link + url);
        //req_call = false;
        //www.SetRequestHeader("Authorization", PlayerPrefs.GetString("Token"));
        yield return www.Send();
        //Debug.Log(www);
        if (www.isNetworkError)
        {
            Debug.Log(www.error);


            Check_Internet_connection();
            //req_call = true;
        }
        else
        {
            close_Retry_panel();
            // Show results as text
            //Debug.Log(www.downloadHandler.text);
            rawJson = www.downloadHandler.text;
            Manual_Start();
            // Or retrieve results as binary data
            // byte[] results = www.downloadHandler.data;
        }
    }

    #region Internet check
    public void Check_Internet_connection()
    {
        StartCoroutine(Ck_net(isConnected =>
        {
            if (isConnected)
            {
                Debug.Log("Internet Available!");
                //return true;
                if (!Notice_panel_script.Instance.Loading_panel.activeSelf)
                {
                    //Notice_panel_script.Instance.notice_Panel.SetActive(true);
                    Notice_panel_script.Instance.Something_wentwrong_panel.GetComponentInChildren<Button>().onClick.AddListener(() => Retry_connection());
                    Notice_panel_script.Instance.Something_wentwrong_panel.SetActive(true);
                }
            }
            else
            {
                Debug.Log("Internet Not Available");
                //return  false;
                if (!Notice_panel_script.Instance.Loading_panel.activeSelf)
                {
                    //Notice_panel_script.Instance.notice_Panel.SetActive(true);
                    Notice_panel_script.Instance.No_internet_Panel.GetComponentInChildren<Button>().onClick.AddListener(() => Retry_connection());
                    Notice_panel_script.Instance.No_internet_Panel.SetActive(true);
                }
            }
        }));
    }

    public IEnumerator Ck_net(Action<bool> syncResult)
    {
        const string echoServer = "http://google.com";

        bool result;
        using (var request = UnityWebRequest.Head(echoServer))
        {
            request.timeout = 0;
            yield return request.SendWebRequest();
            result = !request.isNetworkError && !request.isHttpError && request.responseCode == 200;
        }
        syncResult(result);
    }


    //}
    public void Retry_connection()
    {
        get_data();
        Notice_panel_script.Instance.Something_wentwrong_panel.SetActive(false);
        Notice_panel_script.Instance.No_internet_Panel.SetActive(false);
        Notice_panel_script.Instance.Loading_panel.SetActive(true);
        StartCoroutine("Retry_connection_coroutine");
    }
    IEnumerator Retry_connection_coroutine()
    {
        yield return new WaitForSeconds(3f);
        //StartCoroutine("check_connection");
        Notice_panel_script.Instance.Loading_panel.SetActive(false);
        //Notice_panel_script.Instance.notice_Panel.SetActive(false);
        get_data();
    }
    public void close_Retry_panel()
    {
        Notice_panel_script.Instance.Something_wentwrong_panel.SetActive(false);
        Notice_panel_script.Instance.No_internet_Panel.SetActive(false);
        Notice_panel_script.Instance.Loading_panel.SetActive(false);
        //Notice_panel_script.Instance.notice_Panel.SetActive(false);
    }
    #endregion


    public void Manual_Start()
    {
        jsonResult = JSON.Parse(rawJson);
        Set_Json_data();
    }

    public void Set_Json_data()
    {


        //ADS

        //admob
        android_admob_interstitial_id = jsonResult["data"]["ads"]["android_admob_interstitial_id"].Value;
        ios_admob_interstitial_id = jsonResult["data"]["ads"]["ios_admob_interstitial_id"].Value;
        android_admob_reward_id = jsonResult["data"]["ads"]["android_admob_reward_id"].Value;
        ios_admob_reward_id = jsonResult["data"]["ads"]["ios_admob_reward_id"].Value;

        android_appopen_admob_id= jsonResult["data"]["ads"]["android_appopen_admob_id"].Value;
        ios_appopen_admob_id = jsonResult["data"]["ads"]["ios_appopen_admob_id"].Value;

        ////fb
        android_facebook_interstitial_id = jsonResult["data"]["ads"]["android_facebook_interstitial_id"].Value;
        android_facebook_reward_id = jsonResult["data"]["ads"]["android_facebook_reward_id"].Value;

        ios_facebook_interstitial_id = jsonResult["data"]["ads"]["ios_facebook_interstitial_id"].Value;
        ios_facebook_reward_id = jsonResult["data"]["ads"]["ios_facebook_reward_id"].Value;

        ////unity
        android_unity_game_id = jsonResult["data"]["ads"]["android_unity_game_id"].Value;
        ios_unity_game_id = jsonResult["data"]["ads"]["ios_unity_game_id"].Value;



        ads_click = jsonResult["data"]["ads"]["ads_click"].AsInt;
        
        ads_priority = jsonResult["data"]["ads"]["ads_priority"].Value;
        interstitial_ads_priority = jsonResult["data"]["ads"]["interstitial_ads_priority"].Value;

        ios_ads_click = jsonResult["data"]["ads"]["ios_ads_click"].AsInt;

        ios_ads_priority = jsonResult["data"]["ads"]["ios_ads_priority"].Value;
        ios_interstitial_ads_priority = jsonResult["data"]["ads"]["ios_interstitial_ads_priority"].Value;
        //SEt ALL IDS TO scripts
        Manual_start_ALL_ADS();
    }

    public void Manual_start_ALL_ADS()
    {
        Ads_priority_script.Instance.Initialize_ads();
        //Gamemanager.Instance.ads_controller.GetComponent<AdsController>().Manual_Start();
        //Gamemanager.Instance.ads_controller.GetComponent<Unity_Ads_script>().Manual_Start();
        //Gamemanager.Instance.ads_controller.GetComponent<FB_ADS_script>().Manual_Start();

    }

    protected override void OnApplicationQuitCallback()
    {
        //throw new NotImplementedException();
    }

    protected override void OnEnableCallback()
    {
        //throw new NotImplementedException();
    }
}
