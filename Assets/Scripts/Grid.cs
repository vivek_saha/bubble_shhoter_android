using System;
using UnityEngine;

public class Grid : MonoBehaviour
{
	public GameObject Busy
	{
		get
		{
			return this.busy;
		}
		set
		{
			if (value != null && value.GetComponent<ball>() != null && !value.GetComponent<ball>().NotSorting)
			{
				value.GetComponent<ball>().mesh = base.gameObject;
				if (value.tag == "chicken")
				{
					value.GetComponent<SpriteRenderer>().sortingOrder = 100;
				}
			}
			this.busy = value;
		}
	}

	private void Start()
	{
	}

	private void Update()
	{
		if (this.busy == null)
		{
			if (base.name == "boxCatapult" && !Grid.waitForAnim)
			{
				GameObject gameObject = this.boxSecond;
				GameObject gameObject2 = gameObject.GetComponent<Grid>().busy;
				if (gameObject2 != null)
				{
					gameObject2.GetComponent<bouncer>().bounceToCatapult(base.transform.position);
					gameObject2.GetComponent<Animation>().Play("ball_appier");
					this.busy = gameObject2;
				}
			}
			else if (base.name == "boxSecond" && !Grid.waitForAnim)
			{
				if ((GamePlay.Instance.GameStatus == GameState.Playing || GamePlay.Instance.GameStatus == GameState.Win || GamePlay.Instance.GameStatus == GameState.WaitForChicken) && LevelData.LimitAmount > 0f)
				{
					this.busy = Camera.main.GetComponent<mainscript>().createFirstBall(base.transform.position);
				}
			}
			else if (!(base.name == "boxFirst") || !Grid.waitForAnim)
			{
			}
		}
		if (this.busy != null && !Grid.waitForAnim)
		{
			if (base.name == "boxCatapult")
			{
				if (this.busy.GetComponent<ball>().setTarget)
				{
					this.busy = null;
				}
			}
			else if (base.name == "boxFirst")
			{
				if (Vector3.Distance(base.transform.position, this.busy.transform.position) > 2f)
				{
					this.busy = null;
				}
			}
			else if (base.name == "boxSecond" && Vector3.Distance(base.transform.position, this.busy.transform.position) > 0.9f)
			{
				this.busy = null;
			}
		}
	}

	public void BounceFrom(GameObject box)
	{
		GameObject x = box.GetComponent<Grid>().busy;
		if (x != null && this.busy != null)
		{
			this.busy.GetComponent<bouncer>().bounceTo(box.transform.position);
			box.GetComponent<Grid>().busy = this.busy;
			this.busy = x;
		}
	}

	private void setColorTag(GameObject ball)
	{
		if (ball.name.IndexOf("Orange") > -1)
		{
			ball.tag = "Fixed";
		}
		else if (ball.name.IndexOf("Red") > -1)
		{
			ball.tag = "Fixed";
		}
		else if (ball.name.IndexOf("Yellow") > -1)
		{
			ball.tag = "Fixed";
		}
	}

	private void OnCollisionStay2D(Collision2D other)
	{
		if (other.gameObject.name.IndexOf("ball") > -1 && this.busy == null)
		{
			MonoBehaviour.print("ohter=========>" + other.gameObject.name);
			this.busy = other.gameObject;
		}
	}

	private void OnTriggerExit(Collider other)
	{
	}

	public void destroy()
	{
		base.tag = "Mesh";
		UnityEngine.Object.Destroy(this.busy);
		this.busy = null;
	}

	[SerializeField]
	private GameObject busy;

	private GameObject[] meshes;

	private bool destroyed;

	public float offset;

	private bool triggerball;

	public GameObject boxFirst;

	public GameObject boxSecond;

	public static bool waitForAnim;
}
