using System;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBarScript : MonoBehaviour
{
	private void Start()
	{
		ProgressBarScript.Instance = this;
		this.slider = base.GetComponent<Slider>();
		this.maxWidth = this.slider.value;
		this.ResetBar();
		this.PrepareStars();
	}

	public void UpdateDisplay(float x)
	{
		this.slider.value = this.maxWidth * x;
		if (this.maxWidth * x >= this.maxWidth)
		{
			this.slider.value = this.maxWidth;
		}
	}

	public void AddValue(float x)
	{
		this.UpdateDisplay(this.slider.value * 100f / this.maxWidth / 100f + x);
	}

	private void Update()
	{
	}

	public bool IsFull()
	{
		if (this.slider.value >= this.maxWidth)
		{
			this.ResetBar();
			return true;
		}
		return false;
	}

	public void ResetBar()
	{
		this.UpdateDisplay(0f);
	}

	private void PrepareStars()
	{
		float width = base.GetComponent<RectTransform>().rect.width;
		base.transform.Find("Star1").localPosition = new Vector3((float)(LevelData.star1 * 100 / LevelData.star3) * width / 100f - width / 2f, base.transform.Find("Star1").localPosition.y, 0f);
		base.transform.Find("Star2").localPosition = new Vector3((float)(LevelData.star2 * 100 / LevelData.star3) * width / 100f - width / 2f, base.transform.Find("Star2").localPosition.y, 0f);
	}

	private Slider slider;

	public static ProgressBarScript Instance;

	private float maxWidth;
}
