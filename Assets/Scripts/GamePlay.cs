using System;
using System.Collections;
using InitScriptName;
using UnityEngine;
using UnityEngine.UI;

public class GamePlay : MonoBehaviour
{
	public GameState GameStatus
	{
		get
		{
			return GamePlay.Instance.gameStatus;
		}
		set
		{
			if (GamePlay.Instance.gameStatus != value)
			{
				if (value == GameState.Win)
				{
					if (!this.winStarted)
					{
						base.StartCoroutine(this.WinAction());
					}
				}
				else if (value == GameState.GameOver)
				{
					base.StartCoroutine(this.LoseAction());
				}
				else if (value == GameState.Tutorial && this.gameStatus != GameState.Playing)
				{
					value = GameState.Playing;
					this.gameStatus = value;
				}
				else if (value == GameState.PreTutorial && this.gameStatus != GameState.Playing)
				{
					base.StartCoroutine(this.ShowPreTutorial());
				}
			}
			if (value == GameState.WaitAfterClose)
			{
				base.StartCoroutine(this.WaitAfterClose());
			}
			if (value == GameState.Tutorial && this.gameStatus != GameState.Playing)
			{
				GamePlay.Instance.gameStatus = value;
			}
			GamePlay.Instance.gameStatus = value;
		}
	}

	private void Start()
	{
		GamePlay.Instance = this;
		GamePlay.GamePreover = 0;
	}

	private void Update()
	{
		if (Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.OSXEditor)
		{
			if (UnityEngine.Input.GetKey(KeyCode.W))
			{
				GamePlay.Instance.GameStatus = GameState.Win;
			}
			if (UnityEngine.Input.GetKey(KeyCode.L))
			{
				LevelData.LimitAmount = 0f;
				GamePlay.Instance.GameStatus = GameState.GameOver;
			}
			if (UnityEngine.Input.GetKey(KeyCode.D))
			{
				mainscript.Instance.destroyAllballs();
			}
			if (UnityEngine.Input.GetKey(KeyCode.M))
			{
				LevelData.LimitAmount = 1f;
			}
		}
	}

	private IEnumerator PauseButtonPopOutCorutine(GameObject game)
	{
		yield return new WaitForSeconds(0.6f);
		game.SetActive(false);
		yield break;
	}

	private IEnumerator WinAction()
	{
		this.winStarted = true;
		InitScript.Instance.AddLife(1);
		GameObject.Find("paotong").transform.rotation = Quaternion.Euler(0f, 0f, 0f);
		GameObject.Find("Canvas").transform.Find("Option").GetComponent<Animator>().enabled = true;
		GameObject.Find("Canvas").transform.Find("Option").GetComponent<Animator>().Play("OptionPauseAnim", -1, 0f);
		GameObject.Find("Canvas").transform.Find("LevelCleared").gameObject.SetActive(true);
		GameObject.Find("CanvasMoves").transform.Find("GameObject").GetComponent<Button>().interactable = false;
		if (mainscript.Instance.PauseMenu.activeSelf)
		{
			mainscript.Instance.PauseMenu.GetComponent<Animator>().Play("PauseMenuPopOut", -1, 0f);
			base.StartCoroutine(this.PauseButtonPopOutCorutine(mainscript.Instance.PauseMenu));
		}
		if (mainscript.Instance.BoostPanel.activeSelf)
		{
			mainscript.Instance.BoostPanel.SetActive(false);
		}
		if (mainscript.Instance.perfect.activeSelf)
		{
			mainscript.Instance.perfect.SetActive(false);
		}
		if (mainscript.Instance.Fire)
		{
			mainscript.Instance.Fire.SetActive(false);
		}
		SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.winSound);
		yield return new WaitForSeconds(1f);
		if (LevelData.mode == ModeGame.Vertical)
		{
			yield return new WaitForSeconds(1f);
			GameObject.Find("CanvasPots").transform.Find("Black").gameObject.SetActive(false);
			yield return new WaitForSeconds(0.5f);
		}
		foreach (GameObject gameObject in GameObject.FindGameObjectsWithTag("Ball"))
		{
			gameObject.GetComponent<ball>().StartFall();
		}
		Transform b = GameObject.Find("-Ball").transform;
		ball[] balls = GameObject.Find("-Ball").GetComponentsInChildren<ball>();
		foreach (ball ball in balls)
		{
			ball.StartFall();
		}
		while (LevelData.LimitAmount > 0f)
		{
			if (mainscript.Instance.boxCatapult.GetComponent<Grid>().Busy != null)
			{
				LevelData.LimitAmount -= 1f;
				ball component = mainscript.Instance.boxCatapult.GetComponent<Grid>().Busy.GetComponent<ball>();
				mainscript.Instance.boxCatapult.GetComponent<Grid>().Busy = null;
				component.transform.parent = mainscript.Instance.Balls;
				component.tag = "Ball";
				component.PushBallAFterWin();
			}
			yield return new WaitForEndOfFrame();
		}
		foreach (ball ball2 in balls)
		{
			if (ball2 != null)
			{
				ball2.StartFall();
			}
		}
		yield return new WaitForSeconds(2f);
		while (GameObject.FindGameObjectsWithTag("Ball").Length > 0)
		{
			yield return new WaitForSeconds(0.1f);
		}
		SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.aplauds);
		if (PlayerPrefs.GetInt(string.Format("Level.{0:000}.StarsCount", mainscript.Instance.currentLevel), 0) < mainscript.Instance.stars)
		{
			PlayerPrefs.SetInt(string.Format("Level.{0:000}.StarsCount", mainscript.Instance.currentLevel), mainscript.Instance.stars);
		}
		if (PlayerPrefs.GetInt("Score" + mainscript.Instance.currentLevel) < mainscript.Score)
		{
			PlayerPrefs.SetInt("Score" + mainscript.Instance.currentLevel, mainscript.Score);
		}
		GameObject.Find("Canvas").transform.Find("MenuComplete").gameObject.SetActive(true);
		if (mainscript.Instance.Fire.activeSelf)
		{
			mainscript.Instance.Fire.SetActive(false);
		}
		yield break;
	}

	private void ShowTutorial()
	{
	}

	private IEnumerator ShowPreTutorial()
	{
		yield return new WaitForSeconds(1f);
		GameObject.Find("Canvas").transform.Find("PreTutorial").gameObject.SetActive(true);
		yield break;
	}

	private IEnumerator LoseAction()
	{
		SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.OutOfMoves);
		mainscript.Instance.Layer.SetActive(true);
		GameObject.Find("Canvas").transform.Find("OutOfMoves").gameObject.SetActive(true);
		GameObject.Find("Canvas").transform.Find("Option").GetComponent<Animator>().enabled = true;
		GameObject.Find("Canvas").transform.Find("Option").GetComponent<Animator>().Play("OptionPauseAnim", -1, 0f);
		GameObject.Find("CanvasMoves").transform.Find("GameObject").GetComponent<Button>().interactable = false;
		yield return new WaitForSeconds(1.5f);
		GameObject.Find("Canvas").transform.Find("OutOfMoves").gameObject.SetActive(false);
		if (LevelData.LimitAmount <= 0f)
		{
			GamePlay.GamePreover++;
			if (GamePlay.GamePreover <= 3)
			{
				GameObject gameObject = GameObject.Find("Canvas").transform.Find("MenuPreGameOver").gameObject;
				gameObject.SetActive(true);
				gameObject.transform.Find("Image").transform.Find("ReleasePanel").gameObject.SetActive(false);
				gameObject.transform.Find("Image").transform.Find("BlackTransparent").gameObject.SetActive(false);
				gameObject.transform.Find("Image").transform.Find("Clock").GetChild(0).GetComponent<Image>().fillAmount = 0f;
				gameObject.transform.Find("Image").transform.Find("Clock").GetComponent<AudioSource>().Play();
				base.StartCoroutine(this.Clockanim());
			}
			else
			{
				AnimationManager.instance.ShowGameOver();
			}
		}
		yield return new WaitForSeconds(0.1f);
		yield break;
	}

	private IEnumerator Clockanim()
	{
		yield return new WaitForSeconds(0.01f);
		GameObject MenuPreGameOver = GameObject.Find("Canvas").transform.Find("MenuPreGameOver").gameObject;
		MenuPreGameOver.transform.GetChild(0).transform.Find("Clock").GetChild(0).GetComponent<Image>().fillAmount += 0.001f;
		base.StartCoroutine(this.Clockanim());
		if (MenuPreGameOver.transform.GetChild(0).transform.Find("Clock").GetChild(0).GetComponent<Image>().fillAmount >= 1f)
		{
			base.StopCoroutine(this.Clockanim());
			MenuPreGameOver.transform.GetChild(0).transform.Find("Close").gameObject.SetActive(true);
			MenuPreGameOver.transform.Find("Image").transform.Find("Clock").GetComponent<AudioSource>().Stop();
		}
		yield break;
	}

	private IEnumerator WaitAfterClose()
	{
		yield return new WaitForSeconds(1f);
		this.GameStatus = GameState.Playing;
		yield break;
	}

	public static GamePlay Instance;

	private GameState gameStatus;

	private bool winStarted;

	public float counter;

	public int PotScriptSound;

	public static int GamePreover;
}
