﻿using GoogleMobileAds.Api;
using GoogleMobileAds.Common;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class Admob : MonoBehaviour
{
    //private BannerView bannerView;
    private InterstitialAd interstitialAd;

    private RewardedAd rewardedAd;

    private AppOpenAd appopenAd;

    //private RewardedInterstitialAd rewardedInterstitialAd;
    //private float deltaTime;
    public GameObject admob_bg;

    //public GameObject daily_Loading_panel;
    //public GameObject daily_noice_panel;
    public UnityEvent OnAdLoadedEvent;

    public UnityEvent OnAdFailedToLoadEvent;
    public UnityEvent OnAdOpeningEvent;
    public UnityEvent OnAdFailedToShowEvent;
    public UnityEvent OnUserEarnedRewardEvent;
    public UnityEvent OnAdClosedEvent;
    public UnityEvent OnAdLeavingApplicationEvent;

    public bool onstart_appopen = true;

    public bool Daily_reward = false;

    public string android_admob_interstitial_id;
    public string ios_admob_interstitial_id;
    public string android_admob_reward_id;
    public string ios_admob_reward_id;

    public string android_admob_app_open_id;
    public string ios_admob_app_open_id;


    private bool isShowingAd = false;

    #region UNITY MONOBEHAVIOR METHODS

    public void Manual_Start()
    {
        //get ids from server

#if UNITY_ANDROID

        android_admob_interstitial_id = Ads_API.Instance.android_admob_interstitial_id;

        android_admob_reward_id = Ads_API.Instance.android_admob_reward_id;

        android_admob_app_open_id = Ads_API.Instance.android_appopen_admob_id;

#elif UNITY_IOS

        ios_admob_interstitial_id = Ads_API.Instance.ios_admob_interstitial_id;

        ios_admob_reward_id = Ads_API.Instance.ios_admob_reward_id;

        ios_admob_app_open_id = Ads_API.Instance.ios_appopen_admob_id;
#else

        android_admob_interstitial_id = Ads_API.Instance.android_admob_interstitial_id;

        android_admob_reward_id = Ads_API.Instance.android_admob_reward_id;

        android_admob_app_open_id = Ads_API.Instance.android_appopen_admob_id;

#endif
        MobileAds.SetiOSAppPauseOnBackground(true);

        //List<String> deviceIds = new List<String>() { AdRequest.TestDeviceSimulator };

        // Add some test device IDs (replace with your own device IDs).
        //#if UNITY_IPHONE
        //        deviceIds.Add("96e23e80653bb28980d3f40beb58915c");
        //#elif UNITY_ANDROID
        //        deviceIds.Add("75EF8D155528C04DACBBA6F36F433035");
        //#endif

        //        // Configure TagForChildDirectedTreatment and test device IDs.
        //        RequestConfiguration requestConfiguration =
        //            new RequestConfiguration.Builder()
        //            .SetTagForChildDirectedTreatment(TagForChildDirectedTreatment.Unspecified)
        //            .SetTestDeviceIds(deviceIds).build();

        //MobileAds.SetRequestConfiguration(requestConfiguration);

        // Initialize the Google Mobile Ads SDK.
        //MobileAds.Initialize(HandleInitCompleteAction);

        MobileAds.Initialize(initstatus => { });
        LoadAppOpenAd();
        RequestAndLoadInterstitialAd();
        RequestAndLoadRewardedAd();

    }

    private void HandleInitCompleteAction(InitializationStatus initstatus)
    {
        // Callbacks from GoogleMobileAds are not guaranteed to be called on
        // main thread.
        // In this example we use MobileAdsEventExecutor to schedule these calls on
        // the next Update() loop.
        MobileAdsEventExecutor.ExecuteInUpdate(() =>
        {
            //statusText.text = "Initialization complete";
            //RequestBannerAd();
            if (android_admob_interstitial_id != null)
            {
                RequestAndLoadInterstitialAd();
            }
            if (android_admob_reward_id != null)
            {
                RequestAndLoadRewardedAd();
            }
        });
    }

    private void Update()
    {
    }

    #endregion UNITY MONOBEHAVIOR METHODS

    //#region HELPER METHODS

    //private AdRequest CreateAdRequest()
    //{
    //    return new AdRequest.Builder()
    //        //.AddTestDevice(AdRequest.TestDeviceSimulator)
    //        //.AddTestDevice("0123456789ABCDEF0123456789ABCDEF")
    //        //.AddKeyword("unity-admob-sample")
    //        //.TagForChildDirectedTreatment(false)
    //        .AddExtra("color_bg", "9B30FF")
    //        .Build();
    //}

    //#endregion


    #region AppOpenAd
    public void LoadAppOpenAd()
    {

#if UNITY_EDITOR
        string adUnitId = "unused";
#elif UNITY_ANDROID
            string adUnitId = android_admob_app_open_id;
#elif UNITY_IOS
            string adUnitId = ios_admob_app_open_id;
#else
            string adUnitId = "unexpected_platform";
#endif
        AdRequest request = new AdRequest.Builder().Build();

        // Load an app open ad for portrait orientation
        AppOpenAd.LoadAd(adUnitId, ScreenOrientation.Portrait, request, ((appOpenAd, error) =>
        {
            if (error != null)
            {
                // Handle the error.
                Debug.LogFormat("Failed to load the ad. (reason: {0})", error.LoadAdError.GetMessage());
                //if (Splash_panel_script.Instance != null)
                //{
                //    Gamemanager.Instance.API.GetComponent<Setting_api_script>().splash_done = true;
                //    if (Splash_panel_script.Instance.Splash_slider.value == 1)
                //    {
                //        Splash_panel_script.Instance.gameObject.SetActive(false);

                //    }
                //}
                if (Firebase_custome_script.Instance.play_but != null)
                {
                    Firebase_custome_script.Instance.Splash_loading.SetActive(false);
                    Firebase_custome_script.Instance.play_but.SetActive(true);
                }
                return;
            }

            // App open ad is loaded.
            appopenAd = appOpenAd;
            if (onstart_appopen)
            {
                onstart_appopen = false;
                ShowAdIfAvailable();
            }
        }));
    }

    private bool IsAdAvailable
    {
        get
        {
            return appopenAd != null;
        }
    }

    public void ShowAdIfAvailable()
    {
        if (!IsAdAvailable || isShowingAd)
        {
            //if (Splash_panel_script.Instance != null)
            //{
            //    Splash_panel_script.Instance.gameObject.SetActive(false);
            //}
            return;
        }

        appopenAd.OnAdDidDismissFullScreenContent += HandleAdDidDismissFullScreenContent;
        appopenAd.OnAdFailedToPresentFullScreenContent += HandleAdFailedToPresentFullScreenContent;
        appopenAd.OnAdDidPresentFullScreenContent += HandleAdDidPresentFullScreenContent;
        appopenAd.OnAdDidRecordImpression += HandleAdDidRecordImpression;
        appopenAd.OnPaidEvent += HandlePaidEvent;

        appopenAd.Show();
    }

    private void HandleAdDidDismissFullScreenContent(object sender, EventArgs args)
    {
        Debug.Log("Closed app open ad");
        // Set the ad to null to indicate that AppOpenAdManager no longer has another ad to show.
        appopenAd = null;
        isShowingAd = false;
        LoadAppOpenAd();
        if (Firebase_custome_script.Instance.play_but != null)
        {
            Firebase_custome_script.Instance.Splash_loading.SetActive(false);
            Firebase_custome_script.Instance.play_but.SetActive(true);
        }
        //if (Splash_panel_script.Instance != null)
        //{
        //    Gamemanager.Instance.API.GetComponent<Setting_api_script>().splash_done = true;
        //    if (Splash_panel_script.Instance.Splash_slider.value == 1)
        //    {
        //        Splash_panel_script.Instance.gameObject.SetActive(false);

        //    }
        //}
    }

    private void HandleAdFailedToPresentFullScreenContent(object sender, AdErrorEventArgs args)
    {
        Debug.LogFormat("Failed to present the ad (reason: {0})", args.AdError.GetMessage());
        // Set the ad to null to indicate that AppOpenAdManager no longer has another ad to show.
        appopenAd = null;
        LoadAppOpenAd();
        if (Firebase_custome_script.Instance.play_but != null)
        {
            Firebase_custome_script.Instance.Splash_loading.SetActive(false);
            Firebase_custome_script.Instance.play_but.SetActive(true);
        }
        //if (Splash_panel_script.Instance != null)
        //{
        //    Gamemanager.Instance.API.GetComponent<Setting_api_script>().splash_done = true;
        //    if (Splash_panel_script.Instance.Splash_slider.value == 1)
        //    {
        //        Splash_panel_script.Instance.gameObject.SetActive(false);

        //    }
        //}
    }

    private void HandleAdDidPresentFullScreenContent(object sender, EventArgs args)
    {
        Debug.Log("Displayed app open ad");
        isShowingAd = true;
    }

    private void HandleAdDidRecordImpression(object sender, EventArgs args)
    {
        Debug.Log("Recorded ad impression");
    }

    private void HandlePaidEvent(object sender, AdValueEventArgs args)
    {
        Debug.LogFormat("Received paid event. (currency: {0}, value: {1}",
                args.AdValue.CurrencyCode, args.AdValue.Value);
    }



    public void OnApplicationPause(bool paused)
    {
        // Display the app open ad when the app is foregrounded
        if (!paused)
        {
            if (Ads_priority_script.Instance.show_normal_ads)
            {
                Ads_priority_script.Instance.show_normal_ads = false;
            }
            else
            {

                ShowAdIfAvailable();
            }
        }
    }
    #endregion


    #region INTERSTITIAL ADS

    public void RequestAndLoadInterstitialAd()
    {
        //statusText.text = "Requesting Interstitial Ad.";

#if UNITY_ANDROID
        string adUnitId = android_admob_interstitial_id;
#elif UNITY_IOS
        string adUnitId = ios_admob_interstitial_id;
#else
        string adUnitId = "unexpected_platform";
#endif

        // Clean up interstitial before using it
        if (interstitialAd != null)
        {
            interstitialAd.Destroy();
        }

        interstitialAd = new InterstitialAd(adUnitId);

        // Add Event Handlers
        interstitialAd.OnAdLoaded += (sender, args) => OnAdLoadedEvent.Invoke();
        interstitialAd.OnAdFailedToLoad += (sender, args) => OnAdFailedToLoadEvent.Invoke();
        interstitialAd.OnAdOpening += (sender, args) => OnAdOpeningEvent.Invoke();
        interstitialAd.OnAdClosed += (sender, args) => OnAdClosedEvent.Invoke();
        //interstitialAd.OnAdLeavingApplication += (sender, args) => OnAdLeavingApplicationEvent.Invoke();

        //// Load an interstitial ad
        //interstitialAd.LoadAd(CreateAdRequest());
        AdRequest request = new AdRequest.Builder().Build();
        //// Load the interstitial with the request.
        interstitialAd.LoadAd(request);
    }

    public bool check_loaded_InterstitialAd()
    {
        Debug.Log(interstitialAd.IsLoaded());

        return interstitialAd.IsLoaded();
    }

    public void ShowInterstitialAd()
    {
       
        admob_bg.SetActive(true);
        interstitialAd.Show();
    }

    public void DestroyInterstitialAd()
    {
        if (interstitialAd != null)
        {
            interstitialAd.Destroy();
        }
    }

    #endregion INTERSTITIAL ADS

    #region REWARDED ADS

    public void RequestAndLoadRewardedAd()
    {
        //statusText.text = "Requesting Rewarded Ad.";

#if UNITY_ANDROID
        string adUnitId = android_admob_reward_id;
#elif UNITY_IOS
        string adUnitId = ios_admob_reward_id;
#else
        string adUnitId = "unexpected_platform";
#endif

        // create new rewarded ad instance
        rewardedAd = new RewardedAd(adUnitId);

        // Add Event Handlers
        rewardedAd.OnAdLoaded += (sender, args) => OnAdLoadedEvent.Invoke();
        rewardedAd.OnAdFailedToLoad += (sender, args) => OnAdFailedToLoadEvent.Invoke();
        rewardedAd.OnAdOpening += (sender, args) => OnAdOpeningEvent.Invoke();
        rewardedAd.OnAdFailedToShow += (sender, args) => OnAdFailedToShowEvent.Invoke();
        rewardedAd.OnAdClosed += (sender, args) => OnAdClosedEvent.Invoke();
        rewardedAd.OnUserEarnedReward += (sender, args) => OnUserEarnedRewardEvent.Invoke();

        // Create empty ad request
        //rewardedAd.LoadAd(CreateAdRequest());
        AdRequest request = new AdRequest.Builder().Build();
        //// Load the rewarded ad with the request.
        rewardedAd.LoadAd(request);
    }

    public bool check_loaded_rewarded()
    {
        return rewardedAd.IsLoaded();
    }

    public void ShowRewardedAd()
    {
        if (rewardedAd != null)
        {
           
            admob_bg.SetActive(true);
            rewardedAd.Show();
        }
        else
        {
            //statusText.text = "Rewarded ad is not ready yet.";
        }
    }

    public void Reward_money_Add()
    {
        
        Ads_priority_script.Instance.Add_reward();
        Uploaddata_server();
    }


    public void Uploaddata_server()
    {
        //Update_API.Instance.is_update = 1;
        Update_API.Instance.type = "watchVideo";
        //Update_API.Instance.credits = ;
        //Update_API.Instance.xp = GameManager.Instance.Level_var;
        Update_API.Instance.Update_data();
    }
    #endregion REWARDED ADS

    public void REquest_ads()
    {
        Invoke("waitted_req", 0.5f);
    }

    public void waitted_req()
    {
        if (android_admob_interstitial_id != null)
        {
            if (!check_loaded_InterstitialAd())
            {
                RequestAndLoadInterstitialAd();
            }
        }
        if (android_admob_reward_id != null)
        {
            if (!check_loaded_rewarded())
            {
                RequestAndLoadRewardedAd();
            }
        }
        if (Ads_API.Instance.android_facebook_interstitial_id != null)
        {
            if (!transform.GetComponent<FB_ADS_script>().Check_interstatial_fb_ads())
            {
                transform.GetComponent<FB_ADS_script>().LoadInterstitial();
            }
        }
        if (Ads_API.Instance.android_facebook_reward_id != null)
        {
            if (!transform.GetComponent<FB_ADS_script>().Check_rewarded_fb_ads())
            {
                transform.GetComponent<FB_ADS_script>().LoadRewardedVideo();
            }
        }
    }

    public void Disable_admob_bg()
    {
        
        admob_bg.SetActive(false);
    }

    
}