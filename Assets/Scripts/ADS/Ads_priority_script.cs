﻿using System.Collections;
using UnityEngine;

public enum Ads_reward_type { GameoverPre, SpinGift, Boostpanel , FreeCoin , doublexCoin}
public class Ads_priority_script : Singleton<Ads_priority_script>
{
    //public static Ads_priority_script Instance;
    public Ads_reward_type localsave;
    //public GameObject popup_watchads;
     private BoostType BT;
    private int count_boost;

    public bool show_normal_ads = false;
    GameObject level_of_double_reward;
    private void Awake()
    {
        //if (Instance != null && Instance != this)
        //{
        //    Destroy(this.gameObject);
        //}

        //else
        //{
        //    Instance = this;
        //    DontDestroyOnLoad(this.gameObject);

        //}
    }

    // Start is called before the first frame update
    private void Start()
    {
    }

    // Update is called once per frame
    private void Update()
    {
    }

    public void watch_video(Ads_reward_type ac)
    {
        //localsave = Ads_reward_type.Boostpanel;
        localsave = ac;
        Show_RewardedAds();
        
    }


    public void Double_reward(GameObject a)
    {
        level_of_double_reward = a;
        localsave = Ads_reward_type.doublexCoin;
        Show_RewardedAds();
    }
    public void Buy_boost_watch_video(BoostType boostType, int count)
    {
        localsave = Ads_reward_type.Boostpanel;
        BT = boostType;
        count_boost = count;
        Show_RewardedAds();
    }

    private void Watch_video_reward_get(Ads_reward_type ac)
    {
        switch (ac)
        {
            case Ads_reward_type.GameoverPre:
                AnimationManager.instance.Watch_Video_reward();
                break;
            case Ads_reward_type.SpinGift:
                //addgem
                break;
            case Ads_reward_type.Boostpanel:
                InitScriptName.InitScript.Instance.BuyBoost_watch_Video(BT, count_boost);
                break;
            case Ads_reward_type.FreeCoin:
                InitScriptName.InitScript.Instance.AddGems(Setting_API.Instance.watch_video_coin);
                if(gemshop_script.Instance != null)
                    gemshop_script.Instance.gemshopp.SetActive(false);

                Uploaddata_server();
                break;

            case Ads_reward_type.doublexCoin:
                level_of_double_reward.GetComponent<Level>().double_coin = true;
                CanvasManager.instance.close_double_coin_panel();
                    break;
            default:
                break;
        }
    }

    public void Initialize_ads()
    {
        GetComponent<Admob>().Manual_Start();
        GetComponent<Unity_Ads_script>().Manual_Start();
        GetComponent<FB_ADS_script>().Manual_Start();
    }

    public void Show_interrestial()
    {
        show_normal_ads = true;
        if (Setting_API.Instance.cashout_enabled)
        {
            if (Ads_API.Instance.interstitial_ads_priority == "admob")
            {
                if (transform.GetComponent<Admob>().check_loaded_InterstitialAd())
                {
                    transform.GetComponent<Admob>().ShowInterstitialAd();
                }
                else if (transform.GetComponent<FB_ADS_script>().Check_interstatial_fb_ads())
                {
                    transform.GetComponent<FB_ADS_script>().ShowInterstitial();
                }
                else if (transform.GetComponent<Unity_Ads_script>().check_unity_interstatial())
                {
                    transform.GetComponent<Unity_Ads_script>().ShowInterstitialAd();
                }
                else
                {
                    show_normal_ads = false;
                }
            }
            else if (Ads_API.Instance.interstitial_ads_priority == "fb")
            {
                if (transform.GetComponent<FB_ADS_script>().Check_interstatial_fb_ads())
                {
                    transform.GetComponent<FB_ADS_script>().ShowInterstitial();
                }
                else if (transform.GetComponent<Unity_Ads_script>().check_unity_interstatial())
                {
                    transform.GetComponent<Unity_Ads_script>().ShowInterstitialAd();
                }
                else if (transform.GetComponent<Admob>().check_loaded_InterstitialAd())
                {
                    transform.GetComponent<Admob>().ShowInterstitialAd();
                }
                else
                {
                    show_normal_ads = false;
                }
            }
            else if (Ads_API.Instance.interstitial_ads_priority == "unity")
            {
                if (transform.GetComponent<Unity_Ads_script>().check_unity_interstatial())
                {
                    transform.GetComponent<Unity_Ads_script>().ShowInterstitialAd();
                }
                else if (transform.GetComponent<FB_ADS_script>().Check_interstatial_fb_ads())
                {
                    transform.GetComponent<FB_ADS_script>().ShowInterstitial();
                }
                else if (transform.GetComponent<Admob>().check_loaded_InterstitialAd())
                {
                    transform.GetComponent<Admob>().ShowInterstitialAd();
                }
                else
                {
                    show_normal_ads = false;
                }
            }
        }
    }

    public void Show_RewardedAds()
    {
        show_normal_ads = true;
        if (Ads_API.Instance.ads_priority == "admob")
        {
            if (transform.GetComponent<Admob>().check_loaded_rewarded())
            {
                transform.GetComponent<Admob>().ShowRewardedAd();
            }
            else if (transform.GetComponent<FB_ADS_script>().Check_rewarded_fb_ads())
            {
                transform.GetComponent<FB_ADS_script>().ShowRewardedVideo();
            }
            else if (transform.GetComponent<Unity_Ads_script>().check_unity_rewarded())
            {
                transform.GetComponent<Unity_Ads_script>().ShowRewardedVideo();
            }
            else
            {
                show_normal_ads = false;
                show_popup();
            }
        }
        else if (Ads_API.Instance.ads_priority == "fb")
        {
            if (transform.GetComponent<FB_ADS_script>().Check_rewarded_fb_ads())
            {
                transform.GetComponent<FB_ADS_script>().ShowRewardedVideo();
            }
            else if (transform.GetComponent<Unity_Ads_script>().check_unity_rewarded())
            {
                transform.GetComponent<Unity_Ads_script>().ShowRewardedVideo();
            }
            else if (transform.GetComponent<Admob>().check_loaded_rewarded())
            {
                transform.GetComponent<Admob>().ShowRewardedAd();
            }
            else
            {
                show_normal_ads = false;
                show_popup();
            }
        }
        else if (Ads_API.Instance.ads_priority == "unity")
        {
            if (transform.GetComponent<Unity_Ads_script>().check_unity_rewarded())
            {
                transform.GetComponent<Unity_Ads_script>().ShowRewardedVideo();
            }
            else if (transform.GetComponent<FB_ADS_script>().Check_rewarded_fb_ads())
            {
                transform.GetComponent<FB_ADS_script>().ShowRewardedVideo();
            }
            else if (transform.GetComponent<Admob>().check_loaded_rewarded())
            {
                transform.GetComponent<Admob>().ShowRewardedAd();
            }
            else
            {
                show_normal_ads = false;
                show_popup();
            }
        }
    }

    public void Add_reward()
    {
        if (Time.deltaTime == 0)
        {
            StartCoroutine("rewardss_get");

        }
        else
        {
            Invoke("rewardsss", 0.5f);
        }
    }


    IEnumerator rewardss_get()
    {
        yield return new WaitForSecondsRealtime(0.5f);
        GetComponent<Admob>().Disable_admob_bg();
        Watch_video_reward_get(localsave);
        //popup_panel_script.Instance.Ondone();
        GetComponent<Admob>().waitted_req();
    }

    public void rewardsss()
    {
        GetComponent<Admob>().Disable_admob_bg();
        Watch_video_reward_get(localsave);
        //popup_panel_script.Instance.Ondone();
        GetComponent<Admob>().waitted_req();


    }


    public void Uploaddata_server()
    {
        //Update_API.Instance.is_update = 1;
        Update_API.Instance.type = "watchVideo";
        //Update_API.Instance.credits = ;
        //Update_API.Instance.xp = GameManager.Instance.Level_var;
        Update_API.Instance.Update_data();
    }
    public void show_popup()
    {
        //GameManager.Instance.nem_popup_fun("Ads Are Not Ready");
        CanvasManager.instance._ShowAndroidToastMessage("Ads Are Not Ready...");
    }

    public void hide_popup()
    {
        //popup_watchads.SetActive(false);
    }

    protected override void OnApplicationQuitCallback()
    {
        //throw new System.NotImplementedException();
    }

    protected override void OnEnableCallback()
    {
        //throw new System.NotImplementedException();
    }
}