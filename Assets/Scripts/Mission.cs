using System;

public class Mission
{
	public Mission(int pAmount, MissionType missionType)
	{
		this.amount = pAmount;
		this.type = missionType;
	}

	public MissionType type;

	public int amount;
}
