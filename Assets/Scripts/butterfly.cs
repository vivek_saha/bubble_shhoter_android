using System;
using System.Collections;
using UnityEngine;

public class butterfly : MonoBehaviour
{
	private void Start()
	{
		this.isPaused = Camera.main.GetComponent<mainscript>().isPaused;
		this.arcadeMode = Camera.main.GetComponent<mainscript>().arcadeMode;
		if (this.revertButterFly == 1)
		{
			base.transform.position = new Vector3(3f, (float)UnityEngine.Random.Range(-2, 5), -20f);
			this.flyTo(new Vector3(base.transform.position.x - 18f, base.transform.position.y, base.transform.localScale.z));
		}
		else
		{
			base.transform.localScale = new Vector3(base.transform.localScale.x * -1f, base.transform.localScale.y, base.transform.localScale.z);
			base.transform.position = new Vector3(-3f, (float)UnityEngine.Random.Range(-2, 5), -20f);
			this.flyTo(new Vector3(base.transform.position.x + 18f, base.transform.position.y, base.transform.localScale.z));
		}
	}

	private void Update()
	{
		if (base.transform.position.x < -4f || base.transform.position.x > 4f)
		{
			UnityEngine.Object.Destroy(base.gameObject);
		}
	}

	public void flyTo(Vector3 vector)
	{
		this.bounceTo(vector);
	}

	public void OnTriggerEnter2D(Collider2D owner)
	{
		if (owner.gameObject.name.IndexOf("ball") == 0 && owner.gameObject.GetComponent<ball>().setTarget)
		{
			UnityEngine.Object.Destroy(base.gameObject);
		}
	}

	public void bounceTo(Vector3 vector3)
	{
		vector3 = new Vector3(vector3.x, vector3.y, base.gameObject.transform.position.z);
		this.tempPosition = base.transform.position;
		this.targetPrepare = vector3;
		this.startTime = Time.time;
		base.StartCoroutine(this.bonceCoroutine());
	}

	private IEnumerator bonceCoroutine()
	{
		float speed = 1f;
		if (this.arcadeMode)
		{
			speed = 0.3f;
		}
		while (Vector3.Distance(base.transform.position, this.targetPrepare) > 1f && !this.isPaused)
		{
			base.transform.position = Vector3.Lerp(this.tempPosition, this.targetPrepare, (Time.time - this.startTime) * speed);
			yield return new WaitForSeconds(0.02f);
		}
		yield break;
	}

	private Vector3 tempPosition;

	private Vector3 targetPrepare;

	private bool started;

	private bool isPaused;

	private float startTime;

	private bool arcadeMode;

	public int revertButterFly;
}
