using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bug : MonoBehaviour
{
	private void Start()
	{
		this.spiders = GameObject.Find("Spiders").transform;
		this.ChangeColor(0);
		if (mainscript.Instance.ComboCount % 3 == 0 && mainscript.Instance.ComboCount > 0)
		{
			this.ChangeColor(1);
		}
		if (mainscript.Instance.ComboCount % 5 == 0 && mainscript.Instance.ComboCount > 0)
		{
			this.ChangeColor(2);
		}
		this.SelectPlace();
	}

	private void SelectPlace()
	{
		List<Transform> list = new List<Transform>();
		IEnumerator enumerator = this.spiders.GetEnumerator();
		try
		{
			while (enumerator.MoveNext())
			{
				object obj = enumerator.Current;
				Transform transform = (Transform)obj;
				if (transform.childCount == 0)
				{
					list.Add(transform);
				}
			}
		}
		finally
		{
			IDisposable disposable;
			if ((disposable = (enumerator as IDisposable)) != null)
			{
				disposable.Dispose();
			}
		}
		base.StartCoroutine(this.MoveToPlace(list[UnityEngine.Random.Range(0, list.Count)]));
	}

	private IEnumerator MoveToPlace(Transform place)
	{
		base.transform.parent = place;
		AnimationCurve curveX = new AnimationCurve(new Keyframe[]
		{
			new Keyframe(0f, base.transform.localPosition.x),
			new Keyframe(1f, 0f)
		});
		AnimationCurve curveY = new AnimationCurve(new Keyframe[]
		{
			new Keyframe(0f, base.transform.localPosition.y),
			new Keyframe(1f, 0f)
		});
		curveY.AddKey(0.5f, base.transform.localPosition.y + 3f);
		float startTime = Time.time;
		Vector3 startPos = base.transform.localPosition;
		float speed = UnityEngine.Random.Range(0.4f, 0.6f);
		float distCovered = 0f;
		while (distCovered < 1f)
		{
			if (distCovered > 0.8f && distCovered < 0.81f)
			{
				SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.swish[0]);
			}
			distCovered = (Time.time - startTime) * speed;
			base.transform.localPosition = new Vector3(curveX.Evaluate(distCovered), curveY.Evaluate(distCovered), 0f);
			yield return new WaitForEndOfFrame();
		}
		base.GetComponent<CircleCollider2D>().isTrigger = false;
		yield break;
	}

	public void MoveOut()
	{
		base.transform.parent = null;
		base.StartCoroutine(this.MoveOutCor());
	}

	private IEnumerator MoveOutCor()
	{
		SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.bugDissapier);
		AnimationCurve curveX = new AnimationCurve(new Keyframe[]
		{
			new Keyframe(0f, base.transform.localPosition.x),
			new Keyframe(1f, 0f)
		});
		AnimationCurve curveY = new AnimationCurve(new Keyframe[]
		{
			new Keyframe(0f, base.transform.localPosition.y),
			new Keyframe(1f, 10f)
		});
		curveY.AddKey(0.5f, base.transform.localPosition.y + 3f);
		float startTime = Time.time;
		Vector3 startPos = base.transform.localPosition;
		float speed = 0.6f;
		float distCovered = 0f;
		while (distCovered < 1f)
		{
			distCovered = (Time.time - startTime) * speed;
			base.transform.localPosition = new Vector3(base.transform.localPosition.x, curveY.Evaluate(distCovered), 0f);
			yield return new WaitForEndOfFrame();
		}
		base.transform.parent = null;
		UnityEngine.Object.Destroy(base.gameObject);
		yield break;
	}

	public void ChangeColor(int i)
	{
		this.color = i;
		base.GetComponent<SpriteRenderer>().sprite = this.textures[i];
	}

	private void OnCollisionEnter2D(Collision2D col)
	{
		if (col.collider.name.Contains("ball"))
		{
			base.StartCoroutine(this.SoundsCounter());
			if (mainscript.Instance.bugSounds < 5)
			{
				SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.bug);
			}
			col.collider.GetComponent<ball>().HitBug1++;
			mainscript.Instance.PopupScore(col.collider.GetComponent<ball>().HitBug1 * this.score * (this.color + 1), base.transform.position);
			base.StartCoroutine(this.StartAnim(col.collider.transform.position));
			if (this.color == 2)
			{
				SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.combo[5]);
				this.ChangeColor(1);
				MonoBehaviour.print("------->create ball");
				GameObject gameObject = creatorBall.Instance.createBall(base.transform.position + Vector3.up * 1f, BallColor.random, false, 1, 0);
				gameObject.GetComponent<ball>().StartFall();
			}
		}
	}

	private IEnumerator SoundsCounter()
	{
		mainscript.Instance.bugSounds++;
		yield return new WaitForSeconds(0.3f);
		mainscript.Instance.bugSounds--;
		yield break;
	}

	private IEnumerator StartAnim(Vector3 dir)
	{
		AnimationCurve curveScale = new AnimationCurve(new Keyframe[]
		{
			new Keyframe(0f, 1f),
			new Keyframe(1f, 0.8f)
		});
		AnimationCurve curveScaleReverse = new AnimationCurve(new Keyframe[]
		{
			new Keyframe(0f, 0.8f),
			new Keyframe(1f, 1f)
		});
		AnimationCurve curveScaleX = new AnimationCurve(new Keyframe[]
		{
			new Keyframe(0f, 1f),
			new Keyframe(1f, 1.2f)
		});
		AnimationCurve curveScaleReverseX = new AnimationCurve(new Keyframe[]
		{
			new Keyframe(0f, 1.2f),
			new Keyframe(1f, 1f)
		});
		dir = base.transform.position - dir;
		dir = dir.normalized;
		dir = base.transform.localPosition + dir * 0.5f;
		float startTime = Time.time;
		Vector3 startPos = base.transform.localPosition;
		float speed = 25f;
		float distCovered = 0f;
		while (distCovered < 1f && !float.IsNaN(dir.x))
		{
			distCovered = (Time.time - startTime) * speed;
			if (this == null)
			{
				yield break;
			}
			base.transform.localPosition = Vector3.Lerp(startPos, dir, distCovered);
			base.transform.localScale = new Vector3(curveScaleX.Evaluate(distCovered), curveScale.Evaluate(distCovered), 1f);
			yield return new WaitForEndOfFrame();
		}
		Vector3 lastPos = base.transform.localPosition;
		startTime = Time.time;
		distCovered = 0f;
		while (distCovered < 1f && !float.IsNaN(dir.x))
		{
			distCovered = (Time.time - startTime) * speed;
			if (this == null)
			{
				yield break;
			}
			base.transform.localPosition = Vector3.Lerp(lastPos, Vector3.zero, distCovered);
			base.transform.localScale = new Vector3(curveScaleReverseX.Evaluate(distCovered), curveScaleReverse.Evaluate(distCovered), 1f);
			yield return new WaitForEndOfFrame();
		}
		yield break;
	}

	private Transform spiders;

	public Sprite[] textures;

	public int color;

	private int score = 25;
}
