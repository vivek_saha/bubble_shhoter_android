using System;
using System.Collections;
using InitScriptName;
using UnityEngine;
using UnityEngine.EventSystems;

public class ball : MonoBehaviour
{
	public bool Destroyed
	{
		get
		{
			return this.destroyed;
		}
		set
		{
			if (value)
			{
				base.GetComponent<BoxCollider2D>().enabled = false;
			}
			this.destroyed = value;
		}
	}

	public int HitBug1
	{
		get
		{
			return this.HitBug;
		}
		set
		{
			if (value < 3)
			{
				this.HitBug = value;
			}
		}
	}

	private void Start()
	{
		this.rabbit = GameObject.Find("Rabbit").gameObject.GetComponent<Animation>();
		this.meshPos = new Vector3(-1000f, -1000f, -10f);
		this.dropTarget = base.transform.position;
		this.Meshes = GameObject.Find("-Ball");
		this.bottomBorder = Camera.main.GetComponent<mainscript>().bottomBorder;
		this.topBorder = Camera.main.GetComponent<mainscript>().topBorder;
		this.leftBorder = Camera.main.GetComponent<mainscript>().leftBorder;
		this.rightBorder = Camera.main.GetComponent<mainscript>().rightBorder;
		this.gameOverBorder = Camera.main.GetComponent<mainscript>().gameOverBorder;
		this.gameOver = Camera.main.GetComponent<mainscript>().gameOver;
		this.isPaused = Camera.main.GetComponent<mainscript>().isPaused;
		this.dropedDown = Camera.main.GetComponent<mainscript>().dropingDown;
	}

	private IEnumerator AllowLaunchBall()
	{
		yield return new WaitForSeconds(2f);
		mainscript.StopControl = false;
		yield break;
	}

	public void PushBallAFterWin()
	{
		base.GetComponent<BoxCollider2D>().offset = Vector2.zero;
		base.GetComponent<BoxCollider2D>().size = new Vector2(0.5f, 0.5f);
		this.setTarget = true;
		this.startTime = Time.time;
		this.target = Vector3.zero;
		base.Invoke("StartFall", 0.4f);
	}

	private void Update()
	{
		if (Input.GetMouseButtonUp(0) && DrawLine.BallPrepare)
		{
			GameObject gameObject = base.gameObject;
			if (!this.ClickOnGUI(UnityEngine.Input.mousePosition) && !this.launched && !gameObject.GetComponent<ball>().setTarget && mainscript.Instance.newBall2 == null && this.newBall && !Camera.main.GetComponent<mainscript>().gameOver && (GamePlay.Instance.GameStatus == GameState.Playing || GamePlay.Instance.GameStatus == GameState.WaitForChicken))
			{
				Vector3 v = Camera.main.ScreenToWorldPoint(UnityEngine.Input.mousePosition);
				this.worldPos = v;
				if ((double)this.worldPos.y > -4.601994 && !mainscript.StopControl)
				{
					MonoBehaviour.print("---------->if");
					this.launched = true;
					this.rabbit.Play("rabbit_move");
					SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.BallFire, 2f);
					this.mTouchOffsetX = this.worldPos.x - gameObject.transform.position.x;
					this.mTouchOffsetY = this.worldPos.y - gameObject.transform.position.y;
					this.xOffset = Mathf.Cos(Mathf.Atan2(this.mTouchOffsetY, this.mTouchOffsetX));
					this.yOffset = Mathf.Sin(Mathf.Atan2(this.mTouchOffsetY, this.mTouchOffsetX));
					this.speed = new Vector2(this.xOffset, this.yOffset);
					if (!this.fireBall)
					{
						base.GetComponent<CircleCollider2D>().enabled = true;
					}
					this.target = this.worldPos;
					this.setTarget = true;
					this.startTime = Time.time;
					this.dropTarget = base.transform.position;
					InitScript.Instance.BoostActivated = false;
					mainscript.Instance.newBall = base.gameObject;
					mainscript.Instance.newBall2 = base.gameObject;
					base.GetComponent<Rigidbody2D>().AddForce(this.target - this.dropTarget, ForceMode2D.Force);
					GamePlay.Instance.counter = 0f;
					GameObject.Find("paotong").GetComponent<Animator>().enabled = true;
					GameObject.Find("paotong").GetComponent<Animator>().Play("PaotongAnim", -1, 0f);
					base.gameObject.GetComponent<SpriteRenderer>().sortingOrder = -1;
					DrawLine.BallPrepare = false;
					base.StartCoroutine(mainscript.Instance.Set_false_layer());
				}
				else
				{
					MonoBehaviour.print("---------->else");
					mainscript.Instance.Layer.SetActive(false);
				}
			}
		}
		if (base.transform.position != this.target && this.setTarget && !this.stopedBall && !this.isPaused && Camera.main.GetComponent<mainscript>().dropDownTime < Time.time)
		{
			float num = Vector3.Magnitude(base.GetComponent<Rigidbody2D>().velocity);
			if (num > 20f)
			{
				float d = num / 20f;
				base.GetComponent<Rigidbody2D>().velocity /= d;
			}
			else if (num < 15f)
			{
				float num2 = num / 15f;
				if (num2 != 0f)
				{
					base.GetComponent<Rigidbody2D>().velocity /= num2;
				}
			}
			if (base.GetComponent<Rigidbody2D>().velocity.y < 1.5f && base.GetComponent<Rigidbody2D>().velocity.y > 0f)
			{
				base.GetComponent<Rigidbody2D>().velocity = new Vector2(base.GetComponent<Rigidbody2D>().velocity.x, 1.7f);
			}
		}
		if (this.setTarget)
		{
			this.triggerEnter();
		}
		if ((base.transform.position.y <= -10f || base.transform.position.y >= 5f) && this.fireBall && !this.Destroyed)
		{
			mainscript.Instance.CheckFreeChicken();
			this.setTarget = false;
			this.launched = false;
			this.DestroySingle(base.gameObject, 1E-05f);
			mainscript.Instance.checkBall = base.gameObject;
		}
	}

	private bool ClickOnGUI(Vector3 mousePos)
	{
		EventSystem current = EventSystem.current;
		return current.IsPointerOverGameObject();
	}

	public void SetBoost(BoostType boostType)
	{
		base.tag = "Ball";
		base.GetComponent<SpriteRenderer>().sprite = this.boosts[boostType - BoostType.ColorBallBoost];
		if (boostType == BoostType.ColorBallBoost)
		{
		}
		if (boostType == BoostType.FireBallBoost)
		{
			base.GetComponent<SpriteRenderer>().sortingOrder = 10;
			base.GetComponent<CircleCollider2D>().enabled = false;
			this.fireBall = true;
			this.fireballArray.Add(base.gameObject);
		}
	}

	private void FixedUpdate()
	{
		if (Camera.main.GetComponent<mainscript>().gameOver)
		{
			return;
		}
		if (this.stopedBall)
		{
			base.transform.position = this.meshPos;
			this.stopedBall = false;
			if (this.newBall)
			{
				this.newBall = false;
				base.gameObject.layer = 9;
				Camera.main.GetComponent<mainscript>().checkBall = base.gameObject;
				base.enabled = false;
			}
		}
	}

	public GameObject findInArrayGameObject(ArrayList b, GameObject destObj)
	{
		IEnumerator enumerator = b.GetEnumerator();
		try
		{
			while (enumerator.MoveNext())
			{
				object obj = enumerator.Current;
				GameObject gameObject = (GameObject)obj;
				if (gameObject == destObj)
				{
					return gameObject;
				}
			}
		}
		finally
		{
			IDisposable disposable;
			if ((disposable = (enumerator as IDisposable)) != null)
			{
				disposable.Dispose();
			}
		}
		return null;
	}

	public bool findInArray(ArrayList b, GameObject destObj)
	{
		IEnumerator enumerator = b.GetEnumerator();
		try
		{
			while (enumerator.MoveNext())
			{
				object obj = enumerator.Current;
				GameObject x = (GameObject)obj;
				if (x == destObj)
				{
					return true;
				}
			}
		}
		finally
		{
			IDisposable disposable;
			if ((disposable = (enumerator as IDisposable)) != null)
			{
				disposable.Dispose();
			}
		}
		return false;
	}

	public ArrayList addFrom(ArrayList b, ArrayList b2)
	{
		IEnumerator enumerator = b.GetEnumerator();
		try
		{
			while (enumerator.MoveNext())
			{
				object obj = enumerator.Current;
				GameObject gameObject = (GameObject)obj;
				if (!this.findInArray(b2, gameObject))
				{
					b2.Add(gameObject);
				}
			}
		}
		finally
		{
			IDisposable disposable;
			if ((disposable = (enumerator as IDisposable)) != null)
			{
				disposable.Dispose();
			}
		}
		return b2;
	}

	public void changeNearestColor()
	{
		GameObject gameObject = GameObject.Find("Creator");
		Collider2D[] array = Physics2D.OverlapCircleAll(base.transform.position, 0.5f, 512);
		foreach (Collider2D collider2D in array)
		{
			gameObject.GetComponent<creatorBall>().createBall(collider2D.transform.position, BallColor.random, false, 1, 0);
			MonoBehaviour.print("=====333333333333===>");
			UnityEngine.Object.Destroy(collider2D.gameObject);
		}
	}

	public void checkNextNearestColor(ArrayList b, int counter)
	{
		Vector3 localScale = base.transform.localScale;
		int layerMask = 1 << LayerMask.NameToLayer("Ball");
		Collider2D[] array = Physics2D.OverlapCircleAll(base.transform.position, 1f, layerMask);
		foreach (Collider2D collider2D in array)
		{
			if (collider2D.gameObject.tag == base.tag)
			{
				GameObject gameObject = collider2D.gameObject;
				float num = Vector3.Distance(base.transform.position, gameObject.transform.position);
				if (num <= 1f && !this.findInArray(b, gameObject))
				{
					counter++;
					b.Add(gameObject);
					gameObject.GetComponent<bouncer>().checkNextNearestColor(b, counter);
				}
			}
		}
	}

	public void checkNearestColor()
	{
		int counter = 0;
		GameObject[] array = UnityEngine.Object.FindObjectsOfType(typeof(GameObject)) as GameObject[];
		foreach (GameObject gameObject in array)
		{
			if (gameObject.layer == 9 && gameObject.name.IndexOf("Rainbow") > -1)
			{
				gameObject.tag = base.tag;
			}
		}
		ArrayList arrayList = new ArrayList();
		arrayList.Add(base.gameObject);
		Vector3 localScale = base.transform.localScale;
		GameObject[] array3 = GameObject.FindGameObjectsWithTag(base.tag);
		foreach (GameObject gameObject2 in array3)
		{
			float num = Vector3.Distance(base.transform.position, gameObject2.transform.position);
			if (num <= 0.9f && num > 0f)
			{
				arrayList.Add(gameObject2);
				gameObject2.GetComponent<bouncer>().checkNextNearestColor(arrayList, counter);
			}
		}
		mainscript.Instance.countOfPreparedToDestroy = arrayList.Count;
		if (arrayList.Count >= 3)
		{
			mainscript.Instance.ComboCount++;
			this.destroy(arrayList, 1E-05f);
			mainscript.Instance.CheckFreeChicken();
		}
		if (arrayList.Count < 3)
		{
			Camera.main.GetComponent<mainscript>().bounceCounter++;
			mainscript.Instance.ComboCount = 0;
		}
		arrayList.Clear();
		Camera.main.GetComponent<mainscript>().dropingDown = false;
		this.FindLight(base.gameObject);
	}

	public void StartFall()
	{
		base.enabled = false;
		if (this.mesh != null)
		{
			this.mesh.GetComponent<Grid>().Busy = null;
		}
		if (base.gameObject == null)
		{
			return;
		}
		if (LevelData.mode != ModeGame.Vertical || !this.isTarget)
		{
			if (LevelData.mode == ModeGame.Animals && this.isTarget)
			{
				base.StartCoroutine(this.FlyToTarget());
			}
		}
		this.setTarget = false;
		base.transform.SetParent(null);
		base.gameObject.layer = 13;
		base.gameObject.tag = "Ball";
		base.gameObject.GetComponent<BoxCollider2D>().sharedMaterial = this.Bounce1;
		base.gameObject.GetComponent<CircleCollider2D>().sharedMaterial = this.Bounce1;
		int childCount = base.gameObject.transform.childCount;
		if (childCount > 0)
		{
			base.gameObject.GetComponent<SpriteRenderer>().enabled = false;
			GameObject original = Resources.Load("Particles/BubbleExplosion") as GameObject;
			GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(original, base.gameObject.transform.position + Vector3.back * 20f, Quaternion.identity);
			if (this.mesh != null)
			{
				gameObject.transform.parent = this.mesh.transform;
			}
		}
		else
		{
			if (base.gameObject.GetComponent<Rigidbody2D>() == null)
			{
				base.gameObject.AddComponent<Rigidbody2D>();
			}
			base.gameObject.GetComponent<Rigidbody2D>().isKinematic = false;
			base.gameObject.GetComponent<Rigidbody2D>().gravityScale = 1f;
			base.gameObject.GetComponent<Rigidbody2D>().fixedAngle = false;
			base.gameObject.GetComponent<Rigidbody2D>().velocity = base.gameObject.GetComponent<Rigidbody2D>().velocity + new Vector2((float)UnityEngine.Random.Range(-2, 2), 0f);
			base.gameObject.GetComponent<CircleCollider2D>().enabled = true;
			base.gameObject.GetComponent<CircleCollider2D>().isTrigger = false;
			base.gameObject.GetComponent<CircleCollider2D>().radius = 0.3f;
			base.GetComponent<ball>().falling = true;
			if (GamePlay.Instance.GameStatus == GameState.Win && base.gameObject.name == "ball")
			{
				if (base.gameObject.GetComponent<Rigidbody2D>() == null)
				{
					base.gameObject.AddComponent<Rigidbody2D>();
				}
				GameObject.Find("paotong").GetComponent<Animator>().Play("PaotongAnim", -1, 0f);
				SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.BallFire);
				base.gameObject.GetComponent<Rigidbody2D>().isKinematic = false;
				base.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, (float)UnityEngine.Random.Range(10, 12)), ForceMode2D.Impulse);
				base.gameObject.GetComponent<SpriteRenderer>().sortingOrder = -1;
				base.gameObject.GetComponent<CircleCollider2D>().enabled = true;
				base.gameObject.GetComponent<CircleCollider2D>().isTrigger = false;
				base.gameObject.GetComponent<CircleCollider2D>().radius = 0.3f;
				base.GetComponent<ball>().falling = true;
				base.StartCoroutine(this.FlyBalls());
			}
		}
	}

	private IEnumerator FlyBalls()
	{
		yield return new WaitForSeconds(0.7f);
		base.gameObject.GetComponent<SpriteRenderer>().sortingOrder = 2;
		yield break;
	}

	private IEnumerator FlyToTarget()
	{
		Vector3 targetPos = new Vector3(2.3f, 6f, 0f);
		if (mainscript.Instance.TargetCounter1 < mainscript.Instance.TotalTargets)
		{
			mainscript.Instance.TargetCounter1++;
		}
		AnimationCurve curveX = new AnimationCurve(new Keyframe[]
		{
			new Keyframe(0f, base.transform.position.x),
			new Keyframe(0.5f, targetPos.x)
		});
		AnimationCurve curveY = new AnimationCurve(new Keyframe[]
		{
			new Keyframe(0f, base.transform.position.y),
			new Keyframe(0.5f, targetPos.y)
		});
		curveY.AddKey(0.2f, base.transform.position.y - 1f);
		float startTime = Time.time;
		Vector3 startPos = base.transform.position;
		float distCovered = 0f;
		while (distCovered < 0.6f)
		{
			distCovered = Time.time - startTime;
			base.transform.position = new Vector3(curveX.Evaluate(distCovered), curveY.Evaluate(distCovered), 0f);
			base.transform.Rotate(Vector3.back * 10f);
			yield return new WaitForEndOfFrame();
		}
		UnityEngine.Object.Destroy(base.gameObject);
		yield break;
	}

	public bool checkNearestBall(ArrayList b)
	{
		if ((mainscript.Instance.TopBorder.transform.position.y - base.transform.position.y <= 0.5f && LevelData.mode != ModeGame.Rounded) || (LevelData.mode == ModeGame.Rounded && base.tag == "chicken"))
		{
			Camera.main.GetComponent<mainscript>().controlArray = this.addFrom(b, Camera.main.GetComponent<mainscript>().controlArray);
			b.Clear();
			return true;
		}
		if (this.findInArray(Camera.main.GetComponent<mainscript>().controlArray, base.gameObject))
		{
			b.Clear();
			return true;
		}
		b.Add(base.gameObject);
		IEnumerator enumerator = this.nearBalls.GetEnumerator();
		try
		{
			while (enumerator.MoveNext())
			{
				object obj = enumerator.Current;
				GameObject gameObject = (GameObject)obj;
				if (gameObject != base.gameObject && gameObject != null && gameObject.gameObject.layer == 9)
				{
					float num = Vector3.Distance(base.transform.position, gameObject.transform.position);
					if (num <= 0.9f && num > 0f && !this.findInArray(b, gameObject.gameObject))
					{
						Camera.main.GetComponent<mainscript>().arraycounter++;
						if (gameObject.GetComponent<ball>().checkNearestBall(b))
						{
							return true;
						}
					}
				}
			}
		}
		finally
		{
			IDisposable disposable;
			if ((disposable = (enumerator as IDisposable)) != null)
			{
				disposable.Dispose();
			}
		}
		return false;
	}

	public void connectNearBalls()
	{
		int layerMask = 1 << LayerMask.NameToLayer("Ball");
		Collider2D[] array = Physics2D.OverlapCircleAll(base.transform.position, 0.5f, layerMask);
		this.nearBalls.Clear();
		foreach (Collider2D collider2D in array)
		{
			if (this.nearBalls.Count <= 7)
			{
				this.nearBalls.Add(collider2D.gameObject);
			}
		}
		this.countNEarBalls = this.nearBalls.Count;
	}

	private IEnumerator pullToMesh(Transform otherBall = null)
	{
		GameObject busyMesh = null;
		float searchRadius = 0.2f;
		while (this.findMesh)
		{
			Vector3 centerPoint = base.transform.position;
			Collider2D[] fixedBalls = Physics2D.OverlapCircleAll(centerPoint, 0.1f, 1024);
			foreach (Collider2D collider2D in fixedBalls)
			{
				if (collider2D.gameObject.GetComponent<Grid>() == null)
				{
					this.DestroySingle(base.gameObject, 1E-05f);
				}
				else if (collider2D.gameObject.GetComponent<Grid>().Busy == null)
				{
					this.findMesh = false;
					this.stopedBall = true;
					if (this.meshPos.y <= collider2D.gameObject.transform.position.y)
					{
						this.meshPos = collider2D.gameObject.transform.position;
						busyMesh = collider2D.gameObject;
					}
				}
			}
			if (this.findMesh)
			{
				Collider2D[] array2 = Physics2D.OverlapCircleAll(centerPoint, searchRadius, 1024);
				foreach (Collider2D collider2D2 in array2)
				{
					if (collider2D2.gameObject.GetComponent<Grid>() == null)
					{
						this.DestroySingle(base.gameObject, 1E-05f);
					}
					else if (collider2D2.gameObject.GetComponent<Grid>().Busy == null)
					{
						this.findMesh = false;
						this.stopedBall = true;
						if (this.meshPos.y <= collider2D2.gameObject.transform.position.y)
						{
							this.meshPos = collider2D2.gameObject.transform.position;
							busyMesh = collider2D2.gameObject;
						}
					}
				}
			}
			if (busyMesh != null)
			{
				busyMesh.GetComponent<Grid>().Busy = base.gameObject;
				base.gameObject.GetComponent<bouncer>().offset = busyMesh.GetComponent<Grid>().offset;
				if (LevelData.mode == ModeGame.Rounded)
				{
					LockLevelRounded.Instance.Rotate(this.target, base.transform.position);
				}
			}
			base.transform.parent = this.Meshes.transform;
			UnityEngine.Object.Destroy(base.GetComponent<Rigidbody2D>());
			yield return new WaitForFixedUpdate();
			this.dropTarget = base.transform.position;
			if (this.findMesh)
			{
				searchRadius += 0.2f;
			}
			yield return new WaitForFixedUpdate();
		}
		mainscript.Instance.connectNearBallsGlobal();
		if (busyMesh != null)
		{
			Hashtable animTable = mainscript.Instance.animTable;
			animTable.Clear();
			this.PlayHitAnim(base.transform.position, animTable);
		}
		creatorBall.Instance.OffGridColliders();
		yield return new WaitForSeconds(0.5f);
		yield break;
	}

	public void PlayHitAnim(Vector3 newBallPos, Hashtable animTable)
	{
		int layerMask = 1 << LayerMask.NameToLayer("Ball");
		Collider2D[] array = Physics2D.OverlapCircleAll(base.transform.position, 0.5f, layerMask);
		float force = 0.15f;
		foreach (Collider2D collider2D in array)
		{
			if (!animTable.ContainsKey(collider2D.gameObject) && collider2D.gameObject != base.gameObject && animTable.Count < 50)
			{
				collider2D.GetComponent<ball>().PlayHitAnimCorStart(newBallPos, force, animTable);
			}
		}
		if (array.Length > 0 && !animTable.ContainsKey(base.gameObject))
		{
			this.PlayHitAnimCorStart(array[0].gameObject.transform.position, 0f, animTable);
		}
	}

	public void PlayHitAnimCorStart(Vector3 newBallPos, float force, Hashtable animTable)
	{
		if (!this.animStarted)
		{
			base.StartCoroutine(this.PlayHitAnimCor(newBallPos, force, animTable));
			this.PlayHitAnim(newBallPos, animTable);
		}
	}

	public IEnumerator PlayHitAnimCor(Vector3 newBallPos, float force, Hashtable animTable)
	{
		this.animStarted = true;
		animTable.Add(base.gameObject, base.gameObject);
		if (base.tag == "chicken")
		{
			yield break;
		}
		yield return new WaitForFixedUpdate();
		float dist = Vector3.Distance(base.transform.position, newBallPos);
		force = 1f / dist + force;
		newBallPos = base.transform.position - newBallPos;
		if (base.transform.parent == null)
		{
			this.animStarted = false;
			yield break;
		}
		newBallPos = Quaternion.AngleAxis(base.transform.parent.parent.rotation.eulerAngles.z, Vector3.back) * newBallPos;
		newBallPos = newBallPos.normalized;
		newBallPos = base.transform.localPosition + newBallPos * force / 10f;
		float startTime = Time.time;
		Vector3 startPos = base.transform.localPosition;
		float speed = force * 5f;
		float distCovered = 0f;
		while (distCovered < 1f && !float.IsNaN(newBallPos.x))
		{
			distCovered = (Time.time - startTime) * speed;
			if (this == null)
			{
				yield break;
			}
			if (this.falling)
			{
				yield break;
			}
			base.transform.localPosition = Vector3.Lerp(startPos, newBallPos, distCovered);
			yield return new WaitForEndOfFrame();
		}
		Vector3 lastPos = base.transform.localPosition;
		startTime = Time.time;
		distCovered = 0f;
		while (distCovered < 1f && !float.IsNaN(newBallPos.x))
		{
			distCovered = (Time.time - startTime) * speed;
			if (this == null)
			{
				yield break;
			}
			if (this.falling)
			{
				yield break;
			}
			base.transform.localPosition = Vector3.Lerp(lastPos, startPos, distCovered);
			yield return new WaitForEndOfFrame();
		}
		base.transform.localPosition = startPos;
		this.animStarted = false;
		yield break;
	}

	private void OnTriggerStay2D(Collider2D other)
	{
		if (!this.findMesh || other.gameObject.layer == 9)
		{
		}
	}

	public void FindLight(GameObject activatedByBall)
	{
		int layerMask = 1 << LayerMask.NameToLayer("Ball");
		Collider2D[] array = Physics2D.OverlapCircleAll(base.transform.position, 0.5f, layerMask);
		int num = 0;
		foreach (Collider2D collider2D in array)
		{
			num++;
			if (num <= 10)
			{
				if (collider2D.gameObject.tag == "light" && GamePlay.Instance.GameStatus == GameState.Playing)
				{
					this.DestroySingle(collider2D.gameObject, 0.1f);
					this.DestroySingle(activatedByBall, 0.1f);
				}
				else if (collider2D.gameObject.tag == "cloud" && GamePlay.Instance.GameStatus == GameState.Playing)
				{
					collider2D.GetComponent<ColorBallScript>().ChangeRandomColor();
				}
			}
		}
	}

	private void OnCollisionEnter2D(Collision2D coll)
	{
		this.OnTriggerEnter2D(coll.collider);
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.name.Contains("ball") && this.setTarget && base.name.IndexOf("bug") < 0)
		{
			if (!other.gameObject.GetComponent<ball>().enabled)
			{
				if (other.gameObject.tag == "black_hole" && GamePlay.Instance.GameStatus == GameState.Playing)
				{
					SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.black_hole);
					this.DestroySingle(base.gameObject, 0.1f);
				}
				if (!this.fireBall)
				{
					this.StopBall(true, other.transform);
				}
				else
				{
					if (other.gameObject.tag.Contains("animal") || other.gameObject.tag.Contains("empty") || other.gameObject.tag.Contains("chicken"))
					{
						return;
					}
					this.fireBallLimit--;
					if (this.fireBallLimit > 0)
					{
						this.DestroySingle(other.gameObject, 1E-12f);
					}
					else
					{
						this.StopBall(true, null);
						this.destroy(this.fireballArray, 1E-12f);
					}
				}
			}
		}
		else if (other.gameObject.name.IndexOf("ball") == 0 && this.setTarget && base.name.IndexOf("bug") == 0)
		{
			if (other.gameObject.tag == base.gameObject.tag)
			{
				UnityEngine.Object.Destroy(other.gameObject);
			}
		}
		else if (other.gameObject.name == "TopBorder" && this.setTarget && (LevelData.mode == ModeGame.Vertical || LevelData.mode == ModeGame.Animals) && !this.findMesh)
		{
			base.transform.position = new Vector3(base.transform.position.x, base.transform.position.y, base.transform.position.z);
			this.StopBall(true, null);
			if (this.fireBall)
			{
				this.destroy(this.fireballArray, 1E-12f);
			}
		}
	}

	private void StopBall(bool pulltoMesh = true, Transform otherBall = null)
	{
		this.launched = true;
		mainscript.lastBall = base.gameObject.transform.position;
		creatorBall.Instance.EnableGridColliders();
		this.target = Vector2.zero;
		this.setTarget = false;
		base.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
		this.findMesh = true;
		base.GetComponent<BoxCollider2D>().offset = Vector2.zero;
		base.GetComponent<BoxCollider2D>().size = new Vector2(0.5f, 0.5f);
		if (base.GetComponent<SpriteRenderer>().sprite == this.boosts[0])
		{
			this.DestroyAround();
		}
		if (pulltoMesh)
		{
			base.StartCoroutine(this.pullToMesh(otherBall));
		}
	}

	private void DestroyAround()
	{
		ArrayList arrayList = new ArrayList();
		arrayList.Add(base.gameObject);
		int layerMask = 1 << LayerMask.NameToLayer("Ball");
		Collider2D[] array = Physics2D.OverlapCircleAll(base.transform.position, 1f, layerMask);
		foreach (Collider2D collider2D in array)
		{
			GameObject gameObject = collider2D.gameObject;
			if (!this.findInArray(arrayList, gameObject) && gameObject.tag != "chicken" && !gameObject.tag.Contains("animal") && !gameObject.tag.Contains("empty"))
			{
				arrayList.Add(gameObject);
			}
		}
		if (arrayList.Count >= 0)
		{
			mainscript.Instance.ComboCount++;
			this.destroy(arrayList, 0.001f);
		}
	}

	private void DestroyLine()
	{
		ArrayList arrayList = new ArrayList();
		int layerMask = 1 << LayerMask.NameToLayer("Ball");
		RaycastHit2D[] array = Physics2D.LinecastAll(base.transform.position + Vector3.left * 10f, base.transform.position + Vector3.right * 10f, layerMask);
		foreach (RaycastHit2D raycastHit2D in array)
		{
			if (!this.findInArray(arrayList, raycastHit2D.collider.gameObject))
			{
				arrayList.Add(raycastHit2D.collider.gameObject);
			}
		}
		if (arrayList.Count >= 0)
		{
			mainscript.Instance.ComboCount++;
			mainscript.Instance.destroy(arrayList);
		}
		mainscript.Instance.StartCoroutine(mainscript.Instance.destroyAloneBall());
	}

	public void CheckBallCrossedBorder()
	{
		if (Physics2D.OverlapCircle(base.transform.position, 0.1f, 16384) != null || Physics2D.OverlapCircle(base.transform.position, 0.1f, 131072) != null)
		{
			this.DestroySingle(base.gameObject, 1E-05f);
		}
	}

	private void triggerEnter()
	{
		if (base.transform.position.y <= this.bottomBorder && this.target.y < 0f)
		{
			this.growUp();
			this.StopBall(false, null);
			LevelData.LimitAmount -= 1f;
		}
		else
		{
			if (base.transform.position.x <= this.leftBorder && this.target.x < 0f && !this.touchedSide && this.fireBall)
			{
				base.Invoke("CanceltouchedSide", 0.1f);
				this.target = new Vector2(this.target.x * -1f, this.target.y);
				base.GetComponent<Rigidbody2D>().velocity = new Vector2(base.GetComponent<Rigidbody2D>().velocity.x * -1f, base.GetComponent<Rigidbody2D>().velocity.y);
			}
			if (base.transform.position.x >= this.rightBorder && this.target.x > 0f && !this.touchedSide && this.fireBall)
			{
				base.Invoke("CanceltouchedSide", 0.1f);
				this.target = new Vector2(this.target.x * -1f, this.target.y);
				base.GetComponent<Rigidbody2D>().velocity = new Vector2(base.GetComponent<Rigidbody2D>().velocity.x * -1f, base.GetComponent<Rigidbody2D>().velocity.y);
			}
			if (base.transform.position.y >= this.topBorder && this.target.y > 0f && LevelData.mode == ModeGame.Rounded && !this.touchedTop)
			{
				this.touchedTop = true;
				base.GetComponent<Rigidbody2D>().velocity = new Vector2(base.GetComponent<Rigidbody2D>().velocity.x, base.GetComponent<Rigidbody2D>().velocity.y * -1f);
			}
		}
	}

	private void CanceltouchedSide()
	{
		this.touchedSide = false;
	}

	public void destroy(ArrayList b, float speed = 0.1f)
	{
		base.StartCoroutine(this.DestroyCor(b, speed));
	}

	private IEnumerator DestroyCor(ArrayList b, float speed = 0.1f)
	{
		ArrayList i = new ArrayList();
		IEnumerator enumerator = b.GetEnumerator();
		try
		{
			while (enumerator.MoveNext())
			{
				object obj2 = enumerator.Current;
				GameObject value = (GameObject)obj2;
				i.Add(value);
			}
		}
		finally
		{
			IDisposable disposable;
			if ((disposable = (enumerator as IDisposable)) != null)
			{
				disposable.Dispose();
			}
		}
		Camera.main.GetComponent<mainscript>().bounceCounter = 0;
		int scoreCounter = 0;
		int rate = 0;
		int soundPool = 0;
		IEnumerator enumerator2 = i.GetEnumerator();
		try
		{
			while (enumerator2.MoveNext())
			{
				object obj3 = enumerator2.Current;
				GameObject obj = (GameObject)obj3;
				if (!(obj == null))
				{
					if (obj.name.IndexOf("ball") == 0)
					{
						obj.layer = 0;
					}
					obj.GetComponent<ball>().growUp();
					soundPool++;
					base.GetComponent<Collider2D>().enabled = false;
					if (scoreCounter > 3)
					{
						rate += 10;
						scoreCounter += rate;
					}
					scoreCounter += 10;
					if (b.Count > 10 && UnityEngine.Random.Range(0, 10) > 5)
					{
						mainscript.Instance.perfect.SetActive(true);
					}
					obj.GetComponent<ball>().Destroyed = true;
					if (b.Count < 10 || soundPool % 20 == 0)
					{
						yield return new WaitForSeconds(speed);
					}
				}
			}
		}
		finally
		{
			IDisposable disposable2;
			if ((disposable2 = (enumerator2 as IDisposable)) != null)
			{
				disposable2.Dispose();
			}
		}
		mainscript.Instance.PopupScore(scoreCounter, base.transform.position);
		yield break;
	}

	private void DestroySingle(GameObject obj, float speed = 0.1f)
	{
		Camera.main.GetComponent<mainscript>().bounceCounter = 0;
		int num = 0;
		int num2 = 0;
		int num3 = 0;
		if (obj.name.IndexOf("ball") == 0)
		{
			obj.layer = 0;
		}
		obj.GetComponent<ball>().growUp();
		num3++;
		if (obj.tag == "light")
		{
			SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.spark);
			obj.GetComponent<ball>().DestroyLine();
		}
		if (num > 3)
		{
			num2 += 10;
			num += num2;
		}
		num += 10;
		obj.GetComponent<ball>().Destroyed = true;
		mainscript.Instance.PopupScore(num, base.transform.position);
	}

	public void SplashDestroy()
	{
		if (this.setTarget)
		{
			mainscript.Instance.newBall2 = null;
		}
		UnityEngine.Object.Destroy(base.gameObject);
	}

	public void destroy()
	{
		this.growUpPlaySound();
		this.destroy(base.gameObject);
	}

	public void destroy(GameObject obj)
	{
		if (obj.name.IndexOf("ball") == 0)
		{
			obj.layer = 0;
		}
		Camera.main.GetComponent<mainscript>().bounceCounter = 0;
		obj.GetComponent<ball>().destroyed = true;
		obj.GetComponent<ball>().growUp();
		Camera.main.GetComponent<mainscript>().explode(obj.gameObject);
	}

	public void growUp()
	{
		base.StartCoroutine(this.explode());
	}

	public void growUpPlaySound()
	{
		base.Invoke("growUpDelayed", 1f / (float)UnityEngine.Random.Range(2, 10));
	}

	public void growUpDelayed()
	{
		base.StartCoroutine(this.explode());
	}

	private void playPop()
	{
	}

	private IEnumerator explode()
	{
		GamePlay.Instance.counter += 0.025f;
		yield return new WaitForSeconds(GamePlay.Instance.counter);
		float startTime = Time.time;
		float endTime = Time.time + 0.1f;
		Vector3 tempPosition = base.transform.localScale;
		Vector3 targetPrepare = base.transform.localScale * 1.2f;
		base.GetComponent<SpriteRenderer>().enabled = false;
		base.GetComponent<CircleCollider2D>().enabled = false;
		base.GetComponent<BoxCollider2D>().enabled = false;
		GameObject prefab = Resources.Load("Particles/ExplosionBubble") as GameObject;
		if (base.gameObject.tag == "blue")
		{
			prefab.GetComponent<SpriteRenderer>().color = new Color32(61, 78, 250, byte.MaxValue);
		}
		else if (base.gameObject.tag == "red")
		{
			prefab.GetComponent<SpriteRenderer>().color = new Color32(byte.MaxValue, 30, 11, byte.MaxValue);
		}
		else if (base.gameObject.tag == "yellow")
		{
			prefab.GetComponent<SpriteRenderer>().color = new Color32(249, byte.MaxValue, 0, byte.MaxValue);
		}
		else if (base.gameObject.tag == "green")
		{
			prefab.GetComponent<SpriteRenderer>().color = new Color32(59, byte.MaxValue, 32, byte.MaxValue);
		}
		else if (base.gameObject.tag == "violet")
		{
			prefab.GetComponent<SpriteRenderer>().color = new Color32(138, 83, 243, byte.MaxValue);
		}
		SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.BallExplode);
		GameObject explosion = UnityEngine.Object.Instantiate<GameObject>(prefab, base.gameObject.transform.position + Vector3.back * 20f, Quaternion.identity);
		if (this.mesh != null)
		{
			explosion.transform.parent = this.mesh.transform;
		}
		this.CheckNearCloud();
		if (LevelData.mode != ModeGame.Vertical || !this.isTarget)
		{
			if (LevelData.mode != ModeGame.Animals || this.isTarget)
			{
			}
		}
		UnityEngine.Object.Destroy(base.gameObject, 1f);
		yield break;
	}

	private void CheckNearCloud()
	{
		int layerMask = 1 << LayerMask.NameToLayer("Ball");
		Collider2D[] array = Physics2D.OverlapCircleAll(base.transform.position, 1f, layerMask);
		foreach (Collider2D collider2D in array)
		{
			if (collider2D.gameObject.tag == "cloud")
			{
				GameObject gameObject = collider2D.gameObject;
				float num = Vector3.Distance(base.transform.position, gameObject.transform.position);
				if (num <= 1f)
				{
					gameObject.GetComponent<ColorBallScript>().ChangeRandomColor();
				}
			}
		}
	}

	public void ShowFirework()
	{
		ball.fireworks++;
		if (ball.fireworks <= 2)
		{
			SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.hit);
		}
	}

	public Sprite[] sprites;

	public Sprite[] boosts;

	public bool isTarget;

	private Vector2 speed = new Vector2(250f, 250f);

	public Vector3 target;

	private Vector2 worldPos;

	private Vector3 forceVect;

	public bool setTarget;

	public float startTime;

	private float duration = 1f;

	public GameObject mesh;

	private Vector2[] meshArray;

	public bool findMesh;

	private Vector3 dropTarget;

	private float row;

	private string str;

	public bool newBall;

	private float mTouchOffsetX;

	private float mTouchOffsetY;

	private float xOffset;

	private float yOffset;

	public Vector3 targetPosition;

	private bool stopedBall;

	private bool destroyed;

	public bool NotSorting;

	private ArrayList fireballArray = new ArrayList();

	public PhysicsMaterial2D Bounce1;

	public ArrayList nearBalls = new ArrayList();

	private GameObject Meshes;

	public int countNEarBalls;

	private float bottomBorder;

	private float topBorder;

	private float leftBorder;

	private float rightBorder;

	private float gameOverBorder;

	private bool gameOver;

	private bool isPaused;

	public AudioClip swish;

	public AudioClip pops;

	public AudioClip join;

	private Vector3 meshPos;

	private bool dropedDown;

	private bool rayTarget;

	private RaycastHit2D[] bugHits;

	private RaycastHit2D[] bugHits2;

	private RaycastHit2D[] bugHits3;

	public bool falling;

	private Animation rabbit;

	private int HitBug;

	private bool fireBall;

	private static int fireworks;

	private bool touchedTop;

	private bool touchedSide;

	private int fireBallLimit = 10;

	private bool launched;

	private bool animStarted;

	public Material NewMat;

	public Texture Blue;

	public Texture Red;

	public Texture Yellow;

	public Texture Green;

	public Texture Violate;
}
