using System;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial : MonoBehaviour
{
	private void Start()
	{
		int currentLevel = mainscript.Instance.currentLevel;
		if (currentLevel <= 8)
		{
			this.label.text = this.labels[currentLevel - 1];
			this.picture.sprite = this.pictures[currentLevel - 1];
		}
		else if (currentLevel == 11)
		{
			this.label.text = this.labels[8];
			this.picture.sprite = this.pictures[8];
		}
		else if (currentLevel == 15)
		{
			this.label.text = this.labels[9];
			this.picture.sprite = this.pictures[9];
		}
		else if (currentLevel == 17)
		{
			this.label.text = this.labels[10];
			this.picture.sprite = this.pictures[10];
		}
		else if (currentLevel == 26)
		{
			this.label.text = this.labels[11];
			this.picture.sprite = this.pictures[11];
		}
		else if (currentLevel == 41)
		{
			this.label.text = this.labels[12];
			this.picture.sprite = this.pictures[12];
		}
		else
		{
			GamePlay.Instance.GameStatus = GameState.Playing;
			base.gameObject.SetActive(false);
		}
	}

	private void Update()
	{
	}

	public Text label;

	public Image picture;

	public Sprite[] pictures;

	public string[] labels;
}
