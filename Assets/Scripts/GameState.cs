using System;

public enum GameState
{
	Playing,
	Highscore,
	GameOver,
	Pause,
	Win,
	WaitForPopup,
	WaitAfterClose,
	BlockedGame,
	Tutorial,
	PreTutorial,
	WaitForChicken
}
