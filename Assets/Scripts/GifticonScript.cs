using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GifticonScript : MonoBehaviour
{
	private void Start()
	{
		this.Gift_CardPlay.SetActive(false);
		base.StartCoroutine("GiftCard_Animplay");
	}

	private IEnumerator GiftCard_Animplay()
	{
		while (!this.Gift_CardPlay.activeSelf)
		{
			yield return new WaitForSeconds(0.1f);
			//if (PlayerPrefs.GetInt("ads", 0) == 0 && GoogleMobileAdsDemoScript.instance.interstitial.IsLoaded() && !this.Gift_CardPlay.activeSelf)
			//{
			//	this.Gift_CardPlay.SetActive(true);
			//	break;
			//}
			this.Gift_CardPlay.SetActive(false);
		}
		this.Gift_CardPlay.GetComponent<Animator>().Play("GiftPopUpAnim");
		yield break;
	}

	private IEnumerator GiftCard_AnimPlay1()
	{
		yield return new WaitForSeconds(0.5f);
		this.Gift_CardPlay.SetActive(false);
		if (PlayerPrefs.GetInt("ads", 0) == 0)
		{
			//GoogleMobileAdsDemoScript.instance.ShowInterstitial();
		}
		base.StartCoroutine("GiftCard_Animplay");
		yield break;
	}

	public void Gift_Card_Adsplay()
	{
		base.StopCoroutine("GiftCard_Animplay");
		base.StartCoroutine("GiftCard_AnimPlay1");
		this.Gift_CardPlay.GetComponent<Animator>().Play("GiftIconDestroyAnim");
	}

	public GameObject Gift_CardPlay;

	public List<Sprite> Gift_card_sprite = new List<Sprite>();
}
