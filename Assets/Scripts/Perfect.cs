using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Perfect : MonoBehaviour
{
	private void OnEnable()
	{
		base.GetComponent<Image>().sprite = this.images[UnityEngine.Random.Range(0, this.images.Length)];
		base.GetComponent<Image>().SetNativeSize();
		base.StartCoroutine(this.PerfectAction());
	}

	private IEnumerator PerfectAction()
	{
		yield return new WaitForSeconds(2f);
		base.gameObject.SetActive(false);
		yield break;
	}

	private void Update()
	{
	}

	public Sprite[] images;
}
