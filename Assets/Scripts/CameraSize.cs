using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CameraSize : MonoBehaviour
{
	private void Start()
	{
		if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("map"))
		{
			Application.targetFrameRate = 60;
			float num = (float)Screen.height / (float)Screen.width;
			num = (float)Math.Round((double)num, 2);
			if (num == 1.6f)
			{
				base.GetComponent<Camera>().orthographicSize = 12.23f;
			}
			else if (num == 1.78f)
			{
				base.GetComponent<Camera>().orthographicSize = 13.6f;
			}
			else if (num == 1.5f)
			{
				base.GetComponent<Camera>().orthographicSize = 11.46f;
			}
			else if (num == 1.33f)
			{
				base.GetComponent<Camera>().orthographicSize = 10.16f;
			}
			else if (num == 1.67f)
			{
				base.GetComponent<Camera>().orthographicSize = 12.68f;
			}
			else if (num == 1.25f)
			{
				base.GetComponent<Camera>().orthographicSize = 9.5f;
			}
		}
	}
}
