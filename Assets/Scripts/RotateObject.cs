using System;
using UnityEngine;

public class RotateObject : MonoBehaviour
{
	private void Start()
	{
	}

	private void Update()
	{
		if (Time.timeScale == 1)
		{
			base.transform.Rotate(0f, 0f, this.Rotationspeed * 0.02f);
		}
	}

	public float Rotationspeed;
}
