using System;
using System.Collections.Generic;

public class MissionManager
{
	public MissionManager()
	{
		MissionManager.Instance = this;
	}

	public void InitScore(List<Mission> missions)
	{
		this.missionScores.Clear();
		for (int i = 0; i < missions.Count; i++)
		{
			Mission item = new Mission(0, missions[i].type);
			this.missionScores.Add(item);
		}
	}

	public void AddScore(int amount, MissionType missionType)
	{
		Mission mission = this.missionScores.Find((Mission obj) => obj.type == missionType);
		if (mission != null)
		{
			mission.amount += amount;
		}
	}

	public Mission GetMission(MissionType missionType)
	{
		return this.missionScores.Find((Mission obj) => obj.type == missionType);
	}

	public bool IsWin()
	{
		foreach (Mission mission in LevelData.requestMissions)
		{
			if (this.GetMission(mission.type).amount < mission.amount)
			{
				return false;
			}
		}
		return true;
	}

	public static MissionManager Instance;

	public List<Mission> missionScores = new List<Mission>();
}
