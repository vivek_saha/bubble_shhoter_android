using System;
using System.Collections;
using UnityEngine;

public class LatestartObject : MonoBehaviour
{
	private void Start()
	{
		base.StartCoroutine(this.latetrue());
	}

	private void Update()
	{
	}

	private IEnumerator latetrue()
	{
		yield return new WaitForSeconds(2f);
		base.gameObject.transform.GetChild(0).gameObject.SetActive(true);
		base.gameObject.transform.GetChild(1).gameObject.SetActive(true);
		yield break;
	}
}
