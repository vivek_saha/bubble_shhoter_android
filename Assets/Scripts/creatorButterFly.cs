using System;
using UnityEngine;

public class creatorButterFly : MonoBehaviour
{
	private void Start()
	{
		this.thePrefab = this.butterFly_hd;
	}

	private void Update()
	{
	}

	public void createButterFly(int revertButterFly)
	{
		GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.thePrefab, this.thePrefab.transform.position, base.transform.rotation);
		gameObject.GetComponent<butterfly>().revertButterFly = revertButterFly;
		gameObject.transform.Rotate(new Vector3(0f, 0f, 0f));
	}

	public GameObject butterFly_hd;

	public GameObject butterFly_ld;

	private GameObject thePrefab;
}
