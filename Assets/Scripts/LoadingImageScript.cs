using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingImageScript : MonoBehaviour
{
	private void Start()
	{
    }
    private void OnEnable()
    {

        base.StartCoroutine("GiftCard_Animplay");
    }

    private IEnumerator GiftCard_Animplay()
	{
		int i = 0;
		for (;;)
		{
			yield return new WaitForSecondsRealtime(0.1f);
			if (i < this.Sprites.Count)
			{
				base.transform.GetComponent<Image>().sprite = this.Sprites[i];
				i++;
			}
			else
			{
				i = 0;
				base.StartCoroutine("GiftCard_Animplay");
			}
		}
		yield break;
	}

	public List<Sprite> Sprites = new List<Sprite>();
}
