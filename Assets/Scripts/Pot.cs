using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Pot : MonoBehaviour
{
    private void Start()
    {
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.name.Contains("ball"))
        {
            col.gameObject.GetComponent<ball>().SplashDestroy();
            col.gameObject.GetComponent<Rigidbody2D>().isKinematic = true;
            col.gameObject.GetComponent<Collider2D>().enabled = false;
            this.PlaySplash(col.contacts[0].point);
        }
    }

    private void PlaySplash(Vector2 pos)
    {
        base.StartCoroutine(this.SoundsCounter());
        if (mainscript.Instance.potSounds < 4)
        {
            SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.pops[GamePlay.Instance.PotScriptSound], 2f);
            GamePlay.Instance.PotScriptSound++;
            if (GamePlay.Instance.PotScriptSound >= SoundBase.Instance.pops.Length)
            {
                GamePlay.Instance.PotScriptSound = 0;
            }
        }
        GameObject obj = UnityEngine.Object.Instantiate<GameObject>(this.splashPrefab, base.transform.position, Quaternion.identity);
        this.BubketGlow.SetActive(true);
        base.StartCoroutine(this.BukGlow());
        //Color color = this.BubketGlow.GetComponent<SpriteRenderer>().color;
        //color.a = 0.5f;
        //this.BubketGlow.GetComponent<SpriteRenderer>().color = new Color(this.BubketGlow.GetComponent<SpriteRenderer>().color.r, this.BubketGlow.GetComponent<SpriteRenderer>().color.g, this.BubketGlow.GetComponent<SpriteRenderer>().color.b, color.a);
        UnityEngine.Object.Destroy(obj, 2f);
        mainscript.Instance.PopupScore(this.score * mainscript.doubleScore, base.transform.position + Vector3.up);
    }

    private IEnumerator BukGlow()
    {
        //Color tmp = this.BubketGlow.GetComponent<SpriteRenderer>().color;
        yield return new WaitForSeconds(0.5f);
        //if (tmp.a < 1f)
        //{
        //	tmp.a += 0.05f;
        //	this.BubketGlow.GetComponent<SpriteRenderer>().color = new Color(this.BubketGlow.GetComponent<SpriteRenderer>().color.r, this.BubketGlow.GetComponent<SpriteRenderer>().color.g, this.BubketGlow.GetComponent<SpriteRenderer>().color.b, tmp.a);
        //	base.StartCoroutine(this.BukGlow());
        //}
        //else
        //{
        base.StartCoroutine(this.BukGlowInvisible());
        //}
        yield break;
    }

    private IEnumerator BukGlowInvisible()
    {
        //Color tmp = this.BubketGlow.GetComponent<SpriteRenderer>().color;
        yield return new WaitForSeconds(0.5f);
        //if (tmp.a > 0f)
        //{
        //	tmp.a -= 0.1f;
        //	this.BubketGlow.GetComponent<SpriteRenderer>().color = new Color(this.BubketGlow.GetComponent<SpriteRenderer>().color.r, this.BubketGlow.GetComponent<SpriteRenderer>().color.g, this.BubketGlow.GetComponent<SpriteRenderer>().color.b, tmp.a);
        //	base.StartCoroutine(this.BukGlowInvisible());
        //}
        //else
        //{
        this.BubketGlow.SetActive(false);
        //}
        yield break;
    }

    private IEnumerator SoundsCounter()
    {
        mainscript.Instance.potSounds++;
        yield return new WaitForSeconds(0.2f);
        mainscript.Instance.potSounds--;
        yield break;
    }

    private void Update()
    {
        this.label.text = string.Empty + this.score * mainscript.doubleScore;
    }

    public int score;

    public Text label;

    public GameObject splashPrefab;

    public GameObject BubketGlow;

    private Color col;
}
