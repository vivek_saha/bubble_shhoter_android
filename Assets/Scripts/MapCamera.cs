using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class MapCamera : MonoBehaviour
{
	public void Awake()
	{
		this._transform = base.transform;
		this.currentTime = 0f;
		this.speed = 0f;
		Vector3 position = GameObject.Find("Map1").transform.GetChild(PlayerPrefs.GetInt("MaxLevel",0)).transform.position;
		MonoBehaviour.print("-=-=-=-==-=-=-=--=>" + PlayerPrefs.GetInt("MaxLevel", 0));
		if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("map"))
		{
			Application.targetFrameRate = 60;
			float num = (float)Screen.height / (float)Screen.width;
			if (num > 2f)
			{
				base.GetComponent<Camera>().orthographicSize = 9.5f;
				base.transform.position = new Vector3(0f, position.y, -10f) + new Vector3(0f, 5f, 0f);
			}
			else if (num <= 2f && num >= 1.6f)
			{
				base.GetComponent<Camera>().orthographicSize = 8f;
				base.transform.position = new Vector3(0f, position.y, -10f) + new Vector3(0f, 5f, 0f);
			}
			else if (num < 1.6f)
			{
				base.GetComponent<Camera>().orthographicSize = 6f;
				base.transform.position = new Vector3(0f, position.y, -10f) + new Vector3(0f, 5f, 0f);
			}
		}
	}

	public void OnDrawGizmos()
	{
		Gizmos.DrawWireCube(this.Bounds.center, this.Bounds.size);
	}

	public void Update()
	{
#if UNITY_EDITOR
		this.HandleMouseInput();

#else
		this.HandleTouchInput();

#endif
	}

	private void LateUpdate()
	{
		this.SetPosition(base.transform.position);
	}

	private void HandleTouchInput()
	{
		if (UnityEngine.Input.touchCount == 1 && !GameObject.Find("Canvas").transform.GetChild(10).gameObject.activeSelf && !GameObject.Find("Canvas").transform.GetChild(11).gameObject.activeSelf && !GameObject.Find("Canvas").transform.GetChild(12).gameObject.activeSelf && !GameObject.Find("Canvas").transform.GetChild(13).gameObject.activeSelf && !GameObject.Find("Canvas").transform.GetChild(14).gameObject.activeSelf && !GameObject.Find("Canvas").transform.GetChild(15).gameObject.activeSelf && !GameObject.Find("Canvas").transform.GetChild(16).gameObject.activeSelf && !GameObject.Find("Canvas").transform.GetChild(17).gameObject.activeSelf && !GameObject.Find("Canvas").transform.GetChild(9).gameObject.activeSelf && !GameObject.Find("Canvas").transform.GetChild(8).gameObject.activeSelf && !GameObject.Find("Canvas").transform.GetChild(7).gameObject.activeSelf && !GameObject.Find("Canvas").transform.GetChild(6).gameObject.activeSelf)
		{
			Touch touch = UnityEngine.Input.GetTouch(0);
			if (touch.phase == TouchPhase.Began)
			{
				this.touched = true;
				this.deltaV = Vector2.zero;
				this._prevPosition = touch.position;
				this.firstV = this._prevPosition;
				this.currentTime = Time.time;
			}
			else if (touch.phase == TouchPhase.Moved)
			{
				Vector2 position = touch.position;
				this.MoveCamera(this._prevPosition, position);
				this.deltaV = this._prevPosition - position;
				this._prevPosition = position;
			}
			else if (touch.phase == TouchPhase.Ended)
			{
				this.touched = false;
			}
		}
		else if (!this.touched)
		{
			this.deltaV -= this.deltaV * Time.deltaTime * 10f;
			base.transform.Translate(this.deltaV.x / 30f, this.deltaV.y / 30f, 0f);
		}
	}

	private void HandleMouseInput()
	{
		if (Input.GetMouseButtonDown(0) && !EventSystem.current.currentSelectedGameObject)
		{
			this.deltaV = Vector2.zero;
			this._prevPosition = UnityEngine.Input.mousePosition;
			this.firstV = this._prevPosition;
			this.currentTime = Time.time;
		}
		else if (Input.GetMouseButton(0) && !EventSystem.current.currentSelectedGameObject)
		{
			Vector2 vector = UnityEngine.Input.mousePosition;
			this.MoveCamera(this._prevPosition, vector);
			this.deltaV = this._prevPosition - vector;
			this._prevPosition = vector;
			this.speed = Time.time;
		}
		else if (Input.GetMouseButtonUp(0) && !EventSystem.current.currentSelectedGameObject)
		{
			this.speed = Time.time - this.currentTime;
			Vector3 a = (Vector2)base.transform.position - this.deltaV;
			Vector3 vector2 = base.transform.position - a / 20f;
		}
		else
		{
			this.deltaV -= this.deltaV * Time.deltaTime * 10f;
			base.transform.Translate(this.deltaV.x / 30f, this.deltaV.y / 30f, 0f);
		}
	}

	private void MoveCamera(Vector2 prevPosition, Vector2 curPosition)
	{
		if (EventSystem.current.IsPointerOverGameObject(-1))
		{
			return;
		}
		this.SetPosition(base.transform.localPosition + (this.Camera.ScreenToWorldPoint(prevPosition) - this.Camera.ScreenToWorldPoint(curPosition)));
	}

	public void SetPosition(Vector2 position)
	{
		Vector2 vector = this.ApplyBounds(position);
		this._transform = base.transform;
		this._transform.position = new Vector3(0f, vector.y, this._transform.position.z);
	}

	private Vector2 ApplyBounds(Vector2 position)
	{
		float num = this.Camera.orthographicSize * 2f;
		float num2 = (float)Screen.width * 1f / (float)Screen.height * num;
		position.x = Mathf.Max(position.x, this.Bounds.min.x + num2 / 2f);
		position.y = Mathf.Max(position.y, this.Bounds.min.y + num / 2f);
		position.x = Mathf.Min(position.x, this.Bounds.max.x - num2 / 2f);
		position.y = Mathf.Min(position.y, this.Bounds.max.y - num / 2f);
		return position;
	}

	private Vector2 _prevPosition;

	private Transform _transform;

	public Camera Camera;

	public Bounds Bounds;

	private Vector2 firstV;

	private Vector2 deltaV;

	private float currentTime;

	private float speed;

	private bool touched;
}
