﻿using UnityEngine;
using UnityEngine.UI;

public class HowToplay_panel_script : MonoBehaviour
{

    public static HowToplay_panel_script I;
    public Image img;
    public Sprite[] panels;
    public int Counter = 0;
    public Button next, pre;

    private void Awake()
    {
        I = this;
    }


    private void Start()
    {
        Counter = 0;
        img.sprite = panels[Counter];
        next.onClick.AddListener(()=>  onclick_next());
        pre.onClick.AddListener(() => onclick_pre());
    }
    private void OnEnable()
    {
        Counter = 0;
        img.sprite = panels[Counter];
        next.interactable = true;
        pre.interactable = false;
    }
    public void onclick_next()
    {
        if (Counter == panels.Length - 1)
        {
            Counter = 0;
            //gameObject.SetActive(false);
        }
        else
        {


            Counter++;
        }
        img.sprite = panels[Counter];
        setter_interactable();
    }

    private void setter_interactable()
    {
        if (Counter == panels.Length - 1)
        {
            next.interactable = false;
            pre.interactable = true;
        }
        else if (Counter == 0)
        {
            next.interactable = true;
            pre.interactable = false;
        }
        else
        {
            next.interactable = true;
            pre.interactable = true;
        }
    }

    public void onclick_pre()
    {
        if (Counter == 0)
        {
            Counter = panels.Length - 1;
            //gameObject.SetActive(false);
        }
        else
        {


            Counter--;
        }
        img.sprite = panels[Counter];
        setter_interactable();
    }
}
