using System;
using System.Collections;
using System.Collections.Generic;
using InitScriptName;
using UnityEngine;
using UnityEngine.UI;

public class AnimationManager : MonoBehaviour
{
    public static AnimationManager instance { get; set; }

    public int Counter = 0;

    private void OnEnable()
    {
        AnimationManager.instance = this;
        if (!this.PlayOnEnable || base.name == "Fire")
        {
        }
        if (base.name == "MenuPlay")
        {
            for (int i = 1; i <= 3; i++)
            {
                base.transform.Find("Image").Find("Star" + i).gameObject.SetActive(false);
            }
            int @int = PlayerPrefs.GetInt(string.Format("Level.{0:000}.StarsCount", PlayerPrefs.GetInt("OpenLevel")), 0);
            if (@int > 0)
            {
                for (int j = 1; j <= @int; j++)
                {
                    base.transform.Find("Image").Find("Star" + j).gameObject.SetActive(true);
                }
            }
            else
            {
                for (int k = 1; k <= 3; k++)
                {
                    base.transform.Find("Image").Find("Star" + k).gameObject.SetActive(false);
                }
            }
        }
        if (base.name == "SettingButton" || base.name == "MenuPause" || base.name == "Option")
        {
            if (PlayerPrefs.GetInt("music", 1) == 1)
            {
                MonoBehaviour.print("musicon");
                GameObject.Find("Music").GetComponent<AudioSource>().volume = 0.4f;
                this.music.GetComponent<Image>().sprite = this.musicon;
            }
            else
            {
                MonoBehaviour.print("musicoff");
                GameObject.Find("Music").GetComponent<AudioSource>().volume = 0f;
                this.music.GetComponent<Image>().sprite = this.musicoff;
            }
            if (PlayerPrefs.GetInt("sound", 1) == 1)
            {
                MonoBehaviour.print("soundon");
                SoundBase.Instance.GetComponent<AudioSource>().volume = 1f;
                this.sound.GetComponent<Image>().sprite = this.soundon;
            }
            else
            {
                MonoBehaviour.print("sound0ff");
                SoundBase.Instance.GetComponent<AudioSource>().volume = 0f;
                this.sound.GetComponent<Image>().sprite = this.sound0ff;
            }
        }
    }

    private void OnDisable()
    {
    }

    public void OnFinished()
    {
        if (base.name == "MenuComplete")
        {
            base.StartCoroutine(this.MenuComplete());
            base.StartCoroutine(this.MenuCompleteScoring());
        }
        if (base.name == "MenuPlay")
        {
            InitScript.Instance.currentTarget = LevelData.GetTarget(PlayerPrefs.GetInt("OpenLevel"));
        }
    }

    private IEnumerator MenuComplete()
    {
        PlayerPrefs.SetInt("LevelStar" + PlayerPrefs.GetInt("OpenLevel"), mainscript.Instance.stars);
        yield return new WaitForSeconds(0.5f);
        for (int i = 1; i <= mainscript.Instance.stars; i++)
        {
            base.transform.Find("Image").Find("Star" + i).gameObject.SetActive(true);
            yield return new WaitForSeconds(0.3f);
            SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.GetStarPlaying);
            yield return new WaitForSeconds(0.5f);
        }
        yield break;
    }

    private IEnumerator MenuCompleteScoring()
    {
        Text scores = base.transform.Find("Image").Find("Score").GetComponent<Text>();
        for (int i = 0; i <= mainscript.Score; i += 500)
        {
            scores.text = "Score : " + i;
            yield return new WaitForSeconds(1E-05f);
        }
        scores.text = "Score : " + mainscript.Score;
        yield break;
    }

    public void PlaySoundButton()
    {
        SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.click);
    }

    public IEnumerator Close()
    {
        yield return new WaitForSeconds(0.5f);
        yield break;
    }

    public void CloseMenu()
    {
        SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.click);
        if (base.gameObject.name == "MenuPreGameOver")
        {
            //if (!base.gameObject.transform.GetChild(0).transform.Find("ReleasePanel").gameObject.activeSelf)
            //{
            //    base.gameObject.transform.GetChild(0).transform.Find("ReleasePanel").gameObject.SetActive(true);
            //    MonoBehaviour.print("======>" + mainscript.Instance.TotalTargets);
            //    MonoBehaviour.print("======>" + mainscript.Instance.TargetCounter1);
            //    base.gameObject.transform.GetChild(0).transform.Find("ReleasePanel").GetChild(0).GetComponent<Text>().text = "You are so Close! \n Only " + (mainscript.Instance.TotalTargets - mainscript.Instance.TargetCounter1) + " more Puppy to Release!";
            //    base.gameObject.transform.GetChild(0).transform.Find("BlackTransparent").gameObject.SetActive(true);
            //}
            //else
            //{
                this.ShowGameOver();
            //}
        }
        if (base.gameObject.name == "CongratulationPanel")
        {
            LoadingPanelAnim.instance.LoadingPanelAnimIN("map");
            base.StartCoroutine(this.LoadNewScene("map"));
        }
        if (base.gameObject.name == "MenuComplete")
        {
            if (PlayerPrefs.GetInt("OpenLevel") != 100)
            {
                LoadingPanelAnim.instance.LoadingPanelAnimIN("map");
                base.StartCoroutine(this.LoadNewScene("map"));
                MonoBehaviour.print(string.Concat(new object[]
                {
                    "Max level=======>",
                    PlayerPrefs.GetInt("MaxLevel"),
                    "<========OpenLevels======>",
                    PlayerPrefs.GetInt("OpenLevel")
                }));
                if (PlayerPrefs.GetInt("MaxLevel", 0) < PlayerPrefs.GetInt("OpenLevel"))
                {
                    PlayerPrefs.SetString("LevelPass", "Yes");
                    PlayerPrefs.SetInt("MaxLevel", PlayerPrefs.GetInt("OpenLevel"));
                    MonoBehaviour.print("Max level=======>" + PlayerPrefs.GetInt("MaxLevel"));
                }
                //GoogleMobileAdsDemoScript.instance.ShowInterstitial();
            }
            else
            {
                base.gameObject.SetActive(false);
                GameObject.Find("Canvas").transform.Find("CongratulationPanel").gameObject.SetActive(true);
            }
        }
        if (base.gameObject.name == "ExitPanel")
        {
            LoadingPanelAnim.instance.LoadingPanelAnimIN("map");
            base.StartCoroutine(this.LoadNewScene("map"));
        }
        if (base.gameObject.name == "MenuGameOver")
        {
            //GoogleMobileAdsDemoScript.instance.ShowInterstitial();
            LoadingPanelAnim.instance.LoadingPanelAnimIN("map");
            base.StartCoroutine(this.LoadNewScene("map"));
        }
        if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name == "game" && GamePlay.Instance.GameStatus == GameState.Pause)
        {
            GamePlay.Instance.GameStatus = GameState.WaitAfterClose;
        }
        if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name == "Settings")
        {
            LoadingPanelAnim.instance.LoadingPanelAnimIN("map");
            base.StartCoroutine(this.LoadNewScene("map"));
        }
        SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.swish[1]);
    }

    public void SetFalseObject(GameObject game)
    {
        SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.click);
        game.SetActive(false);
    }

    public void SetObject(GameObject game)
    {
        SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.click);
        game.SetActive(true);
    }

    public void Play()
    {
        SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.click);
        if (base.gameObject.name == "MenuPreGameOver")
        {
            if (GamePlay.GamePreover == 1)
            {
                if (InitScript.Gems >= 5)
                {
                    InitScript.Instance.SpendGems(5);
                    LevelData.LimitAmount += 5f;
                    GamePlay.Instance.GameStatus = GameState.WaitAfterClose;
                    base.gameObject.SetActive(false);
                    GameObject.Find("Canvas").transform.Find("Option").GetComponent<Animator>().enabled = true;
                    GameObject.Find("Canvas").transform.Find("Option").GetComponent<Animator>().Play("OptionPauseAniOut", -1, 0f);
                    MonoBehaviour.print("===========+>");
                    mainscript.Instance.Layer.SetActive(true);
                    mainscript.Instance.Set_false_layer_Function();
                    GameObject.Find("CanvasMoves").transform.Find("GameObject").GetComponent<Button>().interactable = true;
                }
                else
                {
                    MonoBehaviour.print("==-----else---===>");
                    this.BuyGemsGameScene();
                }
            }
            else if (GamePlay.GamePreover == 2)
            {
                if (InitScript.Gems >= 7)
                {
                    InitScript.Instance.SpendGems(7);
                    LevelData.LimitAmount += 10f;
                    GamePlay.Instance.GameStatus = GameState.WaitAfterClose;
                    base.gameObject.SetActive(false);
                    GameObject.Find("Canvas").transform.Find("Option").GetComponent<Animator>().enabled = true;
                    GameObject.Find("Canvas").transform.Find("Option").GetComponent<Animator>().Play("OptionPauseAniOut", -1, 0f);
                    mainscript.Instance.Layer.SetActive(true);
                    mainscript.Instance.Set_false_layer_Function();
                    GameObject.Find("CanvasMoves").transform.Find("GameObject").GetComponent<Button>().interactable = true;
                }
                else
                {
                    this.BuyGemsGameScene();
                }
            }
            else if (GamePlay.GamePreover == 3)
            {
                if (InitScript.Gems >= 10)
                {
                    InitScript.Instance.SpendGems(10);
                    LevelData.LimitAmount += 13f;
                    GamePlay.Instance.GameStatus = GameState.WaitAfterClose;
                    base.gameObject.SetActive(false);
                    GameObject.Find("Canvas").transform.Find("Option").GetComponent<Animator>().enabled = true;
                    GameObject.Find("Canvas").transform.Find("Option").GetComponent<Animator>().Play("OptionPauseAniOut", -1, 0f);
                    mainscript.Instance.Layer.SetActive(true);
                    mainscript.Instance.Set_false_layer_Function();
                    GameObject.Find("CanvasMoves").transform.Find("GameObject").GetComponent<Button>().interactable = true;
                }
                else
                {
                    this.BuyGemsGameScene();
                }
            }
            else if (InitScript.Gems >= 13)
            {
                InitScript.Instance.SpendGems(13);
                LevelData.LimitAmount += 13f;
                GamePlay.Instance.GameStatus = GameState.WaitAfterClose;
                base.gameObject.SetActive(false);
                GameObject.Find("Canvas").transform.Find("Option").GetComponent<Animator>().enabled = true;
                GameObject.Find("Canvas").transform.Find("Option").GetComponent<Animator>().Play("OptionPauseAniOut", -1, 0f);
            }
            else
            {
                this.BuyGemsGameScene();
            }
        }
        else if (base.gameObject.name == "MenuGameOver")
        {
            //GoogleMobileAdsDemoScript.instance.ShowInterstitial();
            LoadingPanelAnim.instance.LoadingPanelAnimIN("map");
            base.StartCoroutine(this.LoadNewScene("map"));
        }
        else if (base.gameObject.name == "MenuPlay")
        {
            if (InitScript.Lifes > 0)
            {
                InitScript.Instance.SpendLife(1);
                LoadingPanelAnim.instance.LoadingPanelAnimIN("game");
                base.StartCoroutine(this.LoadNewScene("game"));

                if(Counter>=Ads_API.Instance.ads_click )
                {
                    Counter = 0;
                    Ads_priority_script.Instance.Show_interrestial();
                }
                else
                {
                    Counter++;
                }

            }
            else
            {
                this.BuyLifeShop();
            }
        }
        else if (base.gameObject.name == "PlayMain")
        {
            //Loading_Panel.SetActive(true);
            PlayerPrefs.SetString("Scene", "menu");
            base.StartCoroutine(this.LoadNewScene_play("map"));
        }
    }

    public void On_click_watch_video()
    {
        Ads_priority_script.Instance.watch_video(Ads_reward_type.GameoverPre);
    }
    public void Watch_Video_reward()
    {
        if (GamePlay.GamePreover == 1)
        {
            LevelData.LimitAmount += 5f;

        }
        else if (GamePlay.GamePreover == 2)
        {
            LevelData.LimitAmount += 10f;

        }
        else if (GamePlay.GamePreover == 3)
        {
            LevelData.LimitAmount += 13f;

        }
        else if (InitScript.Gems >= 13)
        {
            LevelData.LimitAmount += 13f;
        }

        GamePlay.Instance.GameStatus = GameState.WaitAfterClose;
        base.gameObject.SetActive(false);
        GameObject.Find("Canvas").transform.Find("Option").GetComponent<Animator>().enabled = true;
        GameObject.Find("Canvas").transform.Find("Option").GetComponent<Animator>().Play("OptionPauseAniOut", -1, 0f);
        MonoBehaviour.print("===========+>");
        mainscript.Instance.Layer.SetActive(true);
        mainscript.Instance.Set_false_layer_Function();
        GameObject.Find("CanvasMoves").transform.Find("GameObject").GetComponent<Button>().interactable = true;
    }
    private IEnumerator LoadNewScene(string Scenename)
    {
        yield return new WaitForSeconds(1f);
        Application.LoadLevelAsync(Scenename);
        yield break;
    }

    private IEnumerator LoadNewScene_play(string Scenename)
    {
        yield return new WaitForSeconds(3f);
        Application.LoadLevelAsync(Scenename);
        yield break;
    }
    public void PlayTutorial()
    {
        GamePlay.Instance.GameStatus = GameState.Playing;
    }

    public void Next()
    {
        SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.click);
        this.CloseMenu();
    }

    public void BuyGems()
    {
        SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.click);
        CanvasManager.instance.Setobjects(gemshop_script.Instance.gemshopp);
    }

    public void Buy(GameObject pack)
    {
        SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.click);
        if (pack.name == "Pack1")
        {
            InitScript.waitedPurchaseGems = int.Parse(pack.transform.Find("Count").GetComponent<Text>().text.Replace("x ", string.Empty));
        }
        if (pack.name == "Pack2")
        {
            InitScript.waitedPurchaseGems = int.Parse(pack.transform.Find("Count").GetComponent<Text>().text.Replace("x ", string.Empty));
        }
        if (pack.name == "Pack3")
        {
            InitScript.waitedPurchaseGems = int.Parse(pack.transform.Find("Count").GetComponent<Text>().text.Replace("x ", string.Empty));
        }
        if (pack.name == "Pack4")
        {
            InitScript.waitedPurchaseGems = int.Parse(pack.transform.Find("Count").GetComponent<Text>().text.Replace("x ", string.Empty));
        }
        this.CloseMenu();
    }

    public void BuyLifeShop()
    {
        SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.click);
        if (InitScript.Lifes < InitScript.CapOfLife)
        {
            CanvasManager.instance.Setobjects(GameObject.Find("Canvas").transform.Find("LiveShop").gameObject);
        }
    }

    public void BuyLife(GameObject button)
    {
        SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.click);
        if (InitScript.Gems >= int.Parse(button.transform.Find("Price").GetComponent<Text>().text))
        {
            InitScript.Instance.SpendGems(int.Parse(button.transform.Find("Price").GetComponent<Text>().text));
            InitScript.Instance.RestoreLifes();
            GameObject.Find("Canvas").transform.Find("LiveShop").gameObject.SetActive(false);
            this.CloseMenu();
        }
        else
        {
            CanvasManager.instance.Setobjects(gemshop_script.Instance.gemshopp);
        }
    }

    public void ShowGameOver()
    {
        SoundBase.Instance.GetComponent<AudioSource>().Play();
        GameObject.Find("Canvas").transform.Find("MenuGameOver").gameObject.SetActive(true);
        GameObject.Find("CanvasMoves").transform.Find("GameObject").GetComponent<Button>().interactable = false;
        base.gameObject.SetActive(false);
    }

    public void ShowSettings(GameObject menuSettings)
    {
        SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.click);
        if (!menuSettings.activeSelf)
        {
            if (PlayerPrefs.GetInt("music", 1) == 1)
            {
                MonoBehaviour.print("musicon");
                this.music.GetComponent<Image>().sprite = this.musicon;
            }
            else
            {
                MonoBehaviour.print("musicoff");
                this.music.GetComponent<Image>().sprite = this.musicoff;
            }
            if (PlayerPrefs.GetInt("sound", 1) == 1)
            {
                MonoBehaviour.print("soundon");
                this.sound.GetComponent<Image>().sprite = this.soundon;
            }
            else
            {
                MonoBehaviour.print("sound0ff");
                this.sound.GetComponent<Image>().sprite = this.sound0ff;
            }
            menuSettings.SetActive(true);
        }
        else
        {
            menuSettings.SetActive(false);
        }
    }

    public void SoundOff()
    {
        if (this.sound.GetComponent<Image>().sprite == this.soundon)
        {
            PlayerPrefs.SetInt("sound", 0);
            SoundBase.Instance.GetComponent<AudioSource>().volume = 0f;
            this.sound.GetComponent<Image>().sprite = this.sound0ff;
            MonoBehaviour.print("SoundOff===>0");
        }
        else
        {
            MonoBehaviour.print("SoundOff===>1");
            PlayerPrefs.SetInt("sound", 1);
            this.sound.GetComponent<Image>().sprite = this.soundon;
            SoundBase.Instance.GetComponent<AudioSource>().volume = 1f;
        }
        PlayerPrefs.SetInt("Sound", (int)SoundBase.Instance.GetComponent<AudioSource>().volume);
        PlayerPrefs.Save();
    }

    public void MusicOff()
    {
        if (this.music.GetComponent<Image>().sprite == this.musicon)
        {
            PlayerPrefs.SetInt("music", 0);
            this.music.GetComponent<Image>().sprite = this.musicoff;
            GameObject.Find("Music").GetComponent<AudioSource>().volume = 0f;
        }
        else
        {
            PlayerPrefs.SetInt("music", 1);
            this.music.GetComponent<Image>().sprite = this.musicon;
            GameObject.Find("Music").GetComponent<AudioSource>().volume = 0.4f;
        }
        PlayerPrefs.SetInt("Music", (int)GameObject.Find("Music").GetComponent<AudioSource>().volume);
        PlayerPrefs.Save();
    }

    public void Info()
    {
        if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name == "map" || UnityEngine.SceneManagement.SceneManager.GetActiveScene().name == "menu")
        {
            GameObject.Find("Canvas").transform.Find("Tutorial").gameObject.SetActive(true);
        }
        else
        {
            GameObject.Find("Canvas").transform.Find("PreTutorial").gameObject.SetActive(true);
        }
    }

    public void Quit()
    {
        if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name == "game")
        {
            Ads_priority_script.Instance.Show_interrestial();
            LoadingPanelAnim.instance.LoadingPanelAnimIN("map");
            base.StartCoroutine(this.LoadNewScene("map"));
        }
        else
        {
            Application.Quit();
        }
    }

    public void FiveBallsBoost()
    {
        if (GamePlay.Instance.GameStatus != GameState.Playing)
        {
            return;
        }
        SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.click);
        if (InitScript.Instance.FiveBallsBoost > 0)
        {
            if (GamePlay.Instance.GameStatus == GameState.Playing)
            {
                InitScript.Instance.SpendBoost(BoostType.FiveBallsBoost);
                mainscript.Instance.PauseButtonPopOut(GameObject.Find("PauseMenu"));
            }
        }
        else
        {
            this.OpenBoostShop(BoostType.FiveBallsBoost);
        }
    }

    public void ColorBallBoost()
    {
        if (GamePlay.Instance.GameStatus != GameState.Playing)
        {
            return;
        }
        SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.click);
        if (InitScript.Instance.ColorBallBoost > 0)
        {
            if (GamePlay.Instance.GameStatus == GameState.Playing)
            {
                InitScript.Instance.SpendBoost(BoostType.ColorBallBoost);
                mainscript.Instance.PauseButtonPopOut(GameObject.Find("PauseMenu"));
            }
        }
        else
        {
            this.OpenBoostShop(BoostType.ColorBallBoost);
        }
    }

    public void FireBallBoost()
    {
        if (GamePlay.Instance.GameStatus != GameState.Playing)
        {
            return;
        }
        SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.click);
        if (InitScript.Instance.FireBallBoost > 0)
        {
            if (GamePlay.Instance.GameStatus == GameState.Playing)
            {
                InitScript.Instance.SpendBoost(BoostType.FireBallBoost);
                mainscript.Instance.PauseButtonPopOut(GameObject.Find("PauseMenu"));
            }
        }
        else
        {
            this.OpenBoostShop(BoostType.FireBallBoost);
        }
    }

    public void OpenBoostShop(BoostType boosType)
    {
        SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.click);
        GameObject.Find("Canvas").transform.Find("BoostShop").gameObject.GetComponent<BoostShop>().SetBoost(boosType);
    }

    public void BuyBoost(BoostType boostType, int price)
    {
        SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.click);
        if (InitScript.Gems >= price)
        {
            InitScript.Instance.BuyBoost(boostType, 1, price);
            this.CloseMenu();
        }
        else
        {
            this.BuyGemsGameScene();
        }
    }


    public void BuyBoost_watch_videoo_fun(BoostType boostType, int price)
    {
        Ads_priority_script.Instance.Buy_boost_watch_video(boostType, price);
    }

    public void BuyGemsGameScene()
    {
        SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.click);
        gemshop_script.Instance.gemshopp.SetActive(true);
    }

    public GameObject Loading_Panel;

    public bool PlayOnEnable = true;

    private bool WaitForPickupFriends;

    private bool WaitForAksFriends;

    private Dictionary<string, string> parameters;

    public GameObject sound;

    public GameObject music;

    public Sprite musicoff;

    public Sprite musicon;

    public Sprite soundon;

    public Sprite sound0ff;
}
