using System;
using UnityEngine;
using UnityEngine.UI;

public class TargetIcon : MonoBehaviour
{
	private void Start()
	{
		base.GetComponent<Image>().sprite = this.targets[(int)LevelData.mode];
	}

	private void Update()
	{
	}

	public Sprite[] targets;
}
