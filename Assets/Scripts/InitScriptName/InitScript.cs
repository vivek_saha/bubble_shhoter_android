using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InitScriptName
{
	public class InitScript : MonoBehaviour
	{
		public void Awake()
		{
			InitScript.Instance = this;
			if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name == "map" && GameObject.Find("Canvas").transform.Find("MenuPlay").gameObject.activeSelf)
			{
				GameObject.Find("Canvas").transform.Find("MenuPlay").gameObject.SetActive(false);
			}
			InitScript.RestLifeTimer = PlayerPrefs.GetFloat("RestLifeTimer");
			InitScript.DateOfExit = PlayerPrefs.GetString("DateOfExit", string.Empty);
			InitScript.Gems = PlayerPrefs.GetInt("Gems");
			InitScript.BallBoostBuy = PlayerPrefs.GetInt("BallBoostBuy");
			InitScript.Lifes = PlayerPrefs.GetInt("Lifes");
			if (PlayerPrefs.GetInt("Lauched") == 0)
			{
				InitScript.FirstTime = true;
				InitScript.Lifes = InitScript.CapOfLife;
				InitScript.Gems = (int)Setting_API.Instance.wallet_coin;
				PlayerPrefs.SetInt("Gems", InitScript.Gems);
				PlayerPrefs.SetInt("Lifes", InitScript.Lifes);
				PlayerPrefs.SetInt("Lauched", 1);
				PlayerPrefs.SetInt("Music", 1);
				PlayerPrefs.SetInt("Sound", 1);
				PlayerPrefs.Save();
			}
			this.ReloadBoosts();
			InitScript.boostPurchased = false;
		}

		private void Start()
		{
			if (Application.platform == RuntimePlatform.WindowsEditor)
			{
				Application.targetFrameRate = 60;
			}

			//test_purpose
			//AddGems(5000);
        }

		private void Update()
		{
		}

		public void AddGems(int count)
		{
			InitScript.Gems += count;
			PlayerPrefs.SetInt("Gems", InitScript.Gems);
			PlayerPrefs.Save();
			//Update_API.Instance.Update_data();
		}
		public void SetGems(int count)
		{
			InitScript.Gems = count;
			PlayerPrefs.SetInt("Gems", InitScript.Gems);
			PlayerPrefs.Save();
			//Update_API.Instance.Update_data();
		}

		public void SpendGems(int count)
		{
			InitScript.Gems -= count;
			PlayerPrefs.SetInt("Gems", InitScript.Gems);
			PlayerPrefs.Save();
			//Update_API.Instance.Update_data();
		}

		public void RestoreLifes()
		{
			InitScript.Lifes = InitScript.CapOfLife;
			PlayerPrefs.SetInt("Lifes", InitScript.Lifes);
			PlayerPrefs.Save();
		}

		public void AddLife(int count)
		{
			InitScript.Lifes += count;
			if (InitScript.Lifes > InitScript.CapOfLife)
			{
				InitScript.Lifes = InitScript.CapOfLife;
			}
			PlayerPrefs.SetInt("Lifes", InitScript.Lifes);
			PlayerPrefs.Save();
		}

		public int GetLife()
		{
			if (InitScript.Lifes > InitScript.CapOfLife)
			{
				InitScript.Lifes = InitScript.CapOfLife;
				PlayerPrefs.SetInt("Lifes", InitScript.Lifes);
				PlayerPrefs.Save();
			}
			return InitScript.Lifes;
		}

		public void PurchaseSucceded()
		{
			this.AddGems(InitScript.waitedPurchaseGems);
			InitScript.waitedPurchaseGems = 0;
		}

		public void SpendLife(int count)
		{
			if (InitScript.Lifes > 0)
			{
				InitScript.Lifes -= count;
				PlayerPrefs.SetInt("Lifes", InitScript.Lifes);
				PlayerPrefs.Save();
			}
		}

		public void BuyBoost(BoostType boostType, int count, int price)
		{
			this.SpendGems(price);
			PlayerPrefs.SetInt(string.Empty + boostType, PlayerPrefs.GetInt(string.Empty + boostType) + count);
			MonoBehaviour.print(string.Concat(new object[]
			{
				"=if=--------",
				boostType,
				"======",
				PlayerPrefs.GetInt(string.Empty + boostType)
			}));
			PlayerPrefs.Save();
			this.ReloadBoosts();
		}



		public void BuyBoost_watch_Video(BoostType boostType, int count)
        {
			PlayerPrefs.SetInt(string.Empty + boostType, PlayerPrefs.GetInt(string.Empty + boostType) + count);
			MonoBehaviour.print(string.Concat(new object[]
			{
				"=if=--------",
				boostType,
				"======",
				PlayerPrefs.GetInt(string.Empty + boostType)
			}));
			PlayerPrefs.Save();
			this.ReloadBoosts();
		}
		public void SpendBoost(BoostType boostType)
		{
			InitScript.Instance.BoostActivated = true;
			if (boostType != BoostType.FiveBallsBoost)
			{
				mainscript.Instance.boxCatapult.GetComponent<Grid>().Busy.GetComponent<ball>().SetBoost(boostType);
			}
			else if (InitScript.Instance.FiveBallsBoost > 0)
			{
				LevelData.LimitAmount += 5f;
			}
			PlayerPrefs.SetInt(string.Empty + boostType, PlayerPrefs.GetInt(string.Empty + boostType) - 1);
			PlayerPrefs.Save();
			this.ReloadBoosts();
		}

		public void ReloadBoosts()
		{
			this.FiveBallsBoost = PlayerPrefs.GetInt(string.Empty + BoostType.FiveBallsBoost);
			this.ColorBallBoost = PlayerPrefs.GetInt(string.Empty + BoostType.ColorBallBoost);
			this.FireBallBoost = PlayerPrefs.GetInt(string.Empty + BoostType.FireBallBoost);
		}

		public int LoadLevelStarsCount(int level)
		{
			return (level <= 10) ? (level % 3 + 1) : 0;
		}

		public void SaveLevelStarsCount(int level, int starsCount)
		{
			UnityEngine.Debug.Log(string.Format("Stars count {0} of level {1} saved.", starsCount, level));
		}

		public void ClearLevelProgress(int level)
		{
		}

		public void OnLevelClicked(int number)
		{
			if (!GameObject.Find("Canvas").transform.Find("MenuPlay").gameObject.activeSelf)
			{
				PlayerPrefs.SetInt("OpenLevel", number);
				PlayerPrefs.Save();
				InitScript.openLevel = number;
				this.currentTarget = LevelData.GetTarget(number);
				CanvasManager.instance.Setobjects(GameObject.Find("Canvas").transform.Find("MenuPlay").gameObject);
			}
		}

		private void OnApplicationPause(bool pauseStatus)
		{
			if (pauseStatus)
			{
				if (InitScript.RestLifeTimer > 0f)
				{
					PlayerPrefs.SetFloat("RestLifeTimer", InitScript.RestLifeTimer);
				}
				PlayerPrefs.SetInt("Lifes", InitScript.Lifes);
				PlayerPrefs.SetString("DateOfExit", DateTime.Now.ToString());
				PlayerPrefs.SetInt("Gems", InitScript.Gems);
				PlayerPrefs.Save();
			}
		}

		private void OnEnable()
		{
		}

		private void OnDisable()
		{
			PlayerPrefs.SetFloat("RestLifeTimer", InitScript.RestLifeTimer);
			PlayerPrefs.SetInt("Lifes", InitScript.Lifes);
			if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex != 2)
			{
				PlayerPrefs.SetString("DateOfExit", DateTime.Now.ToString());
			}
			PlayerPrefs.SetInt("Gems", InitScript.Gems);
			PlayerPrefs.Save();
		}

		public static InitScript Instance;

		private int _levelNumber = 1;

		private int _starsCount = 1;

		private bool _isShow;

		public static int openLevel;

		public static bool boostJelly;

		public static bool boostMix;

		public static bool boostChoco;

		public static bool sound;

		public static bool music;

		public static int waitedPurchaseGems;

		public static List<string> selectedFriends;

		public static bool Lauched;

		public static int scoresForLeadboardSharing;

		public static int lastPlace;

		public static int savelastPlace;

		public static bool beaten;

		public static List<string> Beatedfriends;

		private int messCount;

		public static bool loggedIn;

		public GameObject EMAIL;

		public GameObject MessagesBox;

		public static bool FirstTime;

		public static int Lifes;

		public static int CapOfLife = 5;

		public static int Gems;

		public static int BallBoostBuy;

		public static float RestLifeTimer;

		public static string DateOfExit;

		public static DateTime today;

		public static DateTime DateOfRestLife;

		public static string timeForReps;

		public static bool openNext;

		public static bool openAgain;

		public int FiveBallsBoost;

		public int ColorBallBoost;

		public int FireBallBoost;

		public bool BoostActivated;

		private Hashtable mapFriends = new Hashtable();

		public Target currentTarget;

		public static bool boostPurchased;
	}
}
