using System;
using UnityEngine;

public class RemainLine : MonoBehaviour
{
	private void Start()
	{
		this.line = base.GetComponent<LineRenderer>();
		if (Camera.main.orthographicSize == 5f)
		{
			base.transform.position = Vector3.zero + Vector3.up * 5.8f;
		}
		else if (Camera.main.orthographicSize == 6f)
		{
			base.transform.position = Vector3.zero + Vector3.up * 6.7f;
		}
		this.line.SetPosition(0, new Vector3(-8f, base.transform.position.y, 0f));
		this.line.SetPosition(1, new Vector3(8f, base.transform.position.y, 0f));
	}

	public void UpdateLine(float step)
	{
		this.line.SetPosition(0, new Vector3(-8f + step / 2f, base.transform.position.y, 0f));
		this.line.SetPosition(1, new Vector3(8f - step / 2f, base.transform.position.y, 0f));
	}

	public void ResetLine()
	{
		this.line.SetPosition(0, new Vector3(-8f, base.transform.position.y, 0f));
		this.line.SetPosition(1, new Vector3(8f, base.transform.position.y, 0f));
	}

	private void Update()
	{
	}

	private LineRenderer line;
}
