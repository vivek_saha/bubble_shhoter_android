﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;
public class nem_popup_script : MonoBehaviour
{
    public TextMeshProUGUI msg;
    // Start is called before the first frame update
    void Start()
    {
        Move_up();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Move_up()
    {
        transform.DOLocalMoveY(0f, 1f).OnComplete(() => { Destroy(this.gameObject,1f); }).SetUpdate(true);
    }

    
}
