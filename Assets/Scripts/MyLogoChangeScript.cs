using System;
using System.Collections;
using UnityEngine;

public class MyLogoChangeScript : MonoBehaviour
{
	public static MyLogoChangeScript instance { get; set; }

	private void Start()
	{
		MyLogoChangeScript.instance = this;
		this.click_enable = false;
		if (PlayerPrefs.GetString("Scene") == "menu")
		{
			if (PlayerPrefs.GetInt("MaxLevel", 0) == 0)
			{
				this.Prv = GameObject.Find("Map1").transform.GetChild(PlayerPrefs.GetInt("MaxLevel", 0)).transform.position + new Vector3(0f, 1.5f, 0f);
				this.RippleParticle.transform.position = this.Prv - new Vector3(0f, 1.5f, 0f);
				base.transform.position = this.Prv;
			}
			else
			{
				base.transform.position = GameObject.Find("Map1").transform.GetChild(PlayerPrefs.GetInt("MaxLevel", 0)).transform.position + new Vector3(0f, 1.5f, 0f);
				this.RippleParticle.transform.position = base.transform.position - new Vector3(0f, 1.5f, 0f);
			}
			base.StartCoroutine(this.Start23());
		}
		else if (PlayerPrefs.GetString("LevelPass") == "Yes")
		{
			this.Prv = GameObject.Find("Map1").transform.GetChild(PlayerPrefs.GetInt("MaxLevel", 0) - 1).transform.position + new Vector3(0f, 1.5f, 0f);
			this.RippleParticle.transform.position = this.Prv - new Vector3(0f, 1.5f, 0f);
			base.transform.position = this.Prv;
		}
		else
		{
			base.transform.position = GameObject.Find("Map1").transform.GetChild(PlayerPrefs.GetInt("MaxLevel", 0)).transform.position + new Vector3(0f, 1.5f, 0f);
			this.RippleParticle.transform.position = base.transform.position - new Vector3(0f, 1.5f, 0f);
		}
		PlayerPrefs.SetString("Scene", "map");
	}

	private void Update()
	{
		if (this.click_enable)
		{
			base.transform.position = Vector3.MoveTowards(base.transform.position, this.Next, 0.1f);
			if (base.transform.position == this.Next)
			{
				this.click_enable = false;
				this.RippleParticle.transform.position = base.transform.position - new Vector3(0f, 1.5f, 0f);
				base.StartCoroutine(this.Start23());
				base.StartCoroutine(this.StartLevelMap());
			}
		}
	}

	public void MovePlayer()
	{
		this.click_enable = false;
		if (PlayerPrefs.GetInt("MaxLevel", 0) == 0)
		{
			this.Prv = GameObject.Find("Map1").transform.GetChild(PlayerPrefs.GetInt("MaxLevel", 0)).transform.position + new Vector3(0f, 1.5f, 0f);
			base.transform.position = this.Prv;
		}
		else
		{
			this.Prv = GameObject.Find("Map1").transform.GetChild(PlayerPrefs.GetInt("MaxLevel", 0) - 1).transform.position + new Vector3(0f, 1.5f, 0f);
			base.transform.position = this.Prv;
			this.Next = GameObject.Find("Map1").transform.GetChild(PlayerPrefs.GetInt("MaxLevel", 0)).transform.position + new Vector3(0f, 1.5f, 0f);
			this.click_enable = true;
		}
	}

	private IEnumerator Start23()
	{
		Vector3 pointA = base.transform.position;
		Vector3 pointB = base.transform.position + new Vector3(0f, 0.3f, 0f);
		for (;;)
		{
			yield return base.StartCoroutine(this.MoveObject(base.transform, pointA, pointB, 0.7f));
			yield return base.StartCoroutine(this.MoveObject(base.transform, pointB, pointA, 0.7f));
		}
		yield break;
	}

	private IEnumerator MoveObject(Transform thisTransform, Vector3 startPos, Vector3 endPos, float time)
	{
		float i = 0f;
		float rate = 1f / time;
		while (i < 1f)
		{
			i += Time.deltaTime * rate;
			thisTransform.position = Vector3.Lerp(startPos, endPos, i);
			yield return null;
		}
		yield break;
	}

	private IEnumerator StartLevelMap()
	{
		yield return new WaitForSeconds(1.5f);
		Level.instance.StartLevelMap();
		yield break;
	}

	private bool click_enable;

	public Vector3 Prv;

	public Vector3 Next;

	public GameObject RippleParticle;
}
