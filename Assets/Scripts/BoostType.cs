using System;

public enum BoostType
{
	FiveBallsBoost,
	ColorBallBoost,
	FireBallBoost
}
