using System;
using UnityEngine;

public class LockLevelRounded : MonoBehaviour
{
	private void Start()
	{
		LockLevelRounded.Instance = this;
		this.staticPos = base.transform.position;
		this.newRot = Quaternion.identity;
	}

	public void Rotate(Vector3 _dir, Vector3 _ballPos)
	{
		_dir = mainscript.Instance.boxCatapult.GetComponent<Grid>().transform.position;
		this.angle = Vector2.Angle(_dir - _ballPos, _ballPos - base.transform.position) / 4f;
		if (base.transform.position.x < _ballPos.x)
		{
			this.angle *= -1f;
		}
		this.newRot = base.transform.rotation * Quaternion.AngleAxis(this.angle, Vector3.back);
		this.addForce = true;
		SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.kreakWheel);
	}

	private void Update()
	{
		if (base.transform.rotation != this.newRot)
		{
			base.transform.rotation = Quaternion.Lerp(base.transform.rotation, this.newRot, Time.deltaTime);
		}
	}

	private Vector3 staticPos;

	public static LockLevelRounded Instance;

	private Vector3 dir;

	private Vector3 ballPos;

	private float angle;

	private Quaternion newRot;

	private bool addForce;
}
