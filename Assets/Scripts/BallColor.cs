using System;

public enum BallColor
{
	blue = 1,
	green,
	red,
	violet,
	yellow,
	random,
	chicken,
	bluebaby,
	greenbaby,
	redbaby,
	violetbaby,
	yellowbaby,
	randombaby
}
