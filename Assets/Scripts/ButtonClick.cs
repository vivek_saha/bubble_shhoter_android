using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class ButtonClick : MonoBehaviour
{
	private void Start()
	{
	}

	private void Update()
	{
		Ray ray = Camera.main.ScreenPointToRay(UnityEngine.Input.mousePosition);
		RaycastHit raycastHit = default(RaycastHit);
		if (Input.GetMouseButtonDown(0) && !EventSystem.current.currentSelectedGameObject && Physics.Raycast(ray, out raycastHit) && raycastHit.collider.name == "MapButton")
		{
			this.YAxisKeyDownCamera = Camera.main.transform.position.y;
		}
		if (Input.GetMouseButtonUp(0) && !EventSystem.current.currentSelectedGameObject && Physics.Raycast(ray, out raycastHit) && raycastHit.collider.name == "MapButton")
		{
			float y = Camera.main.transform.position.y;
			if (this.YAxisKeyDownCamera == y)
			{
				raycastHit.collider.GetComponent<Level>().StartLevel();
			}
		}
	}

	private float YAxisKeyDownCamera;
}
