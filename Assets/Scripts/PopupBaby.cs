using System;
using UnityEngine;

public class PopupBaby : MonoBehaviour
{
	private void Start()
	{
		this.onetime = false;
	}

	private void Update()
	{
		if (!base.gameObject.GetComponent<SpriteRenderer>().enabled && !this.onetime)
		{
			this.onetime = true;
			base.gameObject.transform.GetChild(0).gameObject.SetActive(true);
		}
	}

	private bool onetime;
}
