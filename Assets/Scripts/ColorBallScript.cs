using System;
using UnityEngine;

public class ColorBallScript : MonoBehaviour
{
	private void Start()
	{
	}

	public void SetColor(BallColor color)
	{
		this.mainColor = color;
		foreach (Sprite sprite in this.sprites)
		{
			if (sprite.name == "ball_" + color)
			{
				base.GetComponent<SpriteRenderer>().sprite = sprite;
				this.SetSettings(color);
				base.gameObject.tag = string.Empty + color;
			}
		}
	}

	public void SetColorBaby(BallColor color)
	{
		this.mainColor = color;
		foreach (Sprite sprite in this.sprites)
		{
			if (sprite.name == "ball_" + color + "baby")
			{
				base.GetComponent<SpriteRenderer>().sprite = sprite;
				base.gameObject.tag = string.Empty + color + "baby";
			}
		}
	}

	private void SetSettings(BallColor color)
	{
		if (color != BallColor.chicken || LevelData.mode == ModeGame.Rounded)
		{
		}
	}

	public void SetColor(int color)
	{
		this.mainColor = (BallColor)color;
		base.GetComponent<SpriteRenderer>().sprite = this.sprites[color];
	}

	public void ChangeRandomColor()
	{
		mainscript.Instance.GetColorsInGame();
		this.SetColor(mainscript.colorsDict[UnityEngine.Random.Range(0, mainscript.colorsDict.Count)]);
		base.GetComponent<Animation>().Stop();
	}

	private void Update()
	{
		if (base.transform.position.y <= -16f && base.transform.parent == null)
		{
			UnityEngine.Object.Destroy(base.gameObject);
		}
	}

	public Sprite[] sprites;

	public BallColor mainColor;
}
