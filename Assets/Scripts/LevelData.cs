using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;

public class LevelData
{
	public static float LimitAmount
	{
		get
		{
			return LevelData.limitAmount;
		}
		set
		{
			LevelData.limitAmount = value;
			if (value < 0f)
			{
				LevelData.limitAmount = 0f;
			}
		}
	}

	public static void LoadDataFromXML(int currentLevel)
	{
		LevelData.requestMissions.Clear();
		TextAsset xmlString = Resources.Load("Levels/" + currentLevel) as TextAsset;
		LevelData.ProcessGameDataFromXML(xmlString);
	}

	public static void LoadDataFromLocal(int currentLevel)
	{
		LevelData.requestMissions.Clear();
		TextAsset textAsset = Resources.Load("Levels/" + currentLevel) as TextAsset;
		LevelData.ProcessGameDataFromString(textAsset.text);
	}

	public static void LoadDataFromURL(int currentLevel)
	{
	}

	private static void ProcessGameDataFromString(string mapText)
	{
		string[] array = mapText.Split(new string[]
		{
			"\n"
		}, StringSplitOptions.RemoveEmptyEntries);
		int num = 0;
		foreach (string text in array)
		{
			if (text.StartsWith("GM="))
			{
				string s = text.Replace("GM=", string.Empty).Trim();
				int num2 = int.Parse(s);
				LevelData.mode = (ModeGame)num2;
			}
			else if (text.StartsWith("LMT="))
			{
				string s2 = text.Replace("LMT=", string.Empty).Trim();
				LevelData.limitAmount = (float)int.Parse(s2);
			}
			else if (text.StartsWith("MNS"))
			{
				string text2 = text.Replace("MNS", string.Empty).Trim();
				string[] array3 = text2.Split(new string[]
				{
					"/"
				}, StringSplitOptions.RemoveEmptyEntries);
				for (int j = 0; j < array3.Length; j++)
				{
					int num3 = int.Parse(array3[j].Trim());
					MissionType missionType = (MissionType)j;
					if (num3 > 0)
					{
						LevelData.requestMissions.Add(new Mission(num3, missionType));
					}
				}
			}
			else if (text.StartsWith("data="))
			{
				LevelData.startReadData = true;
			}
			else if (LevelData.startReadData)
			{
				string[] array4 = text.Replace("\r", string.Empty).Split(new string[]
				{
					","
				}, StringSplitOptions.RemoveEmptyEntries);
				for (int k = 0; k < array4.Length; k++)
				{
					int num4 = int.Parse(array4[k].Trim());
					LevelData.map[num * creatorBall.columns + k] = num4;
				}
				num++;
			}
		}
	}

	private static void ProcessGameDataFromXML(TextAsset xmlString)
	{
		XmlDocument xmlDocument = new XmlDocument();
		xmlDocument.LoadXml(xmlString.text);
		XmlNodeList elementsByTagName = xmlDocument.GetElementsByTagName("property");
		IEnumerator enumerator = elementsByTagName.GetEnumerator();
		try
		{
			while (enumerator.MoveNext())
			{
				object obj = enumerator.Current;
				XmlElement xmlElement = (XmlElement)obj;
				if (xmlElement.GetAttribute("name") == "GM")
				{
					LevelData.mode = (ModeGame)int.Parse(xmlElement.GetAttribute("value"));
				}
				if (xmlElement.GetAttribute("name") == "LMT")
				{
					LevelData.limitAmount = (float)int.Parse(xmlElement.GetAttribute("value"));
				}
				if (xmlElement.GetAttribute("name") == "COLORS")
				{
					LevelData.colors = int.Parse(xmlElement.GetAttribute("value"));
				}
				if (xmlElement.GetAttribute("name") == "STAR1")
				{
					LevelData.star1 = int.Parse(xmlElement.GetAttribute("value"));
				}
				if (xmlElement.GetAttribute("name") == "STAR2")
				{
					LevelData.star2 = int.Parse(xmlElement.GetAttribute("value"));
				}
				if (xmlElement.GetAttribute("name") == "STAR3")
				{
					LevelData.star3 = int.Parse(xmlElement.GetAttribute("value"));
				}
			}
		}
		finally
		{
			IDisposable disposable;
			if ((disposable = (enumerator as IDisposable)) != null)
			{
				disposable.Dispose();
			}
		}
		elementsByTagName = xmlDocument.GetElementsByTagName("tile");
		LevelData.colorsDict.Clear();
		LevelData.key = 0;
		for (int i = 0; i < creatorBall.rows; i++)
		{
			for (int j = 0; j < creatorBall.columns; j++)
			{
				XmlElement xmlElement2 = (XmlElement)elementsByTagName[i * creatorBall.columns + j];
				int num = int.Parse(xmlElement2.GetAttribute("gid"));
				if (!LevelData.colorsDict.ContainsValue((BallColor)num) && num > 0 && num < 6)
				{
					LevelData.colorsDict.Add(LevelData.key, (BallColor)num);
					LevelData.key++;
				}
				LevelData.map[i * creatorBall.columns + j] = num;
			}
		}
		if (LevelData.colorsDict.Count == 0)
		{
			LevelData.colorsDict.Add(0, BallColor.yellow);
			LevelData.colorsDict.Add(1, BallColor.red);
			List<BallColor> list = new List<BallColor>();
			list.Add(BallColor.blue);
			list.Add(BallColor.green);
			if (LevelData.mode != ModeGame.Rounded)
			{
				list.Add(BallColor.violet);
			}
			for (int k = 0; k < LevelData.colors - 2; k++)
			{
				BallColor value = BallColor.yellow;
				while (LevelData.colorsDict.ContainsValue(value))
				{
					value = list[UnityEngine.Random.RandomRange(0, list.Count)];
				}
				LevelData.colorsDict.Add(2 + k, value);
			}
		}
	}

	public static int GetScoreTarget(int currentLevel)
	{
		LevelData.LoadDataFromLocal(currentLevel);
		return LevelData.GetMission(MissionType.Stars).amount;
	}

	public static Mission GetMission(MissionType type)
	{
		return LevelData.requestMissions.Find((Mission obj) => obj.type == type);
	}

	public static Target GetTarget(int levelNumber)
	{
		LevelData.LoadLevel(levelNumber);
		return (Target)LevelData.mode;
	}

	public static bool LoadLevel(int currentLevel)
	{
		TextAsset textAsset = Resources.Load("Levels/" + currentLevel) as TextAsset;
		if (textAsset == null)
		{
			textAsset = (Resources.Load("Levels/" + currentLevel) as TextAsset);
		}
		LevelData.ProcesDataFromString(textAsset.text);
		return true;
	}

	private static void ProcesDataFromString(string mapText)
	{
		string[] array = mapText.Split(new string[]
		{
			"\n"
		}, StringSplitOptions.RemoveEmptyEntries);
		LevelData.colorsDict.Clear();
		foreach (string text in array)
		{
			if (text.StartsWith("MODE "))
			{
				string s = text.Replace("MODE", string.Empty).Trim();
				LevelData.mode = (ModeGame)int.Parse(s);
			}
		}
	}

	public static LevelData Instance;

	public static int[] map = new int[770];

	public static List<Mission> requestMissions = new List<Mission>();

	public static ModeGame mode = ModeGame.Vertical;

	private static float limitAmount = 40f;

	private static bool startReadData;

	public static Dictionary<int, BallColor> colorsDict = new Dictionary<int, BallColor>();

	private static int key;

	public static int colors;

	public static int star1;

	public static int star2;

	public static int star3;

	public static int ColletPuppy;
}
