using System;
using System.Collections;
using UnityEngine;

public class DogMovement : MonoBehaviour
{
	private IEnumerator Start()
	{
		Vector3 pointA = base.transform.position;
		Vector3 pointB = base.transform.position + new Vector3(2f, 0f, 0f);
		for (;;)
		{
			base.transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
			yield return base.StartCoroutine(this.MoveObject(base.transform, pointA, pointB, 2.2f));
			base.transform.localRotation = Quaternion.Euler(0f, 180f, 0f);
			yield return base.StartCoroutine(this.MoveObject(base.transform, pointB, pointA, 2.2f));
		}
		yield break;
	}

	private IEnumerator MoveObject(Transform thisTransform, Vector3 startPos, Vector3 endPos, float time)
	{
		float i = 0f;
		float rate = 1f / time;
		while (i < 1f)
		{
			this.Timer -= Time.deltaTime;
			if (this.Timer > 0f)
			{
				i += Time.deltaTime * rate;
				thisTransform.position = Vector3.Lerp(startPos, endPos, i);
				yield return null;
			}
			else
			{
				base.GetComponent<SpriteRenderer>().sprite = this.defaultDogSprite;
				base.GetComponent<Animator>().enabled = false;
				yield return new WaitForSeconds((float)UnityEngine.Random.Range(0, 10));
				base.GetComponent<Animator>().enabled = true;
				this.Timer = (float)UnityEngine.Random.Range(0, 10);
			}
		}
		yield break;
	}

	public Sprite defaultDogSprite;

	public float Timer = 5f;
}
