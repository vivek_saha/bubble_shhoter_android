using System;
using UnityEngine;
using UnityEngine.UI;

public class PreTutorial : MonoBehaviour
{
	private void Start()
	{
		base.GetComponent<Image>().sprite = this.pictures[(int)LevelData.mode];
		SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.swish[0]);
	}

	public void Stop()
	{
		SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.swish[1]);
		GamePlay.Instance.GameStatus = GameState.Tutorial;
		base.gameObject.SetActive(false);
	}

	public void AnimOptionButton()
	{
		GameObject.Find("Canvas").transform.Find("Option").GetComponent<Animator>().enabled = true;
		GameObject.Find("Canvas").transform.Find("Option").GetComponent<Animator>().Play("OptionPauseAniOut", -1, 0f);
	}

	public Sprite[] pictures;
}
