﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using InitScriptName;


public class IAPManager : Singleton<IAPManager>, IStoreListener
{

    //public static IAPManager I;
    private static IStoreController m_StoreController;
    private static IExtensionProvider m_StoreExtensionProvider;

    public string[] coin;

    //public string st_purchase_coin;
    void Start()
    {


    }



    private void Awake()
    {
        //if (I != null && I != this)
        //{
        //    Destroy(this.gameObject);
        //}


        //I = this;
        //DontDestroyOnLoad(this.gameObject);

    }

    public void manual_start()
    {
        coin = new string[GetComponent<IAP_API>().product_id.Length];
        for (int i = 0; i < GetComponent<IAP_API>().product_id.Length; i++)
        {
            coin[i] = GetComponent<IAP_API>().product_id[i];
            Debug.Log(GetComponent<IAP_API>().product_id[i]);
        }

        //if(IAP_API.Instance.St_purchase_product_id!=null)
        //{
        //    st_purchase_coin = IAP_API.Instance.St_purchase_product_id;
        //}
        //foreach (string a in coin)
        //{
        //    print("in manual_start: " + a);
        //}

        if (m_StoreController == null)
        {
            InitializePurchasing();
        }
        //Debug.Log(coin.Length);
    }
    public void InitializePurchasing()
    {
        Debug.Log("IsInitialized : " + IsInitialized());
        if (IsInitialized())
        {
            return;
        }

        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

        //foreach (string a in coin)
        //{
        //    print("in InitializePurchasing: " + a);
        //}
        //Debug.Log(coin)
        for (int i = 0; i < coin.Length; i++)
        {
            Debug.Log("Coin  :" + coin[i]);
            builder.AddProduct(coin[i], ProductType.Consumable);
        }
        //if (st_purchase_coin != null)
        //{
        //    builder.AddProduct(st_purchase_coin, ProductType.Consumable);
        //}
        UnityPurchasing.Initialize(this, builder);
        Debug.Log("IsInitialized : " + IsInitialized());
    }

    private bool IsInitialized()
    {
        return m_StoreController != null && m_StoreExtensionProvider != null;
    }

    public void BuyProductID(string productId)
    {
        try
        {
            if (IsInitialized())
            {
                Product product = m_StoreController.products.WithID(productId);

                if (product != null && product.availableToPurchase)
                {
                    Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));// ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed asynchronously.
                    m_StoreController.InitiatePurchase(product);
                }
                else
                {
                    Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
                }
            }
            else
            {
                Debug.Log("BuyProductID FAIL. Not initialized.");
            }
        }
        catch (Exception e)
        {
            Debug.Log("BuyProductID: FAIL. Exception during purchase. " + e);
        }
    }

    public void RestorePurchases()
    {
        if (!IsInitialized())
        {
            Debug.Log("RestorePurchases FAIL. Not initialized.");
            return;
        }

        if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.OSXPlayer)
        {
            Debug.Log("RestorePurchases started ...");

            var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
            apple.RestoreTransactions((result) =>
            {
                Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
            });
        }
        else
        {
            Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
        }
    }

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        //Debug.Log("OnInitialized: Completed!");

        m_StoreController = controller;
        m_StoreExtensionProvider = extensions;
    }

    public void OnInitializeFailed(InitializationFailureReason error)
    {
        Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
    }

    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));

        //args.purchasedProduct.transactionID

//        if (String.Equals(args.purchasedProduct.definition.id, st_purchase_coin, StringComparison.Ordinal))
//        {
//#if UNITY_EDITOR
//            //Debug.Log("try::"+args.purchasedProduct.receipt);
//            OnPurchase_complete(-1, "pc_test");

//#elif UNITY_ANDROID
//                    Debug.Log("android");
//                    OnPurchase_complete(-1, GetReceiptData(args).orderId);

//#elif UNITY_IOS
//                    Debug.Log("IOS");
//                    OnPurchase_complete(-1, GetReceiptData(args).orderId);
//#endif

//        }
//        else
        {
            for (int i = 0; i < coin.Length; i++)
            {
                if (String.Equals(args.purchasedProduct.definition.id, coin[i], StringComparison.Ordinal))
                {

#if UNITY_EDITOR
                    //Debug.Log("try::"+args.purchasedProduct.receipt);
                    OnPurchase_complete(i, "GPA.33");

                    
#elif UNITY_ANDROID
                    Debug.Log("android");
                    OnPurchase_complete(i, GetReceiptData(args).orderId);

#elif UNITY_IOS
                    Debug.Log("IOS");
                    OnPurchase_complete(i, args.purchasedProduct.transactionID);
#endif
                    break;
                    //Action for money
                }
            }
        }
        return PurchaseProcessingResult.Complete;
    }

    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
    }




    public void OnPurchase_complete(int x, string tr_id)
    {
        
        //if (x == -1)
        //{
        //    Update_API_data.Instance.order_id = tr_id;
        //    Gamemanager.Instance.Onpurchase_complete(Get_API_Data_IAP.Instance.St_purchase_first_val);
        //}
        //else
        //{
        Update_API.Instance.order_id = tr_id;
        Debug.Log("tr_id  :  " + tr_id);
        Update_API.Instance.type = "purchase";
        InitScript.Instance.AddGems((int)GetComponent<IAP_API>().first_val[x]);
        Update_API.Instance.Update_data();
        //}
    }

    public ReceiptData GetReceiptData(PurchaseEventArgs e)

    {
        ReceiptData data = new ReceiptData();

        if (e != null)
        {
            //Main receipt root
            string receiptString = e.purchasedProduct.receipt;
            Debug.Log("receiptString " + receiptString);
            var receiptDict = (Dictionary<string, object>)MiniJson.JsonDecode(receiptString);
            Debug.Log("receiptDict COUNT" + receiptDict.Count);

            //Next level Paylod dict
            string payloadString = (string)receiptDict["Payload"];
            Debug.Log("payloadString " + payloadString);
            var payloadDict = (Dictionary<string, object>)MiniJson.JsonDecode(payloadString);

            //Stuff from json object
            string jsonString = (string)payloadDict["json"];
            Debug.Log("jsonString " + jsonString);
            var jsonDict = (Dictionary<string, object>)MiniJson.JsonDecode(jsonString);
            string orderIdString = (string)jsonDict["orderId"];
            Debug.Log("orderIdString " + orderIdString);
            string packageNameString = (string)jsonDict["packageName"];
            Debug.Log("packageNameString " + packageNameString);
            string productIdString = (string)jsonDict["productId"];
            Debug.Log("productIdString " + productIdString);

            double orderDateDouble = Convert.ToDouble(jsonDict["purchaseTime"]);
            Debug.Log("orderDateDouble " + orderDateDouble);

            string purchaseTokenString = (string)jsonDict["purchaseToken"];
            Debug.Log("purchaseTokenString " + purchaseTokenString);

            //Stuff from skuDetails object
            string skuDetailsString = (string)payloadDict["skuDetails"];
            Debug.Log("skuDetailsString " + skuDetailsString);
            var skuDetailsDict = (Dictionary<string, object>)MiniJson.JsonDecode(skuDetailsString);
            long priceAmountMicrosLong = Convert.ToInt64(skuDetailsDict["price_amount_micros"]);
            Debug.Log("priceAmountMicrosLong " + priceAmountMicrosLong);
            string priceCurrencyCodeString = (string)skuDetailsDict["price_currency_code"];
            Debug.Log("priceCurrencyCodeString " + priceCurrencyCodeString);

            //Creating UTC from Epox
            DateTime orderDateTemp = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            orderDateTemp = orderDateTemp.AddMilliseconds(orderDateDouble);

            data.orderId = orderIdString;
            data.packageName = packageNameString;
            data.productId = productIdString;
            data.purchaseToken = purchaseTokenString;
            data.priceAmountMicros = priceAmountMicrosLong;
            data.priceCurrencyCode = priceCurrencyCodeString;
            data.orderDate = orderDateTemp;
            data.receipt = receiptString;
            Debug.Log("GetReceiptData succesfull");
        }
        else
        {
            Debug.Log("PurchaseEventArgs is NULL");
        }

        return data;
    }

    protected override void OnApplicationQuitCallback()
    {
        //throw new NotImplementedException();
    }

    protected override void OnEnableCallback()
    {
        //throw new NotImplementedException();
    }
}

public class ReceiptData
{
    public string orderId;
    public string packageName;
    public string productId;
    public string purchaseToken;
    public long priceAmountMicros;
    public string priceCurrencyCode;
    public DateTime orderDate;
    public string receipt;
}