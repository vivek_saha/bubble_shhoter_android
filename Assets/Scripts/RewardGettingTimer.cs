using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class RewardGettingTimer : MonoBehaviour
{
	private void Start()
	{
		this.starttime = DateTime.Today;
		if (PlayerPrefs.GetInt("SpinUsed", 0) == 1)
		{
			//this.endtime = new DateTime(PlayerPrefs.GetInt("Starting_Year"), PlayerPrefs.GetInt("Starting_Month"), PlayerPrefs.GetInt("Starting_Day"), PlayerPrefs.GetInt("Starting_Hour"), PlayerPrefs.GetInt("Starting_Minute"), PlayerPrefs.GetInt("Starting_Second"));
			this.endtime = DateTime.Parse (PlayerPrefs.GetString("Starting_Day"));
			//this.TimeDifference = this.starttime - this.endtime;

			//if (this.TimeDifference.TotalSeconds >= 86400.0)
			if(this.starttime!= this.endtime)
			{
				PlayerPrefs.SetInt("SpinUsed", 0);
				base.GetComponent<Text>().text = string.Empty;
				this.Spintext.transform.GetChild(0).gameObject.SetActive(true);
				this.Spintext.transform.GetChild(1).gameObject.SetActive(false);
				this.Map_but.GetComponent<Button>().interactable = true;
               this.Map_but.GetComponent<Animator>().enabled = true;
				this.Map_but.GetComponent<Animator>().Play("FreeSpinJumpingAnim");

			}
			else
			{
				//base.StartCoroutine(this.Spin_Left_Timer_F((int)(86400.0 - this.TimeDifference.TotalSeconds)));
				this.Spintext.transform.GetChild(0).gameObject.SetActive(false);
				this.Spintext.transform.GetChild(1).gameObject.SetActive(true);
                this.Map_but.GetComponent<Button>().interactable = true;
                this.Map_but.GetComponent<Animator>().enabled = false;
            }
		}
		else
		{
			this.Spintext.transform.GetChild(0).gameObject.SetActive(true);
			this.Spintext.transform.GetChild(1).gameObject.SetActive(false);
			base.GetComponent<Text>().text = string.Empty;
            this.Map_but.GetComponent<Button>().interactable = true;
            this.Map_but.GetComponent<Animator>().enabled = true;
			this.Map_but.GetComponent<Animator>().Play("FreeSpinJumpingAnim");
		}
	}

	public void StarTimer()
	{
		base.StartCoroutine(this.Spin_Left_Timer_F(86400));
	}

	public IEnumerator Spin_Left_Timer_F(int SpinTimer)
	{
		while (SpinTimer >= 0)
		{
			if (SpinTimer == 0)
			{
				PlayerPrefs.SetInt("SpinUsed", 0);
				base.GetComponent<Text>().text = string.Empty;
				this.Spintext.transform.GetChild(0).gameObject.SetActive(true);
				this.Spintext.transform.GetChild(1).gameObject.SetActive(false);
				break;
			}
			int hours = Mathf.FloorToInt((float)(SpinTimer / 3600));
			int minutes = Mathf.FloorToInt((float)((SpinTimer - hours * 3600) / 60));
			int seconds = Mathf.FloorToInt((float)(SpinTimer - hours * 3600 - minutes * 60));
			base.GetComponent<Text>().text = string.Format("{0:00} : {1:00} : {2:00}", hours, minutes, seconds);
			yield return new WaitForSeconds(1f);
			SpinTimer--;
		}
		yield break;
	}

	public DateTime starttime;

	public DateTime endtime;

	public TimeSpan TimeDifference;

	public GameObject Spintext;

	public GameObject Map_but;
}
