﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Analytics;
using Firebase.Messaging;
using Firebase.Extensions;
using System;
using System.Threading.Tasks;

using Firebase.Database;

public class Firebase_custome_script : MonoBehaviour
{
    public static Firebase_custome_script Instance;

    public GameObject play_but;
    public GameObject Splash_loading;

    public string server_link;

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this.gameObject);
        }

        else
        {
            Instance = this;
            DontDestroyOnLoad(this.gameObject);

        }
    }


    private string topic = "BubbleShooterTopic";
    // Start is called before the first frame update
    private void Start()
    {
        //ns = new NativeShare();
        FirebaseAnalytics.SetAnalyticsCollectionEnabled(true);
        //set_script.set_toogles();
        crashanalytics_init();

    }

    #region Firebase
    public void crashanalytics_init()
    {
        // Initialize Firebase
        Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            var dependencyStatus = task.Result;
            if (dependencyStatus == Firebase.DependencyStatus.Available)
            {
                // Create and hold a reference to your FirebaseApp,
                // where app is a Firebase.FirebaseApp property of your application class.
                // Crashlytics will use the DefaultInstance, as well;
                // this ensures that Crashlytics is initialized.
                Firebase.FirebaseApp app = Firebase.FirebaseApp.DefaultInstance;
                realtimedatabse();
                InitializeFirebaseMessaging();
                // Set a flag here for indicating that your project is ready to use Firebase.
            }
            else
            {
                UnityEngine.Debug.LogError(System.String.Format(
                  "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                // Firebase Unity SDK is not safe to use here.
            }
        });
    }


    void InitializeFirebaseMessaging()
    {
        //FirebaseMessaging.MessageReceived += OnMessageReceived;
        //FirebaseMessaging.TokenReceived += OnTokenReceived;
        FirebaseMessaging.SubscribeAsync(topic).ContinueWithOnMainThread(task =>
        {
            LogTaskCompletion(task, "SubscribeAsync");
        });
        Debug.Log("Firebase Messaging Initialized");






        // This will display the prompt to request permission to receive
        // notifications if the prompt has not already been displayed before. (If
        // the user already responded to the prompt, thier decision is cached by
        // the OS and can be changed in the OS settings).
        FirebaseMessaging.RequestPermissionAsync().ContinueWithOnMainThread(
          task =>
          {
              LogTaskCompletion(task, "RequestPermissionAsync");
          }
        );




        //isFirebaseInitialized = true;
    }

    private void realtimedatabse()
    {
        //Debug.Log("snapshot---------------------------_start");
        DatabaseReference reference = FirebaseDatabase.DefaultInstance.RootReference;
        reference.Child("Link").GetValueAsync().ContinueWithOnMainThread(tasks =>
        {
            //Debug.Log("taskkkkkkkkkkkkkkk----------" + tasks);
            if (tasks.IsFaulted)
            {
                //Debug.Log("snapshot---------------------------fault");
                // Handle the error...
            }
            else if (tasks.IsCompleted)
            {
                DataSnapshot snapshot = tasks.Result;
                //Debug.Log("snapshot---------------------------" + snapshot.Value);
                server_link = snapshot.Value.ToString();


                Ads_API.Instance.get_data();
                Login_Api_script.Instance.Custom_Start();
                //play_but.SetActive(true);
                // Do something with snapshot...
            }
            else if (tasks.Exception != null)
            {
                Debug.Log(tasks.Exception);
            }
        });
    }

    private bool LogTaskCompletion(Task task, string operation)
    {
        bool complete = false;
        if (task.IsCanceled)
        {
            Debug.Log(operation + " canceled.");
        }
        else if (task.IsFaulted)
        {
            Debug.Log(operation + " encounted an error.");
            foreach (Exception exception in task.Exception.Flatten().InnerExceptions)
            {
                string errorCode = "";
                Firebase.FirebaseException firebaseEx = exception as Firebase.FirebaseException;
                if (firebaseEx != null)
                {
                    errorCode = String.Format("Error.{0}: ",
                      ((Firebase.Messaging.Error)firebaseEx.ErrorCode).ToString());
                }
                Debug.Log(errorCode + exception.ToString());
            }
        }
        else if (task.IsCompleted)
        {
            Debug.Log(operation + " completed");
            complete = true;
        }
        return complete;
    }

    #endregion


    // Update is called once per frame
    void Update()
    {

    }
}
