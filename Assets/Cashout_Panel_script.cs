﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cashout_Panel_script : MonoBehaviour
{
    public static Cashout_Panel_script Instance;
    public Image Cashout_but;

    public Sprite ready_cashout, proccesing_cashout;

    public GameObject nem_popup;

    public RectTransform origin;

    [Space]
    public Text Coin_text;
    public Text bit_coin_text;
    public Text Panel_msg;
    public Text one_bitcoin_value;
    public Image slider_image;
    public GameObject google_sign_in_but;
    public GameObject Slider_object;
    public GameObject Cashout_panel;




    private void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        but_change();
    }




    
    public void but_change()
    {

        if (Setting_API.Instance.cashout_enabled)
        {
            Cashout_but.gameObject.SetActive(true);
            if (Setting_API.Instance.is_cashout_done)
            {
                Cashout_but.sprite = proccesing_cashout;
            }
            else
            {
                Cashout_but.sprite = ready_cashout;
            }
        }
        else
        {
            Cashout_but.gameObject.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void nem_popup_fun(string msg)
    {
        GameObject a = Instantiate(nem_popup);
        a.GetComponent<nem_popup_script>().msg.text = msg;
        a.transform.SetParent(this.transform);
        a.transform.localScale = Vector3.one;
        a.transform.position = origin.transform.position;
    }
    public void Onbut_click()
    {
        if(Cashout_but.sprite == proccesing_cashout)
        {
            nem_popup_fun("Processing..." +
                "\nIt can take 24-48h");
        }
        else
        {
            start_cashout_panel();
        }
    }

    public void start_cashout_panel()
    {
        google_sign_in_but.SetActive(false);
        Panel_msg.gameObject.SetActive(false);
        Slider_object.SetActive(false);

        Panel_msg.text = "You need to have atleast " + Setting_API.Instance.cashout_coin_value.ToString() +" coins to cashout!";
        Coin_text.text = PlayerPrefs.GetInt("Gems", 0).ToString();
        bit_coin_text.text = (decimal.Parse(Setting_API.Instance.bit_coin_value)*(decimal) PlayerPrefs.GetInt("Gems", 0)) + " Bitcoin";

        one_bitcoin_value.text = "1 Coin = "+ Setting_API.Instance.bit_coin_value + " Bitcoin"; ;
        if (PlayerPrefs.GetInt("Gems") == 0)
        {
            slider_image.fillAmount = 0;

        }
        else
        {
            slider_image.fillAmount = (float)PlayerPrefs.GetInt("Gems") /(float) Setting_API.Instance.cashout_coin_value;
            Debug.Log("slider_image.fillAmount " + slider_image.fillAmount);
        }
        if (slider_image.fillAmount >= 1)
        {
            google_sign_in_but.SetActive(true);
            Panel_msg.gameObject.SetActive(true);
            Panel_msg.text = "Sign-In to Withdraw Bitcoin";
        }
        else
        {
            
            Panel_msg.gameObject.SetActive(true);
            Slider_object.SetActive(true);
        }
        Cashout_panel.SetActive(true);
    }


    public void OnCLick_google_login()
    {
        GoogleSignInDemo.Instance.SignInWithGoogle();
    }
}
